package hr.fer.oprpp1.custom.scripting.parser;

import hr.fer.oprpp1.custom.scripting.elems.*;
import hr.fer.oprpp1.custom.scripting.lexer.SmartScriptLexer;
import hr.fer.oprpp1.custom.scripting.lexer.SmartScriptLexerException;
import hr.fer.oprpp1.custom.scripting.nodes.EchoNode;
import hr.fer.oprpp1.custom.scripting.nodes.ForLoopNode;
import hr.fer.oprpp1.custom.scripting.nodes.TextNode;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


public class SmartScriptParserTest {
    @Test
    private String readExample(int n) {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("extra/primjer" + n + ".txt")) {
            if (is == null) throw new RuntimeException("Datoteka extra/primjer" + n + ".txt je nedostupna.");
            byte[] data = is.readAllBytes();
            String text = new String(data, StandardCharsets.UTF_8);
            return text;
        } catch (IOException ex) {
            throw new RuntimeException("Greška pri čitanju datoteke.", ex);
        }
    }

    @Test
    public void test1() {
        String text = readExample(1);
        SmartScriptParser parser = new SmartScriptParser(text);
        System.out.println(parser.getDocumentNode().toString());
        assertEquals(1, parser.getDocumentNode().numberOfChildren());
        assertEquals(TextNode.class, parser.getDocumentNode().getChild(0).getClass());
    }

    @Test
    public void test2() {
        String text = readExample(2);
        SmartScriptParser parser = new SmartScriptParser(text);
        System.out.println(parser.getDocumentNode().toString());
        assertEquals(1, parser.getDocumentNode().numberOfChildren());
        assertEquals(TextNode.class, parser.getDocumentNode().getChild(0).getClass());
    }

    @Test
    public void test3() {
        String text = readExample(3);
        SmartScriptParser parser = new SmartScriptParser(text);
        System.out.println(parser.getDocumentNode().toString());
        assertEquals(1, parser.getDocumentNode().numberOfChildren());
        assertEquals(TextNode.class, parser.getDocumentNode().getChild(0).getClass());
    }

    @Test
    public void test4() {
        try {
            String text = readExample(4);
            SmartScriptParser parser = new SmartScriptParser(text);
            System.out.println(parser.getDocumentNode().toString());
            assertEquals(1, parser.getDocumentNode().numberOfChildren());
            assertEquals(TextNode.class, parser.getDocumentNode().getChild(0).getClass());
        } catch (Exception e) {
        }
    }

    @Test
    public void test5() {
        try {
            String text = readExample(5);
            SmartScriptParser parser = new SmartScriptParser(text);
            System.out.println(parser.getDocumentNode().toString());
            assertEquals(1, parser.getDocumentNode().numberOfChildren());
            assertEquals(TextNode.class, parser.getDocumentNode().getChild(0).getClass());
        } catch (Exception e) {
        }
    }

    @Test
    public void test6() {
        String text = readExample(6);
        SmartScriptParser parser = new SmartScriptParser(text);
        System.out.println(parser.getDocumentNode().toString());
        assertEquals(1, parser.getDocumentNode().numberOfChildren());
        assertEquals(TextNode.class, parser.getDocumentNode().getChild(0).getClass());
    }

    @Test
    public void test7() {
        try {
            String text = readExample(7);
            SmartScriptParser parser = new SmartScriptParser(text);
            System.out.println(parser.getDocumentNode().toString());
            assertEquals(1, parser.getDocumentNode().numberOfChildren());
            assertEquals(TextNode.class, parser.getDocumentNode().getChild(0).getClass());
        } catch (Exception e) {
        }

    }

    @Test
    public void test8() {
        String text = readExample(8);
        SmartScriptParser parser = new SmartScriptParser(text);
        System.out.println(parser.getDocumentNode().toString());
        assertEquals(1, parser.getDocumentNode().numberOfChildren());
        assertEquals(TextNode.class, parser.getDocumentNode().getChild(0).getClass());
    }

    @Test
    public void test9() {
        try {
            String text = readExample(9);
            SmartScriptParser parser = new SmartScriptParser(text);
            System.out.println(parser.getDocumentNode().toString());
            assertEquals(1, parser.getDocumentNode().numberOfChildren());
            assertEquals(TextNode.class, parser.getDocumentNode().getChild(0).getClass());
        } catch (Exception e) {
        }
    }

    @Test
    public void testStringInTags() {
        String text = "A tag follows {$= \"Joe \\\"Long\\\" Smith\"$}.";
        SmartScriptParser parser = new SmartScriptParser(text);
        System.out.println(parser.getDocumentNode().toString());
        System.out.println(parser.getDocumentNode().getChild(0).toString());
        EchoNode e = (EchoNode) parser.getDocumentNode().getChild(1);
        System.out.println(e.getElements()[0].asText());
    }

    @Test
    public void testStringOutsideTag() {
        String text = "Example { bla } blu \\{$=1$}. Nothing interesting {=here}.";
        SmartScriptParser parser = new SmartScriptParser(text);
        SmartScriptParser parser1 = new SmartScriptParser(parser.getDocumentNode().toString());
        System.out.println(parser.getDocumentNode().toString());
        assertEquals(parser.getDocumentNode(), parser1.getDocumentNode());

    }


    @Test
    public void testEchoNode() {
        String text = "{$= -1.2 2 @function + \"string\" $}";
        SmartScriptParser parser= new SmartScriptParser(text);
        Element[] array = {new ElementConstantDouble(-1.2), new ElementConstantInteger(2), new ElementFunction("@function"), new ElementOperator("+"), new ElementString("string")};
        EchoNode echo = new EchoNode(array);
        assertEquals(echo, parser.getDocumentNode().getChild(0));
    }

    @Test
    public void testEchoNodeNoElements() {
        String text= "{$= $}";
        SmartScriptParser parser = new SmartScriptParser(text);
        SmartScriptParser parser1 = new SmartScriptParser(parser.getDocumentNode().toString());
        assertEquals(parser.getDocumentNode(), parser1.getDocumentNode());
    }

    @Test
    public void testForLoopNodeThree() {
        String text = "{$ for i -1.2 \"string\" $}";
        SmartScriptParser parser = new SmartScriptParser(text);
        ForLoopNode loop= new ForLoopNode(new ElementVariable("i"), new ElementConstantDouble(-1.2), new ElementString("string"), null);
        assertEquals(loop, parser.getDocumentNode().getChild(0));
    }

    @Test
    public void testForLoopNodesFour() {
        String text = "{$ for i -1.2 \"string\" -1$}";
        SmartScriptParser parser = new SmartScriptParser(text);
        ForLoopNode loop= new ForLoopNode(new ElementVariable("i"), new ElementConstantDouble(-1.2), new ElementString("string"), new ElementConstantInteger(-1));
        assertEquals(loop, parser.getDocumentNode().getChild(0));
    }

    @Test
    public void testForLoopNodeFive() {
        String text = "{$ for i -1.2 \"string\" -1 2$}";
        assertThrows(SmartScriptParserException.class, ()->new SmartScriptParser(text));
    }

    @Test
    public void testForLoopNodeFirstNotVariable() {
        String text = "{$ for 1 -1.2 \"string\" $}";
        assertThrows(SmartScriptParserException.class, ()->new SmartScriptParser(text));
    }

    @Test
    public void testForLoopNodeOperator() {
        String text = "{$ for i + \"string\" $}";
        assertThrows(SmartScriptParserException.class, ()->new SmartScriptParser(text));
    }
    @Test
    public void testForLoopNodeFunction() {
        String text = "{$ for i @sin \"string\" $}";
        assertThrows(SmartScriptParserException.class, ()-> new SmartScriptParser(text));
    }
}
