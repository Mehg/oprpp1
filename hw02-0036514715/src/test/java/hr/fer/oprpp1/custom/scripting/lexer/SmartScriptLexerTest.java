package hr.fer.oprpp1.custom.scripting.lexer;

import hr.fer.oprpp1.custom.scripting.elems.ElementString;
import hr.fer.oprpp1.custom.scripting.parser.SmartScriptParser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SmartScriptLexerTest {
    @Test
    public void testEmpty() {
        String text = "";
        SmartScriptLexer test = new SmartScriptLexer(text);
        SmartScriptToken next = test.nextToken();
        assertNull(next.getValue());
        assertEquals(SmartScriptTokenType.EOF, next.getType());
        next = test.getToken();
        assertNull(next.getValue());
    }

    @Test
    public void testGetReturnsLastNext() {
        String text = "";
        SmartScriptLexer test = new SmartScriptLexer(text);
        test.nextToken();
        SmartScriptToken next = test.getToken();
        assertEquals(SmartScriptTokenType.EOF, next.getType());

    }

    @Test
    public void testNull() {
        assertThrows(NullPointerException.class, () -> new SmartScriptLexer(null));
    }

    @Test
    public void testRadAfterEOF() {
        SmartScriptLexer lexer = new SmartScriptLexer("");
        lexer.nextToken();
        assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
    }

    @Test
    public void testNoContent() {
        String string = "    \n\t ";
        SmartScriptLexer lexer = new SmartScriptLexer(string);
        assertEquals(string, lexer.nextToken().getValue());
    }

    @Test
    public void testPlainText() {
        String text = "This is \n some     plain text  \t";
        SmartScriptLexer test = new SmartScriptLexer(text);
        assertEquals(text, test.nextToken().getValue());
    }

    @Test
    public void testSpacesInTagName() {
        String text = "{$     end ";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        assertEquals(SmartScriptTokenType.OPEN, lexer.nextToken().getType());
        assertEquals(SmartScriptTokenType.VARIABLE, lexer.nextToken().getType());
        assertEquals("end", lexer.getToken().getValue());
    }

    @Test
    public void testPlainTextEscape() {
        String text = "This is \n some  \\\\ \\{   plain text  \t";
        String expected = "This is \n some  \\ {   plain text  \t";
        SmartScriptLexer test = new SmartScriptLexer(text);
        assertEquals(expected, test.nextToken().getValue());
    }

    @Test
    public void testPlainTextEdge() {
        String text = "This is \n some  \\\\ \\{$   plain text  \t";
        String expected = "This is \n some  \\ {$   plain text  \t";
        SmartScriptLexer test = new SmartScriptLexer(text);
        assertEquals(expected, test.nextToken().getValue());
    }

    @Test
    public void testSquiggly() {
        String text = "This is \n some  \\\\ \\{   plain text  \t";
        String expected = "This is \n some  \\ {   plain text  \t";
        SmartScriptLexer test = new SmartScriptLexer(text);
        assertEquals(expected, test.nextToken().getValue());
    }

    @Test
    public void testVariableNameCorrect() {
        String text = "{$A7_bb";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        assertEquals(SmartScriptTokenType.VARIABLE, lexer.nextToken().getType());
        assertEquals("A7_bb", lexer.getToken().getValue());
    }

    @Test
    public void testVariableNameFalse() {
        String text = "{$_a7_bb";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
    }

    @Test
    public void testOperator() {
        String text = "{$ + - * / ^";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        assertEquals(SmartScriptTokenType.OPERATOR, lexer.nextToken().getType());
        assertEquals(SmartScriptTokenType.OPERATOR, lexer.nextToken().getType());
        assertEquals(SmartScriptTokenType.OPERATOR, lexer.nextToken().getType());
        assertEquals(SmartScriptTokenType.OPERATOR, lexer.nextToken().getType());
        assertEquals(SmartScriptTokenType.OPERATOR, lexer.nextToken().getType());
    }

    @Test
    public void testOperatorFalse() {
        String text = "{$ #";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
    }

    @Test
    public void testWrongFunction() {
        String text = "{$ @3";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        assertThrows(SmartScriptLexerException.class, () -> lexer.nextToken());
    }

    @Test
    public void testCorrectFunction() {
        String text = "{$ @function_1_2_3as";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        assertEquals(SmartScriptTokenType.FUNCTION, lexer.nextToken().getType());
        assertEquals("@function_1_2_3as", lexer.getToken().getValue());
    }

    @Test
    public void testExampleFor() {
        String text = "{$ FOR i-1.35bbb\"1\" $}";
        String expected ="{$ FOR i -1.35 bbb \"1\" $}";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        SmartScriptLexer lexer1 = new SmartScriptLexer(expected);
        while(lexer.nextToken().getType()!=SmartScriptTokenType.EOF){
            assertEquals(lexer.getToken().getType(), lexer1.nextToken().getType());
            assertEquals(lexer.getToken().getValue(), lexer1.getToken().getValue());
        }
    }

    @Test
    public void testStringInTag() {
        String text = "{$= \"Joe \\\"Long\\\" Smith\"";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        lexer.nextToken();
        assertEquals(SmartScriptTokenType.STRING, lexer.nextToken().getType());
        assertEquals("Joe \"Long\" Smith", lexer.getToken().getValue());
    }

    @Test
    public void testStringOutsideTag() {
        String text = "Example { bla } blu \\{$=1$}. Nothing interesting {=here}.";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        assertEquals(SmartScriptTokenType.STRING, lexer.nextToken().getType());
        assertEquals("Example { bla } blu {$=1$}. Nothing interesting {=here}.", lexer.getToken().getValue());
    }

    @Test
    public void testTagOpen() {
        String text = "This i  s {$ text $}";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        assertEquals("This i  s ", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.STRING, lexer.getToken().getType());
        assertEquals("{$", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.OPEN, lexer.getToken().getType());
        assertEquals("text", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.VARIABLE, lexer.getToken().getType());
        assertEquals("$}", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.CLOSE, lexer.getToken().getType());
    }

    @Test
    public void testTagGeneral() {
        String text = "{$= i i * @sin  \"0.000\" 1.23  -2 @decfmt $} @sin";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        assertEquals("{$", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.OPEN, lexer.getToken().getType());
        assertEquals("=", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.EQUALS, lexer.getToken().getType());
        assertEquals("i", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.VARIABLE, lexer.getToken().getType());
        assertEquals("i", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.VARIABLE, lexer.getToken().getType());

        assertEquals("*", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.OPERATOR, lexer.getToken().getType());

        assertEquals("@sin", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.FUNCTION, lexer.getToken().getType());

        assertEquals("0.000", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.STRING, lexer.getToken().getType());

        assertEquals(1.23, lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.DOUBLE, lexer.getToken().getType());

        assertEquals(-2, lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.INTEGER, lexer.getToken().getType());

        assertEquals("@decfmt", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.FUNCTION, lexer.getToken().getType());

        assertEquals("$}", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.CLOSE, lexer.getToken().getType());
        assertEquals(" @sin", lexer.nextToken().getValue());
        assertEquals(SmartScriptTokenType.STRING, lexer.getToken().getType());
    }

    @Test
    public void testFromPdf() {
        String text = "This is sample text.{$ FOR i 1 10 1 $}  This is {$= i $}-th time this message is generated.{$END$}{$FOR i 0 10 2 $}  sin({$=i$}^2) = {$= i i * @sin  \"0.000\" @decfmt $}{$END$}";
        SmartScriptLexer lexer = new SmartScriptLexer(text);

        while (lexer.nextToken().getType() != SmartScriptTokenType.EOF) {
            System.out.println(lexer.getToken().getValue());
        }
    }

    @Test
    public void testTextNodeOutsideTagThrows() {
        String text = "Throw because invalid escape \\k";
        assertThrows(SmartScriptLexerException.class, ()->new SmartScriptParser(text));
    }

    @Test
    public void testStringElementInsideTagNotThrows() {
        String text = "{$ \"\n\" $}";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        assertEquals(SmartScriptTokenType.STRING, lexer.nextToken().getType());
        assertEquals("\n", lexer.getToken().getValue());
    }
    @Test
    public void testStringElementInsideTagThrows() {
        String text = "{$ \" \\k \" $}";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        assertThrows(SmartScriptLexerException.class, ()->lexer.nextToken());
    }

    @Test
    public void testStringNotClosed() {
        String text = "{$ \"asd";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        assertThrows(SmartScriptLexerException.class, ()->lexer.nextToken());
    }

    @Test
    public void testStringClosed() {
        String text = "{$ \"asd\"";
        SmartScriptLexer lexer = new SmartScriptLexer(text);
        lexer.nextToken();
        assertEquals("asd", lexer.nextToken().getValue());
    }
}
