package hr.fer.oprpp1.hw02.prob1;

/**
 * Representation of a lexical unit that groups one or more consecutive characters from the input text
 **/
public class Token {
    private final TokenType type;
    private final Object value;

    /**
     * Creates a new token from given parameters
     * @param type Type of token
     * @param value Value of token
     */
    public Token(TokenType type, Object value) {
        this.type = type;
        this.value = value;
    }

    /**
     * Returns current type of token
     * @return type of token
     */
    public TokenType getType() {
        return type;
    }

    /**
     * Returns value of current token
     * @return value of current token
     */
    public Object getValue() {
        return value;
    }
}