package hr.fer.oprpp1.hw02.prob1;

/**
 * Enumeration for different lexer states. Possible values: BASIC, EXTENDED
 */
public enum LexerState {
    BASIC,EXTENDED
}
