package hr.fer.oprpp1.hw02.prob1;

/**
 * Representation of a simple lexical analyzer
 */
public class Lexer {
    private final char[] data;
    private Token token;
    private int currentIndex;
    private LexerState state;

    /**
     * Constructor which creates a new Lexer which analyses given text
     *
     * @param text String which the Lexer will analyze
     * @throws NullPointerException if text is null
     */
    public Lexer(String text) {
        if (text == null) throw new NullPointerException("String text must not be null");
        this.data = text.toCharArray();
        currentIndex = 0;
        this.state = LexerState.BASIC;
    }

    /**
     * Generates and returns the next token
     *
     * @return the next token
     * @throws LexerException if current token is EOF and next token is called
     */
    public Token nextToken() {
        Token next = null;
        if (state.equals(LexerState.EXTENDED)) next = nextTokenExtended();
        else if (state.equals(LexerState.BASIC)) next = nextTokenBasic();

        if (currentIndex == data.length && next == null) {
            if (token != null && token.getType().equals(TokenType.EOF))
                throw new LexerException("Lexer processed the whole text");
            else
                next = new Token(TokenType.EOF, null);
        }
        token = next;
        return next;
    }

    /**
     * Returns the last generated token; it can be called multiple times because it doesn't generate new token
     *
     * @return last generated token
     */
    public Token getToken() {
        return token;
    }

    /**
     * Private method which returns next Token if in LexerState.EXTENDED state
     *
     * @return next Token
     */
    private Token nextTokenExtended() {
        Token next = null;
        while (currentIndex != data.length) {
            if (data[currentIndex] == '#') {
                //setState(LexerState.BASIC);
                next = new Token(TokenType.SYMBOL, '#');
                currentIndex++;
                break;
            }
            if (!Character.isWhitespace(data[currentIndex])) {
                StringBuilder sb = new StringBuilder();
                while (!Character.isWhitespace(data[currentIndex]) && data[currentIndex] != '#') {
                    sb.append(data[currentIndex++]);
                }
                next = new Token(TokenType.WORD, sb.toString());
                //if (data[currentIndex] == '#') setState(LexerState.BASIC);
                break;
            } else
                currentIndex++;
        }


        return next;
    }

    /**
     * Private method which return next Token if Lexer is in LexerState.BASIC state
     *
     * @return next Token
     * @throws LexerException if string format is invalid or number is too big to be parsed
     */
    private Token nextTokenBasic() {
        Token next = null;
        while (currentIndex != data.length) {
            if (!Character.isWhitespace(data[currentIndex])) {
                StringBuilder sb = new StringBuilder();
                if (isWordToken()) {
                    while (isWordToken()) {
                        if (isDoubleEscape()) {
                            ++currentIndex;
                            if (currentIndex == data.length) throw new LexerException("Invalid string");
                            if (Character.isDigit(data[currentIndex]) || isDoubleEscape()) {
                                sb.append(data[currentIndex++]);
                            } else throw new LexerException("Invalid string");
                        } else
                            sb.append(data[currentIndex++]);
                    }
                    next = new Token(TokenType.WORD, sb.toString());
                    break;
                } else if (isNumberToken()) {
                    while (isNumberToken()) {
                        sb.append(data[currentIndex++]);
                    }
                    try {
                        next = new Token(TokenType.NUMBER, Long.parseLong(sb.toString()));
                    } catch (NumberFormatException e) {
                        throw new LexerException("Number is too big to be a Long");
                    }
                    break;
                } else {
                    if (currentIndex != data.length) {
                        next = new Token(TokenType.SYMBOL, data[currentIndex++]);
                        //if (next.getValue().equals('#')) setState(LexerState.EXTENDED);
                    }
                    break;
                }
            }
            currentIndex++;
        }
        return next;
    }

    /**
     * Sets the lexer state to given state
     *
     * @param state given state
     * @throws NullPointerException if state is null
     */
    public void setState(LexerState state) {
        if (state == null) throw new NullPointerException("state cannot be null");
        this.state = state;
    }

    /**
     * helper function to determine whether current char is compatible with Token with TokenType Word
     *
     * @return true if it is compatible with Token with TokenType Word, otherwise false
     */
    private boolean isWordToken() {
        return currentIndex != data.length && (Character.isLetter(data[currentIndex]) || isDoubleEscape());
    }

    /**
     * helper function to determine whether current char is compatible with Token with TokenType Number
     *
     * @return true if it is compatible with Token with TokenType Number, otherwise false
     */
    private boolean isNumberToken() {
        return currentIndex != data.length && Character.isDigit(data[currentIndex]);
    }

    /**
     * helper function to determine whether current char is equal to two consecutive backslashes
     *
     * @return true if it is equal to two consecutive backslashes, otherwise false
     */
    private boolean isDoubleEscape() {
        return Character.toString(data[currentIndex]).equals("\\");
    }


}

