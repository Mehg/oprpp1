package hr.fer.oprpp1.hw02.prob1;

/**
 * Exception thrown by Lexer
 */
public class LexerException extends RuntimeException {
    public LexerException(String message) {
        super(message);
    }
}
