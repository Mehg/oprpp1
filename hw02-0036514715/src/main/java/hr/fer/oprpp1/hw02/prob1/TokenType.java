package hr.fer.oprpp1.hw02.prob1;

/**
 * Enum with possible values: EOF, WORD, NUMBER, SYMBOL
 */
public enum TokenType {
    EOF, WORD, NUMBER, SYMBOL
}
