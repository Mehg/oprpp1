    package hr.fer.oprpp1.custom.collections;
    
    /**
     * The Processor is a model of an object capable of performing some operation on the passed object
     */
    public interface Processor {
        void process(Object value);
    }
