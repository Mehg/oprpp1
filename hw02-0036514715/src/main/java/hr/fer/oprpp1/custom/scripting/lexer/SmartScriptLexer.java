package hr.fer.oprpp1.custom.scripting.lexer;


/**
 * Representation of a lexical analyzer used in SmartScriptParser
 */
public class SmartScriptLexer {
    private final char[] data;
    private SmartScriptToken token;
    private int currentIndex;
    private SmartScriptLexerState state;

    /**
     * Constructs a new SmartScriptLexer from given text
     *
     * @param text given text which will be analysed
     * @throws NullPointerException if text is null
     */
    public SmartScriptLexer(String text) {
        if (text == null) throw new NullPointerException("String text must not be null");
        this.data = text.toCharArray();
        currentIndex = 0;
        this.state = SmartScriptLexerState.TEXT;
    }

    /**
     * Returns last generated Token. Can be null
     *
     * @return last generated Token
     */
    public SmartScriptToken getToken() {
        return token;
    }

    /**
     * Sets the state of current SmartScriptLexer. Can be SmartScriptLexerState.BASIC or SmartScriptLexerState.EXTENDED
     *
     * @param state SmartScriptLexerState
     */
    public void setState(SmartScriptLexerState state) {
        this.state = state;
    }

    /**
     * Generates and returns next Token
     *
     * @return next Token
     * @throws SmartScriptLexerException if EOF and next token is called
     */
    public SmartScriptToken nextToken() {
        SmartScriptToken next = null;
        if (state.equals(SmartScriptLexerState.TEXT)) next = nextTokenText();
        if (next == null && state.equals(SmartScriptLexerState.TAG)) next = nextTokenTag();

        if (currentIndex == data.length && next == null) {
            if (token != null && token.getType().equals(SmartScriptTokenType.EOF))
                throw new SmartScriptLexerException("Lexer processed the whole text");
            else
                next = new SmartScriptToken(SmartScriptTokenType.EOF, null);
        }
        token = next;
        return next;
    }

    /**
     * Private method which return next Token in SmartScriptLexerState.TEXT mode.
     * Automatically switches state if it runs into opening tag
     *
     * @return next Token
     * @throws SmartScriptLexerException if illegal escaping in text
     */
    public SmartScriptToken nextTokenText() {
        SmartScriptToken next = null;
        StringBuilder sb = new StringBuilder();

        while (currentIndex != data.length) {
            if (currentIndex < (data.length - 1) && data[currentIndex] == '{' && data[currentIndex + 1] == '$') {
                setState(SmartScriptLexerState.TAG);
                if (sb.length() != 0)
                    next = new SmartScriptToken(SmartScriptTokenType.STRING, sb.toString());
                break;
            }
            if (currentIndex < (data.length - 1) && data[currentIndex] == '\\') {
                if (data[currentIndex + 1] == '\\')
                    sb.append("\\");
                else if (data[currentIndex + 1] == '{')
                    sb.append("{");
                else throw new SmartScriptLexerException("Illegal escaping in text");
                currentIndex += 2;
            } else {
                sb.append(data[currentIndex++]);
            }

            if (currentIndex == data.length) {
                next = new SmartScriptToken(SmartScriptTokenType.STRING, sb.toString());
            }

        }
        return next;
    }

    /**
     * Private method which return next Token in SmartScriptLexerState.TAG mode.
     * Automatically switches state if it runs into closing tag
     *
     * @return next Token
     * @throws SmartScriptLexerException if can't identify token tag
     */
    public SmartScriptToken nextTokenTag() {
        SmartScriptToken next = null;
        while (currentIndex != data.length) {
            if (data[currentIndex] == '{' && currentIndex + 1 < data.length) {
                if (data[currentIndex + 1] == '$') {
                    next = new SmartScriptToken(SmartScriptTokenType.OPEN, "{$");
                    currentIndex += 2;
                    break;
                }
            }
            if (!Character.isWhitespace(data[currentIndex])) {
                StringBuilder sb = new StringBuilder();

                if (isClose()) {
                    setState(SmartScriptLexerState.TEXT);
                    next = new SmartScriptToken(SmartScriptTokenType.CLOSE, "$}");
                    currentIndex += 2;
                    break;
                }

                if (Character.isDigit(data[currentIndex]) || isLeadingMinus()) {
                    next = getNextDigitToken(sb);
                    break;
                } else if (data[currentIndex] == '@') {
                    next = getNextFunctionToken(sb);
                    break;
                } else if (Character.isLetter(data[currentIndex])) {
                    next = getNextVariableToken(sb);
                    break;
                } else if (data[currentIndex] == '"') {
                    next = getNextStringToken(sb);
                    break;
                } else if (data[currentIndex] == '=' && getToken().getType().equals(SmartScriptTokenType.OPEN)) {
                    next = new SmartScriptToken(SmartScriptTokenType.EQUALS, "=");
                    currentIndex++;
                    break;
                } else if (isOperator()) {
                    next = new SmartScriptToken(SmartScriptTokenType.OPERATOR, Character.toString(data[currentIndex++]));
                    break;
                } else throw new SmartScriptLexerException("Can't identify");
            }
            currentIndex++;
        }
        return next;
    }

    /**
     * Private method which returns next string token
     *
     * @param sb StringBuilder used in method
     * @return new Token with type String
     */
    private SmartScriptToken getNextStringToken(StringBuilder sb) {
        SmartScriptToken next;
        currentIndex++;
        while (currentIndex < data.length && data[currentIndex] != '"') {
            if (currentIndex < data.length - 1 && data[currentIndex] == '\\' && data[currentIndex + 1] == '\\') {
                sb.append("\\");
                currentIndex += 2;
            } else if (currentIndex < data.length - 1 && data[currentIndex] == '\\' && data[currentIndex + 1] == '"') {
                sb.append('"');
                currentIndex += 2;
            } else if (data[currentIndex] == '\\')
                throw new SmartScriptLexerException("Wrong escaping");

            sb.append(data[currentIndex++]);
        }
        if(currentIndex== data.length) throw new SmartScriptLexerException("String was not closed");
        currentIndex++;
        next = new SmartScriptToken(SmartScriptTokenType.STRING, sb.toString());
        return next;
    }

    /**
     * Private method which returns next variable token
     *
     * @param sb StringBuilder used in method
     * @return new Token with type Variable
     */
    private SmartScriptToken getNextVariableToken(StringBuilder sb) {
        SmartScriptToken next;
        while (currentIndex != data.length && isVariable())
            sb.append(data[currentIndex++]);
        next = new SmartScriptToken(SmartScriptTokenType.VARIABLE, sb.toString());
        return next;
    }

    /**
     * Private method which return next function token
     *
     * @param sb StringBuilder used in method
     * @return new Token with type Function
     * @throws SmartScriptLexerException if invalid input with function (@ and no letter)
     */
    private SmartScriptToken getNextFunctionToken(StringBuilder sb) {
        SmartScriptToken next;
        sb.append("@");
        currentIndex++;
        if (!Character.isLetter(data[currentIndex]))
            throw new SmartScriptLexerException("Invalid input with function");
        while (currentIndex != data.length && (Character.isLetter(data[currentIndex]) || Character.isDigit(data[currentIndex]) || data[currentIndex] == '_'))
            sb.append(data[currentIndex++]);

        next = new SmartScriptToken(SmartScriptTokenType.FUNCTION, sb.toString());
        return next;
    }

    /**
     * Private method which returns next digit token
     *
     * @param sb StringBuilder used in method
     * @return new Token with type either Double or Integer
     * @throws SmartScriptLexerException if number is too long to be parsed
     */
    private SmartScriptToken getNextDigitToken(StringBuilder sb) {
        SmartScriptToken next;
        if (isLeadingMinus()) {
            sb.append('-');
            currentIndex++;
        }
        while (currentIndex != data.length && (Character.isDigit(data[currentIndex]) || data[currentIndex] == '.')) {
            sb.append(data[currentIndex++]);
        }
        try {
            if (sb.toString().contains("."))
                next = new SmartScriptToken(SmartScriptTokenType.DOUBLE, Double.parseDouble(sb.toString()));
            else
                next = new SmartScriptToken(SmartScriptTokenType.INTEGER, Integer.parseInt(sb.toString()));
        } catch (Exception e) {
            throw new SmartScriptLexerException("Number was too long");
        }
        return next;
    }

    /**
     * Helper function which checks if SmartScriptLexer found a number with a leading minus
     *
     * @return true if it is, otherwise false
     */
    private boolean isLeadingMinus() {
        return currentIndex < data.length - 1 && data[currentIndex] == '-' && Character.isDigit(data[currentIndex + 1]);
    }

    /**
     * Helper function which checks if SmartScriptLexer found a closing tag
     *
     * @return true if closing tag, otherwise false
     */
    private boolean isClose() {
        return currentIndex < (data.length - 1) && data[currentIndex] == '$' && data[currentIndex + 1] == '}';
    }

    /**
     * Helper function which checks if SmartScriptLexer found a variable
     *
     * @return true if variable, otherwise false
     */
    private boolean isVariable() {
        return Character.isLetter(data[currentIndex]) || Character.isDigit(data[currentIndex]) || data[currentIndex] == '_';
    }

    /**
     * Helper function which check if SmartScriptLexer found an operator
     *
     * @return true if operator, otherwise false
     */
    private boolean isOperator() {
        return data[currentIndex] == '+' || data[currentIndex] == '-' || data[currentIndex] == '*' || data[currentIndex] == '/' || data[currentIndex] == '^';
    }
}
