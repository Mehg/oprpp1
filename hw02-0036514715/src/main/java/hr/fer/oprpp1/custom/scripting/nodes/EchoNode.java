package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.scripting.elems.Element;
import hr.fer.oprpp1.custom.scripting.elems.ElementString;


/**
 * Node representing a command which generates some textual output dynamically
 */
public class EchoNode extends Node {
    private final Element[] elements;

    public Element[] getElements() {
        return elements;
    }

    /**
     * Constricts a new EchoNode from given parameters
     *
     * @param elements elements of class Element
     */
    public EchoNode(Element[] elements) {
        this.elements = elements;
    }


    /**
     * Returns String representation of EchoNode with all its elements included
     *
     * @return String representation of EchoNode
     */
    @Override
    public String
    toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{$=");
        for (Element element : elements) {
            if (element instanceof ElementString) {
                sb.append("\"");
                sb.append(((ElementString) element).getValue().replace("\\", "\\\\").replace("\"", "\\\""));
                sb.append("\"").append(" ");
            } else
                sb.append(element.asText()).append(" ");
        }
        sb.append("$}");
        return sb.toString();
    }

    /**
     * Checks if this EchoNode is equal to other Node. They are equal if all their elements are equal
     *
     * @param o other Node
     * @return true if they are equal, otherwise false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EchoNode echoNode = (EchoNode) o;
        if (this.numberOfChildren() != ((EchoNode) o).numberOfChildren()) return false;
        for (int i = 0; i < this.elements.length; i++) {
            if (!this.elements[i].asText().equals(echoNode.elements[i].asText())) return false;
        }
        return true;
    }

}
