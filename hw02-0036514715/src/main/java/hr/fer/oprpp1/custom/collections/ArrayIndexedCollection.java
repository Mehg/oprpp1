package hr.fer.oprpp1.custom.collections;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * An implementation of resizable array-backed collection of objects. Extends class Collection from previous problem
 */
public class ArrayIndexedCollection implements List {
    private int size;
    private Object[] elements;
    private long modificationCount;

    private static class ArrayElementsGetter implements ElementsGetter {
        private int nextIndex;
        private final ArrayIndexedCollection collection;
        private final long savedModificationCount;

        public ArrayElementsGetter(ArrayIndexedCollection collection, long savedModificationCount) {
            this.collection = collection;
            this.savedModificationCount = savedModificationCount;
            this.nextIndex = 0;
        }

        @Override
        public boolean hasNextElement() {
            if (savedModificationCount != collection.modificationCount)
                throw new ConcurrentModificationException("The array must not be altered in a meaningful way");
            return nextIndex < collection.size;
        }

        @Override
        public Object getNextElement() {
            if (savedModificationCount != collection.modificationCount)
                throw new ConcurrentModificationException("The array must not be altered in a meaningful way");
            if (this.hasNextElement())
                return collection.elements[nextIndex++];
            else throw new NoSuchElementException("There are no elements left");
        }
    }

    /**
     * The default constructor; creates an instance with capacity set to 16
     */
    public ArrayIndexedCollection() {
        this(16);
    }

    /**
     * Creates an ArrayIndexedCollection with capacity set as initialCapacity
     *
     * @param initialCapacity capacity
     * @throws IllegalArgumentException if initial capacity less than 1
     */
    public ArrayIndexedCollection(int initialCapacity) {
        if (initialCapacity < 1)
            throw new IllegalArgumentException("Initial capacity needs to be greater or equal to 1");
        elements = new Object[initialCapacity];
        modificationCount = 0;
    }

    /**
     * Creates an ArrayIndexedCollection with capacity set to 16 and copies elements from other Collection into this newly constructed collection
     *
     * @param other Reference to some other Collection which elements are copied into this
     */
    public ArrayIndexedCollection(Collection other) {
        this(other, 16);
    }

    /**
     * Creates an ArrayIndexedCollection with capacity set to initialCapacity and copies elements from other Collection into this newly constructed collection
     *
     * @param other           Reference to some other Collection which elements are copied into this
     * @param initialCapacity capacity
     * @throws NullPointerException if the other Collection is null
     */
    public ArrayIndexedCollection(Collection other, int initialCapacity) {
        this(Math.max(other.size(), initialCapacity));
        this.addAll(other);
    }

    /**
     * Adds the given object into this collection
     *
     * @param value Object which will be added in the collection
     * @throws NullPointerException if the given Object is null
     */
    @Override
    public void add(Object value) {
        this.insert(value, size);
    }

    /**
     * Method which removes an instance of the given Object from the collection
     *
     * @param value Object whose instance will be removed from the collection
     * @return Returns true only if the collection contains given value and removes one occurrence of it
     */
    @Override
    public boolean remove(Object value) {
        int indexOf = this.indexOf(value);
        if (indexOf != -1) {
            this.remove(indexOf);
            return true;
        }
        return false;
    }

    /**
     * Returns the size of the collection
     *
     * @return the number of currently stored objects in this collections
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * Method which checks if given Object is contained in the collection
     *
     * @param value Object which will be checked if it is in the collection
     * @return Returns true only if the collection contains given value, as determined by equals method
     */
    @Override
    public boolean contains(Object value) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(value)) return true;
        }
        return false;
    }

    /**
     * Allocates new array with size equals to the size of this collections, fills it with collection content and returns the array
     *
     * @return New array filled with the collection
     */
    @Override
    public Object[] toArray() {
        Object[] newArray = new Object[size];
        System.arraycopy(elements, 0, newArray, 0, size);
        return newArray;
    }

    /**
     * Removes all elements from this collection
     */
    @Override
    public void clear() {
        modificationCount++;
        for (int i = 0; i < size; i++)
            elements[i] = null;
        size = 0;
    }

    /**
     * Creates ElementsGetter for current instance of class
     *
     * @return new ElementsGetter
     */
    @Override
    public ElementsGetter createElementsGetter() {
        return new ArrayElementsGetter(this, modificationCount);
    }

    /**
     * Returns the object that is stored in backing array at position index
     *
     * @param index Valid indexes are 0 to size-1
     * @return object at given index
     */
    @Override
    public Object get(int index) {
        if (index < 0 || index >= size)
            throw new IndexOutOfBoundsException("Invalid index. It should be between 0 and size-1");
        return elements[index];
    }

    /**
     * Inserts the given value at the given position in array (all elements at a greater positions will be shifted one place toward the end)
     *
     * @param value    Object which is inserted
     * @param position Position at which the Object will be inserted
     * @throws IndexOutOfBoundsException if position is invalid
     * @throws NullPointerException      if value is null
     */
    @Override
    public void insert(Object value, int position) {
        if (position < 0 || position > size)
            throw new IndexOutOfBoundsException("Invalid position. It should be between 0 and size");

        if (position == size) modificationCount++;
        if (size == elements.length) {
            modificationCount++;
            Object[] biggerElements = new Object[elements.length * 2];
            System.arraycopy(elements, 0, biggerElements, 0, size);
            elements = biggerElements;
        }

        if (value == null) throw new NullPointerException("Object which you want to add must not be null");

        Object temp1, temp2 = value;
        for (int i = position; i < size; i++) {
            temp1 = elements[i];
            elements[i] = temp2;
            temp2 = temp1;
        }
        elements[size++] = temp2;
    }

    /**
     * Returns the index of the first instance of given object
     *
     * @param value Object which is searched for in the ArrayIndexedCollection
     * @return the index of the first instance if the ArrayIndexedCollection contains it. Otherwise -1
     */
    @Override
    public int indexOf(Object value) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(value)) return i;
        }
        return -1;
    }

    /**
     * Removes Object at given index
     *
     * @param index Position at which the object should be removed
     */
    @Override
    public void remove(int index) {
        if (index < 0 || index > (size - 1))
            throw new IndexOutOfBoundsException("Invalid position. It should be between 0 and size-1");
        modificationCount++;
        System.arraycopy(elements, index + 1, elements, index, size - 1 - index);
        elements[size - 1] = null;
        size--;
    }

    /**
     * Returns current capacity of the ArrayIndexedCollection
     *
     * @return capacity
     */
    public int getCapacity() {
        return elements.length;
    }
}

/*

/**
     * Method calls processor.process(.) for each element of this collection
     *
     * @param processor Given processor
     *//*

    @Override
    public void forEach(Processor processor) {
        for (int i = 0; i < size; i++) {
            processor.process(elements[i]);
        }
    }
*/