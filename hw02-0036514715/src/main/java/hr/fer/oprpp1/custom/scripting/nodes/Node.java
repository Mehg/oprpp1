package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.collections.ArrayIndexedCollection;

/**
 * Base class for all graph nodes
 */
public class Node {
    private ArrayIndexedCollection children;

    /**
     * Adds another Node as a child of this node
     *
     * @param child other node
     */
    public void addChildNode(Node child) {
        if (child == null) throw new NullPointerException("Child must not be null");
        if (children == null) {
            children = new ArrayIndexedCollection();
        }
        children.add(child);
    }

    /**
     * Returns the number of children the current Node has
     *
     * @return number of children
     */
    public int numberOfChildren() {
        if (children == null) return 0;
        return children.size();
    }

    /**
     * Returns Node which was added as a child of this node on given position
     *
     * @param index given position
     * @return child Node at given position
     */
    public Node getChild(int index) {
        return (Node) children.get(index);
    }

}
