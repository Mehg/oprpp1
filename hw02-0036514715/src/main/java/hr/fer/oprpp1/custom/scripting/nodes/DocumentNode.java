package hr.fer.oprpp1.custom.scripting.nodes;

/**
 * A node representing an entire document
 */
public class DocumentNode extends Node {

    /**
     * Creates a new DocumentNode
     */
    public DocumentNode() {
        super();
    }

    /**
     * Returns String representation of whole document. This includes all DocumentNode children
     *
     * @return String representation of while document
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numberOfChildren(); i++) {
            sb.append(getChild(i));
        }
        return sb.toString();
    }


    /**
     * Checks if this DocumentNode is equal to other Node. They are equal if all of their children are equal
     *
     * @param o other Node
     * @return true if they are equal, otherwise false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocumentNode echoNode = (DocumentNode) o;
        for (int i = 0; i < numberOfChildren(); i++) {
            if (!this.getChild(i).equals(echoNode.getChild(i)))
                return false;
        }
        return true;
    }

}
