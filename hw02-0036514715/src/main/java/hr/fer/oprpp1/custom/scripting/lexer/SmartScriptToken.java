package hr.fer.oprpp1.custom.scripting.lexer;


/**
 * Representation of a lexical unit that groups one or more consecutive characters from the input text
 */
public class SmartScriptToken {
    private final SmartScriptTokenType type;
    private final Object value;

    /**
     * Returns type of token
     *
     * @return type of token
     */
    public SmartScriptTokenType getType() {
        return type;
    }

    /**
     * Returns value of current token
     *
     * @return value of current token
     */
    public Object getValue() {
        return value;
    }

    /**
     * Constructs a new SmartScriptToken from given parameters
     *
     * @param type  TokenType - type of token
     * @param value value of token
     */
    public SmartScriptToken(SmartScriptTokenType type, Object value) {
        this.type = type;
        this.value = value;
    }
}
