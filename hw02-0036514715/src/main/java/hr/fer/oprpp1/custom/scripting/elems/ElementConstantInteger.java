package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Element which represents an Integer value
 */
public class ElementConstantInteger extends Element {
    private final int value;

    /**
     * Constructs a new ElementConstantInteger from given value
     *
     * @param value given value
     */
    public ElementConstantInteger(int value) {
        this.value = value;
    }

    /**
     * Returns value of ElementConstantInteger
     *
     * @return value of current ElementConstantInteger
     */
    public int getValue() {
        return value;
    }

    /**
     * Returns String value of ElementConstantInteger
     *
     * @return string value
     */
    @Override
    public String asText() {
        return Integer.toString(value);
    }
}
