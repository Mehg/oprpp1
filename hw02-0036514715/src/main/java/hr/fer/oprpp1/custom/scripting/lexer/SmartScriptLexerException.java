package hr.fer.oprpp1.custom.scripting.lexer;

/**
 * Exception thrown by SmartScriptLexer
 */
public class SmartScriptLexerException extends RuntimeException {
    public SmartScriptLexerException(String message) {
        super(message);
    }
}
