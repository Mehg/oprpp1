package hr.fer.oprpp1.custom.collections;


import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * Implementation of linked list-backed collection of objects
 */
public class LinkedListIndexedCollection implements List {
    private static class ListNode {
        ListNode previous;
        ListNode next;
        Object value;

        public ListNode(ListNode previous, ListNode next, Object value) {
            this.previous = previous;
            this.next = next;
            this.value = value;
        }
    }

    private static class LinkedElementsGetter implements ElementsGetter {
        private LinkedListIndexedCollection collection;
        private ListNode nextNode;
        private final long savedModificationCount;

        public LinkedElementsGetter(LinkedListIndexedCollection collection, long savedModificationCount) {
            this.collection = collection;
            this.nextNode = collection.first;
            this.savedModificationCount = savedModificationCount;
        }

        @Override
        public boolean hasNextElement() {
            if (savedModificationCount != collection.modificationCount)
                throw new ConcurrentModificationException("The list must not be altered");
            return nextNode != null;
        }

        @Override
        public Object getNextElement() {
            if (savedModificationCount != collection.modificationCount)
                throw new ConcurrentModificationException("The array must not be altered");
            if (hasNextElement()) {
                Object value = nextNode.value;
                nextNode = nextNode.next;
                return value;
            }
            throw new NoSuchElementException("There are no elements left");
        }
    }

    private int size;
    private ListNode first;
    private ListNode last;
    private long modificationCount;

    /**
     * Default constructor. Creates empty LinkedListIndexedCollection
     */
    public LinkedListIndexedCollection() {
        first = last = null;
        size = 0;
        modificationCount = 0;
    }

    /**
     * Creates LinkedListIndexedCollection and fills it with elements from other Collection
     *
     * @param other Collection from which we add the elements
     */
    public LinkedListIndexedCollection(Collection other) {
        this();
        this.addAll(other);
    }

    /**
     * Returns the size of the LinkedListIndexedCollection
     *
     * @return the size of the LinkedListIndexedCollection
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Adds the given object into this collection at the end of collection
     *
     * @param value Object which will be added in the collection
     * @throws NullPointerException if value is null
     */
    @Override
    public void add(Object value) {
        if (value == null) throw new NullPointerException("Value must not be null");
        ListNode node = new ListNode(last, null, value);
        modificationCount++;

        if (size == 0) {
            first = last = node;
        } else {
            last.next = node;
            last = node;
        }
        size++;
    }

    /**
     * Check if given value is contained in LinkedListIndexedCollection
     *
     * @param value Object which will be checked if it is in the collection
     * @return true if value is in LinkedListIndexedCollection, otherwise false
     */
    @Override
    public boolean contains(Object value) {
        ListNode curr = first;
        while (curr != null) {
            if (curr.value.equals(value)) return true;
            curr = curr.next;
        }
        return false;
    }

    /**
     * Removes the first instance of given Object
     *
     * @param value Object whose instance will be removed from the collection
     * @return true if value has been removed from LinkedListIndexedCollection, otherwise false
     */
    @Override
    public boolean remove(Object value) {
        int index = this.indexOf(value);
        if (index == -1) return false;
        remove(index);
        return true;
    }

    /**
     * Allocates new array with size equals to the size of this collections, fills it with collection content and
     * returns the array
     *
     * @return New array filled with the collection
     **/
    @Override
    public Object[] toArray() {
        ListNode curr = first;
        Object[] array = new Object[size];
        int i = 0;
        while (curr != null) {
            array[i++] = curr.value;
            curr = curr.next;
        }
        return array;
    }

    /**
     * Removes all elements from the collection. Collection forgets about current linked list
     */
    @Override
    public void clear() {
        modificationCount++;
        size = 0;
        first = last = null;
    }

    /**
     * Creates ElementsGetter for current instance of class
     *
     * @return new ElementsGetter
     */
    @Override
    public ElementsGetter createElementsGetter() {
        return new LinkedElementsGetter(this, modificationCount);
    }

    /**
     * Returns the object that is stored in linked list at position index
     *
     * @param index position
     * @return the object at that position
     * @throws IndexOutOfBoundsException if index is less than 0 and greater than size -1
     */
    @Override
    public Object get(int index) {
        if (index < 0 || index > size - 1) throw new IndexOutOfBoundsException("Index must be between 0 and size-1");
        ListNode curr = getListNode(index);
        return curr.value;
    }

    /**
     * Inserts (does not overwrite) the given value at the given position in linked-list. Elements starting from this position are shifted one position
     *
     * @param value    Object which will be inserted
     * @param position Position at which the Object will be inserted
     * @throws IndexOutOfBoundsException if position is invalid
     * @throws NullPointerException      if value is null
     */
    @Override
    public void insert(Object value, int position) {
        if (value == null) throw new NullPointerException("Value must not be null");
        if (position < 0 || position > size)
            throw new IndexOutOfBoundsException("Position must be greater than 0 and less than size. Iz was " + position);

        ListNode curr, prev, temp;

        if (size == 0) {
            curr = new ListNode(null, null, value);
            first = last = curr;
        } else {
            if (position == 0 || position == size) {
                if (position == 0) {
                    curr = new ListNode(null, first, value);
                    first.previous = curr;
                    first = curr;
                }
                if (position == size) {
                    curr = new ListNode(last, null, value);
                    last.next = curr;
                    last = curr;
                }
            } else {
                curr = getListNode(position);
                prev = curr.previous;
                temp = new ListNode(prev, curr, value);

                prev.next = temp;
                curr.previous = temp;
            }

        }
        size++;
        modificationCount++;
    }

    /**
     * Searches the collection and returns the index of the first occurrence of the given value or -1 if the value is not found
     *
     * @param value Value that is searched for
     * @return index of the first occurrence of given value
     */
    @Override
    public int indexOf(Object value) {
        ListNode curr = first;
        int index = 0;
        while (curr != null) {
            if ((curr.value).equals(value)) {
                return index;
            }
            curr = curr.next;
            index++;
        }
        return -1;
    }

    /**
     * Removes element at specified index from collection. Following elements are shifted.
     *
     * @param index Position from which we remove the node
     * @throws IndexOutOfBoundsException if index is less than 0 and greater than size-1
     */
    @Override
    public void remove(int index) {
        if (index < 0 || index >= size)
            throw new IndexOutOfBoundsException("Index has to be between 0 and size-1 but it was" + index);

        ListNode toRemove = getListNode(index);
        if (toRemove == first && toRemove == last) {
            first = last = null;
        } else if (toRemove == first || toRemove == last) {
            if (toRemove == first) {
                first = toRemove.next;
                toRemove.next.previous = null;
            }
            if (toRemove == last) {
                last = toRemove.previous;
                toRemove.previous.next = null;
            }
        } else {
            ListNode after = toRemove.next, before = toRemove.previous;
            before.next = after;
            after.previous = before;
        }
        size--;
        modificationCount++;
    }

    /**
     * Returns a node at the given position in O(n/2+1)
     *
     * @param position at which the wanted node is
     * @return node at position
     */
    private ListNode getListNode(int position) {
        ListNode curr;
        if (position < (size / 2)) {
            curr = first;
            for (int i = 0; i < position; i++) {
                curr = curr.next;
            }
        } else {
            curr = last;
            for (int i = (size - 1); i > position; i--) {
                curr = curr.previous;
            }
        }
        return curr;
    }


}
/*
/**
 * Method calls processor.process(.) for each element of this collection
 *
 * @param processor Given processor
 *//*
    @Override
    public void forEach(Processor processor) {
        ListNode curr = first;
        while (curr != null) {
            processor.process(curr.value);
            curr = curr.next;
        }
    }*/