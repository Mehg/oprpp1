package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Element which represents a Double value
 */
public class ElementConstantDouble extends Element {
    private final Double value;

    /**
     * Constructs a new ElementConstantDouble from given value
     * @param value given value
     */
    public ElementConstantDouble(Double value) {
        this.value = value;
    }

    /**
     * Returns value of ElementConstantDouble
     * @return value of current ElementConstantDouble
     */
    public Double getValue() {
        return value;
    }

    /**
     * Returns String value of Double value of ElementConstantDouble
     *
     * @return string value
     */
    @Override
    public String asText() {
        return Double.toString(value);
    }
}
