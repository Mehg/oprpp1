package hr.fer.oprpp1.custom.scripting.elems;


/**
 * Element which represents a function - a function starts with @
 */
public class ElementFunction extends Element {
    private final String name;

    /**
     * Returns value of ElementFunction
     *
     * @return value of current ElementFunction
     */
    public String getName() {
        return name;
    }

    /**
     * Constructs a new ElementFunction from given value
     *
     * @param name given string
     */
    public ElementFunction(String name) {
        this.name = name;
    }

    /**
     * Returns String value of ElementFunction
     *
     * @return string value
     */
    @Override
    public String asText() {
        return name;
    }
}

