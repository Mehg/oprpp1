package hr.fer.oprpp1.custom.scripting.elems;


/**
 * Base class representing element in process of parsing
 */
public class Element {

    /**
     * Returns current element as string
     *
     * @return string value of current element
     */
    public String asText() {
        return "";
    }
}
