package hr.fer.oprpp1.custom.scripting.parser;

/**
 * Exception which is thrown by SmartScriptParser
 */
public class SmartScriptParserException extends RuntimeException {
    public SmartScriptParserException(String message) {
        super(message);
    }
}
