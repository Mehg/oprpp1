package hr.fer.oprpp1.java.fractals;

import hr.fer.oprpp1.math.Complex;
import hr.fer.oprpp1.math.ComplexRootedPolynomial;
import hr.fer.zemris.java.fractals.viewer.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class for drawing Newton-Raphson iteration based fractal viewer
 */
public class Newton {
    public static void main(String[] args) {
        List<Complex> roots = getUserRoots();
        System.out.println("Image of fractal will appear shortly. Thank you.");
        FractalViewer.show(new NewtonProducer(new ComplexRootedPolynomial(Complex.ONE, roots.toArray(Complex[]::new))));
    }

    /**
     * Helper method which extracts arguments from user
     *
     * @return List of Complex numbers which represent the roots for ComplexRootedPolynomial
     */
    static List<Complex> getUserRoots() {
        System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
        System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");

        List<Complex> roots = new ArrayList<>();
        int i = 1;
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("Root " + i + "> ");
            String line = sc.nextLine();

            if (line.trim().equalsIgnoreCase("done"))
                if (roots.size() < 2) {
                    System.out.println("Please enter at least two roots");
                    continue;
                } else
                    break;

            try {
                roots.add(Complex.parse(line.trim()));
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                i--;
            }

            i++;
        }

        sc.close();
        return roots;
    }


    /**
     * Implementation of IFractalProducer for Newton-Raphson fractals
     */
    public static class NewtonProducer implements IFractalProducer {
        /**
         * ComplexRootedPolynomial which is used as base function
         */
        private final ComplexRootedPolynomial polynomial;
        /**
         * Maximum number of iterations
         */
        private final int maxIterations;
        /**
         * Convergence threshold
         */
        private final double convergenceThreshold;
        /**
         * Root threshold
         */
        private final double rootThreshold;

        /**
         * Creates a new NewtonProducer from given parameters
         *
         * @param polynomial given polynomial
         */
        public NewtonProducer(ComplexRootedPolynomial polynomial) {
            this.polynomial = polynomial;
            this.maxIterations = 16 * 16 * 16;
            this.convergenceThreshold = 0.001;
            this.rootThreshold = 0.002;
        }

        @Override
        public void produce(double reMin, double reMax, double imMin, double imMax,
                            int width, int height, long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {

            int offset = 0;
            short[] data = new short[width * height];

            for (int y = 0; y < height; y++) {
                if (cancel.get()) break;
                for (int x = 0; x < width; x++) {
                    Complex zn = mapToComplexPlain(x, y, reMin, reMax, imMin, imMax, width, height);
                    Complex znOld;
                    int i = 0;

                    do {
                        znOld = zn;
                        zn = zn.sub(polynomial.apply(zn).divide(polynomial.toComplexPolynom().derive().apply(zn)));
                        i++;
                    } while (znOld.sub(zn).module() > convergenceThreshold && i < maxIterations);

                    data[offset++] = (short) (polynomial.indexOfClosestRootFor(zn, rootThreshold) + 1);
                }
            }
            observer.acceptResult(data, (short) (polynomial.toComplexPolynom().order() + 1), requestNo);
        }

        /**
         * Private helper function for mapping a point to the Complex plain
         *
         * @param x      given x coordinate
         * @param y      given y coordinate
         * @param reMin  minimum of real axis
         * @param reMax  maximum of real axis
         * @param imMin  minimum of imaginary axis
         * @param imMax  maximum of imaginary axis
         * @param width  width
         * @param height height
         * @return Complex
         */
        private Complex mapToComplexPlain(int x, int y, double reMin, double reMax, double imMin, double imMax, int width, int height) {
            double real = x * (reMax - reMin) / (width - 1.0) + reMin;
            double imag = (height - 1.0 - y) * (imMax - imMin) / (height - 1.0) + imMin;

            return new Complex(real, imag);
        }
    }
}
