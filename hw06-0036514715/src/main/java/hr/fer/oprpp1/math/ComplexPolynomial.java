package hr.fer.oprpp1.math;

import java.util.Arrays;
import java.util.List;

/**
 * Models a complex polynomial with following format
 * f(z)=z0+z1*z+z2*z^2+...+zn*z^n
 */
public class ComplexPolynomial {
    /**
     * List of Complex factors - z0, z1, z2, ..., zn
     */
    private final List<Complex> factors;

    /**
     * Creates a new Complex from given arguments
     *
     * @param factors given factors order from z0 to zn
     */
    public ComplexPolynomial(Complex... factors) {
        if (factors == null) throw new NullPointerException("factors must not be null");
        this.factors = Arrays.asList(factors);
    }

    /**
     * Returns the order of complex polynomial
     *
     * @return the order of complex polynomial
     */
    public short order() {
        return (short) (factors.size() - 1);
    }


    /**
     * Multiplies this and given ComplexPolynomial and returns the result
     *
     * @param p given ComplexPolynomial
     * @return new ComplexPolynomial which is the product of this and given ComplexPolynomial
     */
    public ComplexPolynomial multiply(ComplexPolynomial p) {
        if (p == null) throw new NullPointerException("other polynomial must not be null");
        Complex[] newFactors = new Complex[p.order() + this.order() + 1];

        Arrays.fill(newFactors, Complex.ZERO);

        for (int i = 0; i < p.factors.size(); i++) {
            for (int j = 0; j < this.factors.size(); j++) {
                newFactors[i + j] = newFactors[i + j].add(p.factors.get(i).multiply(factors.get(j)));
            }
        }

        return new ComplexPolynomial(newFactors);

    }


    /**
     * Returns current ComplexPolynomial's derivative
     *
     * @return current ComplexPolynomial's derivative
     */
    public ComplexPolynomial derive() {
        Complex[] newFactors = new Complex[factors.size() - 1];

        for (int i = 1; i < factors.size(); i++) {
            newFactors[i - 1] = factors.get(i).multiply(new Complex(i, 0));
        }
        return new ComplexPolynomial(newFactors);
    }

    public Complex apply(Complex z) {
        Complex result = new Complex();

        for (int i = 0; i < factors.size(); i++) {
            result = result.add(z.power(i).multiply(factors.get(i)));
        }

        return result;

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = factors.size() - 1; i > 0; i--) {
            sb.append(factors.get(i)).append("*z^").append(i).append("+");
        }
        sb.append(factors.get(0));
        return sb.toString();
    }

}
