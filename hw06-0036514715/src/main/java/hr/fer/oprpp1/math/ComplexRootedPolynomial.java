package hr.fer.oprpp1.math;

import java.util.Arrays;
import java.util.List;

/**
 * Models a complex polynomial with following format
 * f(z)=z0*(z-z1)*(z-z2)*...*(z-zn)
 */
public class ComplexRootedPolynomial {
    /**
     * Constant - z0
     */
    private final Complex constant;
    /**
     * Complex numbers in brackets - z1, z2, ..., zn
     */
    private final List<Complex> roots;

    /**
     * Creates a new Complex from given arguments
     *
     * @param constant Complex constant - z0
     * @param roots    Complex roots - z1, z2, ..., zn
     */
    public ComplexRootedPolynomial(Complex constant, Complex... roots) {
        if (constant == null || roots == null) throw new NullPointerException("Arguments must not be null");
        this.constant = constant;
        this.roots = Arrays.asList(roots);
    }

    /**
     * Applies given Complex to current ComplexRootedPolynomial
     *
     * @param z given Complex
     * @return result of applying
     */
    public Complex apply(Complex z) {
        if (z == null) throw new NullPointerException("Given complex number must not be null");
        Complex result = constant;

        for (Complex c : roots) {
            result = result.multiply(z.sub(c));
        }

        return result;
    }

    /**
     * Converts current ComplexRootedPolynomial to ComplexPolynomial
     *
     * @return equivalent ComplexPolynomial
     */
    public ComplexPolynomial toComplexPolynom() {
        ComplexPolynomial result = new ComplexPolynomial(constant);

        for (Complex c : roots) {
            result = result.multiply(new ComplexPolynomial(c.negate(), Complex.ONE));
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(constant);

        for (Complex c : roots) {
            sb.append("*").append("(z-").append(c).append(")");
        }

        return sb.toString();
    }


    /**
     * Returns index of closest root for given complex number and threshold
     * @param z given complex number
     * @param threshold given threshold
     * @return index of closest root for given complex number and threshold. If such does not exist returns -1
     */
    public int indexOfClosestRootFor(Complex z, double threshold) {
        if (z == null) throw new NullPointerException("Complex number must not be null");

        int index = -1;
        double minDistance = threshold;

        for (int i = 0; i < roots.size(); i++) {
            double distance = z.sub(roots.get(i)).module();
            if (distance < minDistance) {
                minDistance = distance;
                index = i;
            }
        }
        return index;
    }
}
