package hr.fer.oprpp1.hw05.crypto;

import hr.fer.oprpp1.hw05.crypto.Util;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UtilTest {
    @Test
    public void testHexToByteNull() {
        assertThrows(NullPointerException.class, ()-> Util.hextobyte(null) );
    }

    @Test
    public void testByteToHexNull() {
        assertThrows(NullPointerException.class, ()->Util.bytetohex(null));
    }

    @Test
    public void testHexToByteInvalidLength() {
        assertThrows(IllegalArgumentException.class, ()->Util.hextobyte("012"));
    }

    @Test
    public void testHexToByteInvalidCharacter() {
        assertThrows(IllegalArgumentException.class, ()->Util.hextobyte("a1f2g4"));
    }

    @Test
    public void testHexToBytePDF() {
        byte[] correct = new byte[] {1, -82, 34};
        byte[] result = Util.hextobyte("01aE22");

        for(int i=0; i< result.length; i++)
            assertEquals(correct[i], result[i] );
    }

    @Test
    public void testByteToHexPDF() {
        assertEquals( "01ae22", Util.bytetohex(new byte[] {1, -82, 34}));
    }
}
