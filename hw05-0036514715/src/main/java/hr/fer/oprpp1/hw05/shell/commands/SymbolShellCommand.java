package hr.fer.oprpp1.hw05.shell.commands;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

import java.util.List;

/**
 * ShellCommand used for changing or printing the current prompt, multiline, or morelines symbol
 */
public class SymbolShellCommand implements ShellCommand {
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        String[] argumentParts = arguments.split("\\s");


        if (argumentParts.length != 1 && argumentParts.length != 2)
            env.writeln("Invalid number of arguments - was " + argumentParts.length + " expected 1 or 2");
        else {
            Character c = extractFirstArgument(argumentParts[0], env);

            if (c.equals(' '))
                env.writeln("Invalid first argument - was " + argumentParts[0] + " expected PROMPT/MULTILINE/MORELINES");
            else if (argumentParts.length == 1)
                env.writeln("Symbol for " + argumentParts[0] + " is '" + c + "'");
            else if (argumentParts[1].length() != 1)
                env.writeln("Invalid second argument - was " + argumentParts[1] + " expected character");
            else {
                Character newChar = argumentParts[1].strip().charAt(0);
                setCharacter(argumentParts[0], newChar, env);
                env.writeln("Symbol for " + argumentParts[0] + " changed from '" + c + "' to '" + newChar + "'");
            }

        }

        return ShellStatus.CONTINUE;
    }

    private Character extractFirstArgument(String s, Environment env) {
        return switch (s) {
            case "PROMPT" -> env.getPromptSymbol();
            case "MULTILINE" -> env.getMultilineSymbol();
            case "MORELINES" -> env.getMorelinesSymbol();
            default -> ' ';
        };
    }

    private void setCharacter(String s, Character newChar, Environment env) {
        switch (s) {
            case "PROMPT" -> env.setPromptSymbol(newChar);
            case "MULTILINE" -> env.setMultilineSymbol(newChar);
            case "MORELINES" -> env.setMorelinesSymbol(newChar);
            default -> throw new IllegalStateException("Unexpected value: " + s);
        }

    }

    @Override
    public String getCommandName() {
        return "symbol";
    }

    @Override
    public List<String> getCommandDescription() {
        return List.of("Command for getting and setting current PROMPT, MORELINES or MULTILINE symbol",
                "The command gets one or two arguments",
                "The first argument has to be either PROMPT, MORELINES or MULTILINE ",
                "If there is no second argument the command will return the symbol used for the first argument",
                "If there is a second argument the command will set the second argument as the symbol for the first argument");
    }
}
