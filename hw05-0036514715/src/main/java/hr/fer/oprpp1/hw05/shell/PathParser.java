package hr.fer.oprpp1.hw05.shell;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class used for parsing paths
 */
public class PathParser {
    private String data;
    private final ArrayList<String> allPaths;

    /**
     * Creates a new PathParser which will parse given text
     *
     * @param d given text
     */
    public PathParser(String d) {
        this.data = d.strip();
        allPaths = new ArrayList<>();
        parse();
    }

    /**
     * Reads next path in given text and puts it in allPaths
     */
    public void parse() {
        while (!data.isBlank()) {
            String path = data.startsWith("\"") ? extractOnePathInQuotes() : extractOnePath();
            allPaths.add(path);
        }

    }

    /**
     * Returns all parsed paths
     *
     * @return all parsed paths
     */
    public List<String> getAllPaths() {
        return allPaths;
    }

    /**
     * Method which reads next path in given text where the path is surrounded by quotation marks
     *
     * @return new String which represents next path
     * @throws IllegalArgumentException if given path doesn't match the format i.e. "Path".txt
     */
    public String extractOnePathInQuotes() {
        int firstQuote = data.indexOf("\"");
        int secondQuote = data.indexOf("\"", 1);

        String working = data.substring(firstQuote + 1, secondQuote);
        working = working.replace("\\\\", "\\");
        working = working.replace("\\\"", "\"");

        data = data.substring(secondQuote + 1);

        if (!data.equals("") && data.equals(data.stripLeading()))
            throw new IllegalArgumentException();
        data = data.strip();

        return working;
    }

    /**
     * Method which reads next path in given text where the path isn't surrounded by quotation marks
     *
     * @return new String which represents next path
     */
    public String extractOnePath() {
        String path = data;
        if (data.contains(" ")) {
            path = data.split(" ")[0];
            data = data.split(" ", 2)[1];
            data = data.strip();
        } else {
            data = "";
        }

        return path;
    }

}
