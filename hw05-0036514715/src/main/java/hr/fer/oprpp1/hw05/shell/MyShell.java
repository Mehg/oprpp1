package hr.fer.oprpp1.hw05.shell;

import hr.fer.oprpp1.hw05.shell.commands.*;

import java.util.Collections;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Class which represents a shell
 */
public class MyShell {

    /**
     * Method which is used for parsing user input and executing commands
     */
    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {

            Environment environment = new MyShellEnv(sc);
            SortedMap<String, ShellCommand> commands = environment.commands();
            ShellStatus status;

            do {
                environment.write(environment.getPromptSymbol() + " ");

                String l = readLineOrLines(environment);

                String commandName = l.split(" ")[0];
                String arguments = "";
                if (!commandName.equals(l.strip())) {
                    arguments = l.split(" ", 2)[1].strip();
                }
                ShellCommand command = commands.get(commandName);
                if (command == null) {
                    environment.writeln("Command doesn't exist");
                    status = ShellStatus.CONTINUE;
                } else
                    status = command.executeCommand(environment, arguments);
            } while (status != ShellStatus.TERMINATE);

        } catch (ShellIOException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

    }

    /**
     * Method used for reading user input. Can be single line or multiple if user used MORELINES symbol
     *
     * @param environment current environment
     * @return new String representing user input
     */
    public static String readLineOrLines(Environment environment) {
        StringBuilder retVal = new StringBuilder();
        do {
            String l = environment.readLine();
            l = l.strip();


            if (!l.endsWith(String.valueOf(environment.getMorelinesSymbol()))) {
                retVal.append(l);
                break;
            }

            retVal.append(l, 0, l.length() - 1);

            environment.write(environment.getMultilineSymbol() + " ");

        } while (true);
        return retVal.toString();
    }

    /**
     * Implementation of an Environment
     */
    public static class MyShellEnv implements Environment {
        /**
         * Internal Scanner used for communicating with the user
         */
        private final Scanner sc;
        /**
         * Map of all commands
         */
        private final SortedMap<String, ShellCommand> commands;
        /**
         * Prompt symbol
         */
        private Character promptSymbol;
        /**
         * Multiline symbol
         */
        private Character multilineSymbol;
        /**
         * Symbol for more lines
         */
        private Character morelinesSymbol;

        /**
         * Creates a new MyShellEnv with given Scanner and initializes the commands map and symbols
         *
         * @param sc given Scanner
         */
        public MyShellEnv(Scanner sc) {
            this.sc = sc;
            this.promptSymbol = '>';
            this.multilineSymbol = '|';
            this.morelinesSymbol = '\\';
            this.commands = new TreeMap<>();

            commands.put("cat", new CatShellCommand());
            commands.put("charsets", new CharsetsShellCommand());
            commands.put("copy", new CopyShellCommand());
            commands.put("exit", new ExitShellCommand());
            commands.put("help", new HelpShellCommand());
            commands.put("hexdump", new HexdumpShellCommand());
            commands.put("ls", new LsShellCommand());
            commands.put("mkdir", new MkdirShellCommand());
            commands.put("symbol", new SymbolShellCommand());
            commands.put("tree", new TreeShellCommand());

            System.out.println("Welcome to MyShell v 1.0");
        }


        @Override
        public String readLine() throws ShellIOException {
            try {
                return sc.nextLine();
            } catch (Exception e) {
                throw new ShellIOException(e.getMessage());
            }
        }

        @Override
        public void write(String text) throws ShellIOException {
            try {
                System.out.print(text);
            } catch (Exception e) {
                throw new ShellIOException(e.getMessage());
            }
        }

        @Override
        public void writeln(String text) throws ShellIOException {
            try {
                System.out.println(text);
            } catch (Exception e) {
                throw new ShellIOException(e.getMessage());
            }
        }

        @Override
        public SortedMap<String, ShellCommand> commands() {
            return Collections.unmodifiableSortedMap(commands);
        }

        @Override
        public Character getMultilineSymbol() {
            return multilineSymbol;
        }


        @Override
        public void setMultilineSymbol(Character symbol) {
            this.multilineSymbol = symbol;
        }

        @Override
        public Character getPromptSymbol() {
            return promptSymbol;
        }

        @Override
        public void setPromptSymbol(Character symbol) {
            this.promptSymbol = symbol;
        }

        @Override
        public Character getMorelinesSymbol() {
            return morelinesSymbol;
        }

        @Override
        public void setMorelinesSymbol(Character symbol) {
            this.morelinesSymbol = symbol;
        }
    }
}
