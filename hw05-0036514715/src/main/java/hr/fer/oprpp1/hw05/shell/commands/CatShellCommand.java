package hr.fer.oprpp1.hw05.shell.commands;

import hr.fer.oprpp1.hw05.shell.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * ShellCommand used for reading file contend
 */
public class CatShellCommand implements ShellCommand {
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        List<String> argumentParts;
        try {
            PathParser p = new PathParser(arguments);
            argumentParts = p.getAllPaths();
        } catch (Exception e) {
            env.writeln("Invalid filename");
            return ShellStatus.CONTINUE;
        }

        if (argumentParts.size() != 1 && argumentParts.size() != 2)
            env.writeln("Invalid number of arguments - was " + argumentParts.size() + " expected 1 or 2");

        else {
            Charset c = argumentParts.size() == 1 ? Charset.defaultCharset() : Charset.forName(argumentParts.get(1));

            Path path = Paths.get(argumentParts.get(0));
            if (Files.isDirectory(path) || !Files.exists(path) || !Files.isReadable(path))
                env.writeln("File " + argumentParts.get(0) + " is not a readable file");

            try {
                Files.readAllLines(path, c).forEach(env::writeln);
            } catch (IOException e) {
                env.writeln("Error while trying to read file");
            }

        }
        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return "cat";
    }

    @Override
    public List<String> getCommandDescription() {
        return List.of("Command cat takes one or two arguments.",
                "The first argument is path to some file and is mandatory.",
                "The second argument is charset name that should be used to interpret chars from bytes.",
                "If not provided, a default platform charset will be used.",
                "This command opens given file and writes its content to console");
    }
}
