package hr.fer.oprpp1.hw05.shell;

/**
 * Enumeration for Shell which describes it's status
 * CONTINUE meaning the Shell should continue working
 * TERMINATE meaning the Shell should be terminated
 */
public enum ShellStatus {
    CONTINUE, TERMINATE
}
