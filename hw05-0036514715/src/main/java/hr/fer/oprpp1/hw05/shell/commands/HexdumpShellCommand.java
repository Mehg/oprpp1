package hr.fer.oprpp1.hw05.shell.commands;

import hr.fer.oprpp1.hw05.crypto.Util;
import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.PathParser;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * ShellCommand used for producing hex-output
 */
public class HexdumpShellCommand implements ShellCommand {
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        List<String> argumentParts;
        try {
            PathParser p = new PathParser(arguments);
            argumentParts = p.getAllPaths();
        } catch (Exception e) {
            env.writeln("Invalid filename");
            return ShellStatus.CONTINUE;
        }

        if (argumentParts.size() != 1)
            env.writeln("Invalid number of arguments - was " + argumentParts.size() + " expected 1");
        else {

            Path path = Paths.get(argumentParts.get(0));
            if (Files.isDirectory(path) || !Files.exists(path) || !Files.isReadable(path)) {
                env.writeln("Can't generate hex-output from file " + argumentParts.get(0));
                return ShellStatus.CONTINUE;
            }

            try (InputStream is = Files.newInputStream(path)) {
                byte[] bytearray = new byte[16];
                int line = 0;
                while (true) {
                    int size = is.read(bytearray);
                    if (size == -1) break;

                    String currentLine = "0".repeat(8 - Integer.toHexString(line).length()) + Integer.toHexString(line);

                    env.write(currentLine + ": ");
                    String hex = Util.bytetohex(bytearray);

                    for (int i = 0; i < bytearray.length; i++) {
                        env.write(hex.charAt(i) + "" + hex.charAt(i + 1));
                        if (i == bytearray.length / 2)
                            env.write("|");
                        else env.write(" ");
                    }
                    env.write(" | ");

                    for (byte b : bytearray) {
                        if ((char) b < 32 || (char) b > 127)
                            env.write(".");
                        else
                            env.write(String.valueOf((char) b));
                    }

                    line += 16;

                    env.writeln("");

                }


            } catch (Exception e) {
                env.writeln("Error occurred while trying to read from file");
            }
        }

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return "hexdump";
    }

    @Override
    public List<String> getCommandDescription() {
        return List.of("The command expects a single argument: file name, and produces hex-output.");
    }
}
