package hr.fer.oprpp1.hw05.shell.commands;

import hr.fer.oprpp1.hw05.shell.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * ShellCommand used for listing files in current directory
 */
public class LsShellCommand implements ShellCommand {
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        List<String> argumentParts;
        try {
            PathParser p = new PathParser(arguments);
            argumentParts = p.getAllPaths();
        } catch (Exception e) {
            env.writeln("Invalid filename");
            return ShellStatus.CONTINUE;
        }

        if (argumentParts.size() != 1)
            env.writeln("Invalid number of arguments - was " + argumentParts.size() + " expected 1");

        else {

            Path path = Paths.get(argumentParts.get(0));

            if (!Files.isDirectory(path) || !Files.exists(path)) {
                env.writeln("File " + argumentParts.get(0) + " can't be listed");
                return ShellStatus.CONTINUE;
            }


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Files.list(path).forEach(f -> {
                    BasicFileAttributeView faView = Files.getFileAttributeView(
                            f, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS
                    );
                    try {
                        BasicFileAttributes attributes = faView.readAttributes();
                        FileTime fileTime = attributes.creationTime();
                        String formattedDateTime = sdf.format(new Date(fileTime.toMillis()));

                        char d = attributes.isDirectory() ? 'd' : '-';
                        char r = Files.isReadable(f) ? 'r' : '-';
                        char w = Files.isWritable(f) ? 'w' : '-';
                        char x = Files.isExecutable(f) ? 'x' : '-';

                        long size = Files.size(f);


                        String output = String.format("%c%c%c%c%10d %s %s", d, r, w, x, size, formattedDateTime, f.getFileName());
                        env.writeln(output);

                    } catch (IOException e) {
                        env.writeln("Error while trying to get attributes from file");
                    }

                });
            } catch (Exception e) {
                env.writeln("Error while trying to read files");
            }

        }

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return "ls";
    }

    @Override
    public List<String> getCommandDescription() {
        return List.of("Command takes a single argument – directory – and writes a directory listing (not recursive).",
                "The output consists of 4 columns.",
                "First column indicates if current object is directory (d), readable (r), writable (w) and executable (x).",
                "Second column contains object size in bytes.",
                "Third column contains file creation date/time",
                "Fourth column contains file name.");
    }
}
