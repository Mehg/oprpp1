package hr.fer.oprpp1.hw05.shell;

/**
 * Exception which can happen in Shell
 */
public class ShellIOException extends RuntimeException {
    public ShellIOException(String message) {
        super(message);
    }
}
