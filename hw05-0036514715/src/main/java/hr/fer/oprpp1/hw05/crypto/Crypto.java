package hr.fer.oprpp1.hw05.crypto;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Scanner;


/**
 * Class used for checking SHA-256, encrypting and decrypting files
 */
public class Crypto {
    /**
     * Executes command which user defined via command line
     * If user specifies the method 'checksha' it is expected that one argument is inputted. Returns the sha for given file
     * If user specifies the method 'encrypt' or 'decrypt' it is expected that two arguments are inputted. Encrypts/Decrypts given file.
     *
     * @param args - args[0] specifies which method is used and gives arguments
     */
    public static void main(String[] args) {
        if (args.length == 0)
            System.out.println("Please provide valid input: checksha/encrypt/decrypt followed by arguments");

        String method = args[0].split(" ")[0];
        String arguments = args[0].split(" ", 2)[1];

        try {
            switch (method) {
                case "checksha" -> checksha(arguments);
                case "encrypt" -> endecrypt(arguments.split(" ")[0], arguments.split(" ")[1], true);
                case "decrypt" -> endecrypt(arguments.split(" ")[0], arguments.split(" ")[1], false);
                default -> {
                    System.out.println("Invalid method");
                    System.exit(1);
                }
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Invalid number of arguments for method " + method);
            System.exit(1);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }

    }

    /**
     * Method which asks the user for expected sha-256 digest and compares it to calculated sha-256 digest for given file
     *
     * @param bin given file
     * @throws NoSuchAlgorithmException - if no such algorithm exists
     */
    public static void checksha(String bin) throws NoSuchAlgorithmException {
        System.out.println("Please provide expected sha-256 digest for " + bin + ":");
        System.out.print("> ");

        Scanner sc = new Scanner(System.in);
        String expected = sc.next();
        sc.close();

        StringBuilder sb = new StringBuilder();
        sb.append("Digesting completed. ");

        MessageDigest md = MessageDigest.getInstance("SHA-256");

        read(bin, md);

        String actual = Util.bytetohex(md.digest());

        if (actual.equals(expected)) sb.append("Digest of ").append(bin).append(" matches expected digest.");
        else
            sb.append("Digest of ").append(bin).append(" does not match the expected digest. Digest was: ").append(actual);

        System.out.println(sb.toString());

    }

    /**
     * Encrypts or decrypts given file and creates second file. It will ask the user to provide a password
     * and initialization vector
     *
     * @param first   first file
     * @param second  second file
     * @param encrypt true for encryption, false for decryption
     * @throws NoSuchPaddingException             if an Exception occurred
     * @throws NoSuchAlgorithmException           if an Exception occurred
     * @throws InvalidAlgorithmParameterException if an Exception occurred
     * @throws InvalidKeyException                if an Exception occurred
     */
    public static void endecrypt(String first, String second, boolean encrypt) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        System.out.println("Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):");
        System.out.print("> ");

        Scanner sc = new Scanner(System.in);
        String password = sc.next();

        System.out.println("Please provide initialization vector as hex-encoded text (32 hex-digits):");
        System.out.print("> ");

        String vector = sc.next();
        sc.close();

        SecretKeySpec keySpec = new SecretKeySpec(Util.hextobyte(password), "AES");
        AlgorithmParameterSpec paramSpec = new IvParameterSpec(Util.hextobyte(vector));
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);

        createNewFile(first, second, cipher);

        System.out.println((encrypt ? "Encryption " : "Decryption ") +
                "completed. " +
                "Generated file " +
                second +
                " based on file " +
                first);

    }

    /**
     * Reads 4kB at the time and creates a new MessageDigest for given file
     *
     * @param filename given file
     * @param md       given MessageDigest
     */
    private static void read(String filename, MessageDigest md) {
        if (!Files.exists(Paths.get(filename))) {
            System.out.println("File doesn't exist");
            System.exit(1);
        }
        try (InputStream is = Files.newInputStream(Paths.get(filename))) {
            byte[] bytearray = new byte[4096];

            while (true) {
                int size = is.read(bytearray);
                if (size == -1) break;
                md.update(bytearray, 0, size);
            }

        } catch (IOException e) {
            System.out.println("error occurred while reading file");
            System.exit(0);
        }
    }

    /**
     * Creates new file (second) either encrypted or decrypted from given file
     *
     * @param first  first file
     * @param second second file
     * @param cipher given Cipher
     */
    private static void createNewFile(String first, String second, Cipher cipher) {
        if (!Files.exists(Paths.get(first))) {
            System.out.println("File doesn't exist");
            System.exit(1);
        }
        try (OutputStream os = Files.newOutputStream(Paths.get(second))) {
            try (InputStream is = Files.newInputStream(Paths.get(first))) {
                byte[] bytearray = new byte[4096];

                while (true) {
                    int size = is.read(bytearray);
                    if (size == -1) break;
                    os.write(cipher.update(bytearray, 0, size));
                }
                os.write(cipher.doFinal());
            }
        } catch (IOException | IllegalBlockSizeException | BadPaddingException e) {
            System.out.println("error occurred while reading file");
            System.exit(0);
        }

    }
}
