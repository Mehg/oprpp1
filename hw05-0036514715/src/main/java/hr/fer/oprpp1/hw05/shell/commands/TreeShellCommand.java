package hr.fer.oprpp1.hw05.shell.commands;

import hr.fer.oprpp1.hw05.shell.*;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

/**
 * ShellCommand used for printing the tree from given directory
 */
public class TreeShellCommand implements ShellCommand {
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        List<String> argumentParts;
        try {
            PathParser p = new PathParser(arguments);
            argumentParts = p.getAllPaths();
        } catch (Exception e) {
            env.writeln("Invalid filename");
            return ShellStatus.CONTINUE;
        }

        if (argumentParts.size() != 1)
            env.writeln("Invalid number of arguments - was " + argumentParts.size() + " expected 1");

        else {

            Path path = Paths.get(argumentParts.get(0));

            if (!Files.isDirectory(path) || !Files.exists(path))
                env.writeln("File " + argumentParts.get(0) + " can't be listed");
            try {
                Files.walkFileTree(path, new FileVisitor<>() {
                    private int level = 0;

                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
                        env.write(" ".repeat(level++));
                        env.writeln(dir.getFileName().toString());
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                        env.write(" ".repeat(level));
                        env.writeln(file.getFileName().toString());
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFileFailed(Path file, IOException exc) {
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
                        level--;
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (Exception e) {
                env.writeln("Error while trying to walk tree");
            }

        }

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return "tree";
    }

    @Override
    public List<String> getCommandDescription() {
        return List.of("The command expects a single argument: directory name and prints a tree.",
                "Each directory level shifts output two characters to the right.");
    }
}
