package hr.fer.oprpp1.hw05.shell;

import java.util.SortedMap;

/**
 * Interface which all environment should implement
 */
public interface Environment {
    /**
     * Reads next user input
     *
     * @return String which represents next user input
     * @throws ShellIOException if an exception occurred during reading
     */
    String readLine() throws ShellIOException;

    /**
     * Writes given text to user
     *
     * @param text given text
     * @throws ShellIOException if an exception occurred during writing
     */
    void write(String text) throws ShellIOException;

    /**
     * Writes given text to user and terminates the line
     *
     * @param text given text
     * @throws ShellIOException if an exception occurred during writing
     */
    void writeln(String text) throws ShellIOException;

    /**
     * Returns the current map of commands
     *
     * @return the current map of commands
     */
    SortedMap<String, ShellCommand> commands();

    /**
     * Returns the current multiline symbol
     *
     * @return the current multiline symbol
     */
    Character getMultilineSymbol();

    /**
     * Sets the current multiline symbol to given symbol
     *
     * @param symbol given symbol
     */
    void setMultilineSymbol(Character symbol);

    /**
     * Returns the current prompt symbol
     *
     * @return the current prompt symbol
     */
    Character getPromptSymbol();

    /**
     * Sets the current prompt symbol to given symbol
     *
     * @param symbol given symbol
     */
    void setPromptSymbol(Character symbol);

    /**
     * Returns the current symbol for more lines
     *
     * @return the current symbol for more lines
     */
    Character getMorelinesSymbol();

    /**
     * Sets the current symbol for more lines to given symbol
     *
     * @param symbol given symbol
     */
    void setMorelinesSymbol(Character symbol);
}
