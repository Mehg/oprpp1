package hr.fer.oprpp1.hw05.shell.commands;

import hr.fer.oprpp1.hw05.shell.*;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


/**
 * ShellCommand used for copying files
 */
public class CopyShellCommand implements ShellCommand {
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        List<String> argumentParts;
        try {
            PathParser p = new PathParser(arguments);
            argumentParts = p.getAllPaths();
        } catch (Exception e) {
            env.writeln("Invalid filename");
            return ShellStatus.CONTINUE;
        }

        if (argumentParts.size() != 2)
            env.writeln("Invalid number of arguments - was " + argumentParts.size() + " expected 2");

        Path src = Paths.get(argumentParts.get(0));
        Path dest = Paths.get(argumentParts.get(1));
        try {
            if (Files.exists(dest) && !Files.isDirectory(dest)) {
                env.writeln("File " + dest.getFileName() + " already exists. Do you want to overwrite it? [yes/no]");
                String answer = env.readLine();
                answer = answer.strip().toLowerCase();
                if (!answer.equals("yes"))
                    return ShellStatus.CONTINUE;
                Files.delete(dest);
            } else if (Files.isDirectory(dest)) {
                dest = Paths.get(dest.toString(), src.getFileName().toString());
            }

            InputStream is = Files.newInputStream(src);
            OutputStream os = Files.newOutputStream(dest);

            byte[] bytearray = new byte[4096];

            while (true) {
                int size = is.read(bytearray);
                if (size == -1) break;
                os.write(bytearray, 0, size);
            }

            is.close();
            os.close();

        } catch (Exception e) {
            env.writeln("Error while trying to copy file");
        }


        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return "copy";
    }

    @Override
    public List<String> getCommandDescription() {
        return List.of("The copy command expects two arguments: source file name and destination file name.",
                "If destination file exists, asks user if it is allowed to overwrite it.",
                "If the second argument is directory, copies the original file into that directory using the original file name.");
    }
}
