package hr.fer.oprpp1.hw05.shell;

import java.util.List;

/**
 * Interface which should be implemented by all ShellCommands
 */
public interface ShellCommand {
    /**
     * Executes current command
     *
     * @param env       given environment
     * @param arguments given arguments
     * @return new ShellStatus
     */
    ShellStatus executeCommand(Environment env, String arguments);

    /**
     * Returns the name of current command
     *
     * @return the name of current command
     */
    String getCommandName();

    /**
     * Returns the description of current command
     *
     * @return the description of current command
     */
    List<String> getCommandDescription();
}
