package hr.fer.oprpp1.hw05.shell.commands;

import hr.fer.oprpp1.hw05.shell.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * ShellCommand used for creating a directory
 */
public class MkdirShellCommand implements ShellCommand {
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        List<String> argumentParts;
        try {
            PathParser p = new PathParser(arguments);
            argumentParts = p.getAllPaths();
        } catch (Exception e) {
            env.writeln("Invalid filename");
            return ShellStatus.CONTINUE;
        }

        if (argumentParts.size() != 1)
            env.writeln("Invalid number of arguments - was " + argumentParts.size() + " expected 1");

        Path path = Path.of(argumentParts.get(0));
        if (Files.exists(path))
            env.writeln("Directory already exists");
        try {
            Files.createDirectory(path);
        } catch (Exception e) {
            env.writeln("Error while trying to create a directory");
        }

        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return "mkdir";
    }

    @Override
    public List<String> getCommandDescription() {
        return List.of("The command takes a single argument: directory name, and creates the appropriate directory");
    }
}
