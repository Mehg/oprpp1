package hr.fer.oprpp1.hw05.shell.commands;

import hr.fer.oprpp1.hw05.shell.Environment;
import hr.fer.oprpp1.hw05.shell.ShellCommand;
import hr.fer.oprpp1.hw05.shell.ShellStatus;

import java.util.List;

/**
 * ShellCommand used for listing all supported commands, and printing the description of specified commands
 */
public class HelpShellCommand implements ShellCommand {
    @Override
    public ShellStatus executeCommand(Environment env, String arguments) {
        if (!arguments.isBlank()) {
            ShellCommand c = env.commands().get(arguments.strip());
            if (c == null) {
                env.writeln("Command " + arguments + " doesn't exist");
            } else {
                env.writeln(c.getCommandName());
                for (String s : c.getCommandDescription())
                    env.writeln(s);
            }

        } else {
            for (ShellCommand c : env.commands().values()) {
                env.writeln(c.getCommandName());
            }
        }
        return ShellStatus.CONTINUE;
    }

    @Override
    public String getCommandName() {
        return "help";
    }

    @Override
    public List<String> getCommandDescription() {
        return List.of("The command expects 0 or 1 arguments.",
                "If started with no arguments, it lists names of all supported commands",
                "If started with single argument, it prints the name and the description of selected command");
    }
}
