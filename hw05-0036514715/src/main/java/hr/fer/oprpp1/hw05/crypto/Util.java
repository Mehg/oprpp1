package hr.fer.oprpp1.hw05.crypto;

/**
 * Helper class for converting hex Strings to byte arrays, and byte arrays to hex Strings
 */
public class Util {
    /**
     * Creates new byte array from given hexString
     *
     * @param keyText hex String
     * @return new byte array
     * @throws NullPointerException     if given text is null
     * @throws IllegalArgumentException if given text is of odd length or contains non hex characters
     */
    public static byte[] hextobyte(String keyText) {
        if (keyText == null) throw new NullPointerException("keyText must not be null");
        if (keyText.length() % 2 == 1) throw new IllegalArgumentException("keyText must be even");
        if (!keyText.matches("[a-fA-F0-9]*")) throw new IllegalArgumentException("keyText isn't hex");

        String workingString = keyText.toLowerCase();
        byte[] bytearray = new byte[keyText.length() / 2];

        for (int i = 0, j = 0; i < keyText.length(); i += 2, j++) {
            int firstDigit = Character.digit(workingString.charAt(i), 16);
            int secondDigit = Character.digit(workingString.charAt(i + 1), 16);
            bytearray[j] = (byte) ((firstDigit << 4) + secondDigit);
        }

        return bytearray;
    }

    /**
     * Creates new hex String from given byte array
     *
     * @param bytearray given byte array
     * @return new hex String
     * @throws NullPointerException if given byte array is null
     */
    public static String bytetohex(byte[] bytearray) {
        if (bytearray == null) throw new NullPointerException("bytearray must not be null");
        StringBuilder sb = new StringBuilder();
        for (byte b : bytearray) {
            char[] hexDigits = new char[2];
            hexDigits[0] = Character.forDigit((b >> 4) & 0xF, 16);
            hexDigits[1] = Character.forDigit((b & 0xF), 16);
            sb.append(new String(hexDigits));

        }
        return sb.toString();
    }

}
