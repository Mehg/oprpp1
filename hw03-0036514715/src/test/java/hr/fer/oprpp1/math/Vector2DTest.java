package hr.fer.oprpp1.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class Vector2DTest {
    @Test
    public void testVectorCreate() {
        var vector = new Vector2D(2.2, 10);

        assertEquals(2.2, vector.getX());
        assertEquals(10, vector.getY());
    }

    @Test
    public void testVectorAdd() {
        var vector = new Vector2D(2.2, 10);
        vector.add(new Vector2D(1.1, 100));

        assertTrue(3.3 - Math.abs(vector.getX()) < 1E-8);
        assertTrue(110 - Math.abs(vector.getY()) < 1E-8);
    }

    @Test
    public void testVectorAdded() {
        var vector = new Vector2D(2.2, 10);
        var newVector = vector.added(new Vector2D(1.1, 100));

        assertTrue(2.2 - Math.abs(vector.getX()) < 1E-8);
        assertTrue(10 - Math.abs(vector.getY()) < 1E-8);

        assertTrue(3.3 - Math.abs(newVector.getX()) < 1E-8);
        assertTrue(110 - Math.abs(newVector.getY()) < 1E-8);
    }

    @Test
    public void testVectorCopy() {
        var vector = new Vector2D(2.2, 2.2);
        var newVector = vector.copy();

        assertNotSame(newVector, vector);
    }

    @Test
    public void testVectorScale() {
        var vector = new Vector2D(2.2, 2.2);
        vector.scale(2);

        assertTrue(4.4 - Math.abs(vector.getX()) < 1E-8);
        assertTrue(4.4 - Math.abs(vector.getY()) < 1E-8);
    }

    @Test
    public void testVectorScaled() {
        var vector = new Vector2D(2.2, 2.2);
        var newVector = vector.scaled(2);

        assertNotSame(newVector, vector);
        assertTrue(4.4 - Math.abs(newVector.getX()) < 1E-8);
        assertTrue(4.4 - Math.abs(newVector.getY()) < 1E-8);
    }

    @Test
    void testGetX() {
        assertEquals(5, new Vector2D(5, 10).getX());
    }

    @Test
    void testGetY() {
        assertEquals(10, new Vector2D(5, 10).getY());
    }

    @Test
    void testRotate() {
        var vector = new Vector2D(5, 10);

        vector.rotate(Math.PI);

        assertEquals(new Vector2D(-5, -10), vector);
    }

    @Test
    void testRotated() {
        assertEquals(new Vector2D(-5, -10), new Vector2D(5, 10).rotated(Math.PI));
        assertEquals(new Vector2D(-5, -10), new Vector2D(-5, -10).rotated(2 * Math.PI));
        assertEquals(new Vector2D(-5, -10), new Vector2D(10, -5).rotated(-Math.PI / 2));
        assertEquals(new Vector2D(-5, -10), new Vector2D(-10, 5).rotated(Math.PI / 2));
    }
}