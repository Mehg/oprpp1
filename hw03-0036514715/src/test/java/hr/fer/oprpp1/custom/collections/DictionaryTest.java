package hr.fer.oprpp1.custom.collections;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DictionaryTest {
    
    //empty
    
    @Test
    public void testEmpty() {
        Dictionary<Integer, Integer> test = new Dictionary<>();
        assertTrue(test.isEmpty());
    }

    @Test
    void testIsEmptyWithNotEmpty() {
        Dictionary<Integer,Integer> test = new Dictionary<>();
        test.put(1,1);
        assertFalse(test.isEmpty());
    }

    @Test
    public void testEmptyClear() {
        Dictionary<Integer, Integer> test = new Dictionary<>();

        test.put(1, 1);
        test.put(2, 2);

        test.clear();
        assertTrue(test.isEmpty());
    }

    //size

    @Test
    public void testGetSizeEmpty() {
        assertEquals(0, new Dictionary<Integer, Integer>().size());
    }

    @Test
    public void testGetSize() {
        Dictionary<Integer, Integer> test = new Dictionary<>();

        test.put(1, 1);
        test.put(2, 2);

        assertEquals(2, test.size());
    }


    //put

    @Test
    void testPutNullKey() {
        Dictionary<Integer,Integer> test = new Dictionary<>();
        assertThrows(NullPointerException.class, () -> test.put(null, 3));
    }

    @Test
    void testPutWithDuplicates() {
        Dictionary<Integer,Integer> test = new Dictionary<>();

        assertEquals(0, test.size());

        test.put(1, 1);
        test.put(1, 2);

        assertEquals(1, test.size());
        assertEquals(2, test.get(1));
    }


    @Test
    public void testPutOverWrites() {
        Dictionary<Integer, Integer> test = new Dictionary<>();

        test.put(1, 1);
        test.put(2, 2);
        test.put(1, 3);

        assertEquals(3, test.get(1));
    }

    //get

    @Test
    public void testGet() {
        Dictionary<Integer, Integer> test = new Dictionary<>();

        test.put(1, 1);
        test.put(2, 2);

        assertEquals(1, test.get(1));
    }

    //remove

    @Test
    public void testRemoveGetsOldValue() {
        Dictionary<Integer, Integer> test = new Dictionary<>();

        test.put(1, 1);
        test.put(2, 2);

        assertEquals(1, test.remove(1));
    }

    @Test
    public void testRemove() {
        Dictionary<Integer, Integer> test = new Dictionary<>();

        test.put(1, 1);
        test.put(2, 2);

        test.remove(1);

        assertEquals(1, test.size());
    }

}
