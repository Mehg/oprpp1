package hr.fer.oprpp1.math;


/**
 * Implementation of a 2D vector.
 */
public class Vector2D {
    private double x;
    private double y;

    /**
     * Creates a new vector from given parameter
     * @param x value of x component
     * @param y value of y component
     */
    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns the value of x component
     * @return the value of x component
     */
    public double getX() {
        return x;
    }

    /**
     * Returns the value of y component
     * @return the value of y component
     */
    public double getY() {
        return y;
    }

    /**
     * Adds offset to current vector
     * @param offset vector which will be added to current vector
     */
    public void add(Vector2D offset) {
        this.x = this.x + offset.x;
        this.y = this.y + offset.y;
    }

    /**
     * Creates and returns a new vector which is the result of adding this vector to offset vector
     * @param offset vector which will be added to current vector
     * @return new vector which is the sum of this and offset vector
     */
    public Vector2D added(Vector2D offset) {
        Vector2D v = this.copy();
        v.add(offset);
        return v;
    }

    /**
     * Rotates current vector for given angle in radians
     * @param angle angle in radians
     */
    public void rotate(double angle) {
        double scale = Math.pow(10, 4);
        double nX =  this.x * Math.cos(angle) - this.y * Math.sin(angle);
        double nY= this.x * Math.sin(angle) + this.y * Math.cos(angle);
        this.x = Math.round(scale*nX)/scale;
        this.y = Math.round(scale*nY)/scale;
    }

    /**
     * Creates and returns a new vector which is the result of rotating current vector for given angle
     * @param angle angle in radians
     * @return new vector which is the rotated current vector for given angle
     */
    public Vector2D rotated(double angle) {
        Vector2D v = this.copy();
        v.rotate(angle);
        return v;
    }

    /**
     * Scales current vector for given scaler
     * @param scalar scalar
     */
    public void scale(double scalar) {
        this.x *= scalar;
        this.y *= scalar;
    }

    /**
     * Creates and returns new vector which is the result of scaling current vector for given scaler
     * @param scalar scalar
     * @return new vector which is the scaled current vector for given scalar
     */
    public Vector2D scaled(double scalar) {
        Vector2D v = this.copy();
        v.scale(scalar);
        return v;
    }

    /**
     * Creates and returns a new copy of current vector
     * @return new vector with values same as current vector
     */
    public Vector2D copy() {
        return new Vector2D(this.x, this.y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector2D vector2D = (Vector2D) o;
        return Double.compare(vector2D.x, x) == 0 &&
                Double.compare(vector2D.y, y) == 0;
    }

}
