package hr.fer.oprpp1.custom.collections;

import java.util.*;

/**
 * Implementation of a simple hash table
 *
 * @param <K> class of key
 * @param <V> class of value
 */
public class SimpleHashtable<K, V> implements Iterable<SimpleHashtable.TableEntry<K, V>> {
    /**
     * Class which represents a table entry
     *
     * @param <K> class of key
     * @param <V> class of value
     */
    public static class TableEntry<K, V> {
        private final K key;
        private V value;
        private TableEntry<K, V> next;

        /**
         * Returns the value of current TableEntry
         *
         * @return value
         */
        public V getValue() {
            return value;
        }

        /**
         * Sets the value of current TableEntry to given value
         *
         * @param value given value
         */
        public void setValue(V value) {
            this.value = value;
        }

        /**
         * Returns the key of current TableEntry
         *
         * @return key
         */
        public K getKey() {
            return key;
        }

        /**
         * Creates a new TableEntry for given parameters
         *
         * @param key   key
         * @param value value
         * @param next  next TableEntry
         */
        public TableEntry(K key, V value, TableEntry<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TableEntry<?, ?> that = (TableEntry<?, ?>) o;
            return key.equals(that.key) &&
                    Objects.equals(value, that.value);
        }

        @Override
        public int hashCode() {
            return Math.abs(key.hashCode());
        }
    }

    private TableEntry<K, V>[] table;
    private int size;
    private boolean modified = false;

    /**
     * Creates new SimpleHashtable with capacity set to 16. Capacity is the number of rows
     */
    public SimpleHashtable() {
        this(16);
    }

    /**
     * Creates new SimpleHashtable from given capacity.
     * Capacity of the new SimpleHashTable will be the first power of 2 greater or equal to given capacity
     *
     * @param capacity given capacity
     */
    public SimpleHashtable(int capacity) {
        if (capacity < 1) throw new IllegalArgumentException("Capacity is less than 1");
        table = (TableEntry<K, V>[]) new TableEntry[(int) Math.pow(2, Math.ceil(Math.log(capacity) / Math.log(2)))];
        size = 0;
    }

    @Override
    public Iterator<SimpleHashtable.TableEntry<K, V>> iterator() {
        modified = false;
        return new IteratorImpl();
    }

    /**
     * Creates and inserts a new TableEntry with given key and value in SimpleHashtable.
     * If an entry with given key already exists the old value will be replaced with the new one and the old one will be returned.
     * If the SimpleHashtable doesn't contain an entry with given key, null will be returned
     * The position at which the entry will be set is determined by hashing the key.
     * If the table is occupied 75% or more double the current capacity
     *
     * @param key   given key
     * @param value given value
     * @return old value if the SimpleHashtable already contains an entry with given key, otherwise null
     * @throws NullPointerException is given key is null
     */
    public V put(K key, V value) {
        if (key == null) throw new NullPointerException("Key must not be null");
        if (1.0 * (size+1) / getCapacity() >= 0.75) reallocate();
        modified = true;

        TableEntry<K, V> entry = new TableEntry<>(key, value, null);
        TableEntry<K, V> oldEntry;

        if (containsKey(entry.getKey())) {
            oldEntry = getEntry(entry.key);
            V retVal = oldEntry.getValue();
            oldEntry.setValue(entry.getValue());
            return retVal;
        }

        int position = calculatePosition(entry);
        oldEntry = table[position];

        if (table[position] == null) {
            table[position] = entry;
            size++;
            return null;
        }

        while (oldEntry.next != null) {
            oldEntry = oldEntry.next;
        }
        oldEntry.next = entry;
        size++;


        return null;
    }


    /**
     * Searches for an entry with given key and returns its value. If such entry doesn't exist returns null
     *
     * @param key given key
     * @return value for given key if entry with given key exists, otherwise null
     */
    public V get(Object key) {
        TableEntry<K, V> entry = getEntry((K) key);
        if (entry != null) return entry.value;
        return null;
    }

    /**
     * Returns the number of currently stored TableEntries
     *
     * @return the number of currently stored elements
     */
    public int size() {
        return size;
    }


    /**
     * Checks if SimpleHashtable contains an entry with given key.
     *
     * @param key given key
     * @return true if SimpleHashtable contains entry with given key, otherwise false
     */
    public boolean containsKey(Object key) {
        if (key == null) return false;
        int position = calculatePosition((K) key);
        TableEntry<K, V> entry = table[position];

        while (entry != null) {
            if (entry.getKey().equals(key)) {
                return true;
            }
            entry = entry.next;
        }
        return false;
    }

    /**
     * Checks if SimpleHashtable contains an entry with given value.
     *
     * @param value given value
     * @return true if SimpleHashtable contains entry with given value, otherwise false
     */
    public boolean containsValue(Object value) {
        for (int i = 0; i < getCapacity(); i++) {
            TableEntry<K, V> entry = table[i];
            while (entry != null) {
                if (entry.getValue() == null || entry.getValue().equals(value))
                    return true;
                entry = entry.next;
            }
        }
        return false;
    }

    /**
     * Removes an entry from SimpleHashtable for given key if such entry exists
     *
     * @param key given key
     * @return old value if successfully removes, otherwise null
     */
    public V remove(Object key) {
        if (key == null)
            return null;
        modified = true;
        int position = calculatePosition((K) key);
        TableEntry<K, V> entry = this.table[position];

        if (entry == null) return null;
        if (entry.getKey().equals(key)) {
            this.table[position] = entry.next;
            V retVal = entry.getValue();
            this.size--;
            return retVal;
        }

        while (entry.next != null) {
            if (entry.next.getKey().equals(key)) {
                entry.next = entry.next.next;
                V retval = entry.next.getValue();
                this.size--;
                return retval;
            }
            entry = entry.next;
        }
        return null;
    }

    /**
     * Checks if SimpleHashtable is empty
     *
     * @return true if empty, otherwise false
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Private helper method which returns Entry from given key
     *
     * @param key given key
     * @return entry from given key
     */
    private TableEntry<K, V> getEntry(K key) {
        if (key == null) return null;
        int position = calculatePosition((K) key);
        TableEntry<K, V> entry = table[position];

        while (entry != null) {
            if (entry.getKey().equals(key)) {
                return entry;
            }
            entry = entry.next;
        }
        return null;
    }

    /**
     * Private helper method with returns the row at which the entry should be
     *
     * @param entry given entry
     * @return row
     */
    private int calculatePosition(TableEntry<K, V> entry) {
        return Math.abs(entry.hashCode()) % getCapacity();
    }

    /**
     * Private helper method with returns the row at which the entry should be
     *
     * @param key given key
     * @return row
     */
    private int calculatePosition(K key) {
        return Math.abs(key.hashCode()) % getCapacity();
    }

    /**
     * Returns current capacity - row count
     *
     * @return current capacity
     */
    public int getCapacity() {
        return table.length;
    }

    /**
     * Private heper method which doubles the capacity of current array
     */
    private void reallocate() {
        Object[] array = this.toArray();
        this.clear();
        table = (TableEntry<K, V>[]) new TableEntry[this.getCapacity() * 2];
        for (Object o : array) {
            if (o != null) {
                TableEntry<K, V> entry = (TableEntry<K, V>) o;
                this.put(entry.key, entry.value);
            }
        }
    }

    /**
     * Removes all TableEntries from SimpleHashtable
     */
    public void clear() {
        modified = true;
        for (int i = 0; i < getCapacity(); i++)
            table[i] = null;
        size = 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");

        boolean first = true;
        for (int i = 0; i < getCapacity(); i++) {
            TableEntry<K, V> entry = table[i];
            while (entry != null) {
                if (first) {
                    sb.append(entry.getKey()).append("=").append(entry.getValue());
                    first = false;
                    entry = entry.next;
                } else {
                    sb.append(", ").append(entry.getKey()).append("=").append(entry.getValue());
                    entry = entry.next;
                }
            }
        }
        sb.append("]");
        return sb.toString();
    }

    /**
     * Creates an array from current SimpleHashtable
     *
     * @return array representation of SimpleHashtable
     */
    public TableEntry<K, V>[] toArray() {
        TableEntry<K, V>[] array = new TableEntry[size];
        for (int i = 0, j = 0; i < getCapacity(); i++) {
            TableEntry<K, V> entry = table[i];
            while (entry != null) {
                array[j++] = entry;
                entry = entry.next;
            }
        }
        return array;
    }

    /**
     * Private class which is an implementation of an iterator for SimpleHashtable
     */
    private class IteratorImpl implements Iterator<SimpleHashtable.TableEntry<K, V>> {
        private int processed;
        private TableEntry<K, V> currentEntry;
        private TableEntry<K, V> nextEntry;
        private boolean removed = false;

        /**
         * Creates a new IteratorImpl
         */
        public IteratorImpl() {
            for (int i = 0; i < getCapacity(); i++)
                if (table[i] != null) {
                    nextEntry = table[i];
                    break;
                }

            processed = 0;
        }

        /**
         * Checks if SimpleHashtable has next element
         *
         * @return true if next element is not null, otherwise false
         * @throws ConcurrentModificationException if SimpleHashtable was modified while iterating
         */
        public boolean hasNext() {
            if (modified) throw new ConcurrentModificationException("Array modified while iterating");
            return processed < size;
        }

        /**
         * Returns next element
         *
         * @return next <code>TableEntry<K,V></code>
         * @throws ConcurrentModificationException if SimpleHashtable was modified while iterating
         * @throws NoSuchElementException          if there are no elements left
         */
        public SimpleHashtable.TableEntry<K, V> next() {
            if (modified) throw new ConcurrentModificationException("Array modified while iterating");
            if (!hasNext()) throw new NoSuchElementException("There are no elements left");
            currentEntry = nextEntry;
            if (currentEntry.next != null) {
                nextEntry = currentEntry.next;
            } else {
                int currentSlot = calculatePosition(currentEntry.getKey());
                for (int i = currentSlot + 1; i < getCapacity(); i++)
                    if (table[i] != null) {
                        nextEntry = table[i];
                        break;
                    }
            }
            processed++;
            removed = false;
            return currentEntry;
        }

        /**
         * Removes current TableEntry from SimpleHashtable
         *
         * @throws ConcurrentModificationException if SimpleHashtable was modified while iterating
         * @throws IllegalStateException           if method was called more than once on same element
         */
        public void remove() {
            if (modified) throw new ConcurrentModificationException("Array modified while iterating");
            if (removed) throw new IllegalStateException("Element already removed");
            SimpleHashtable.this.remove(currentEntry.getKey());
            processed--;
            modified = false;
            removed = true;
        }
    }
}
