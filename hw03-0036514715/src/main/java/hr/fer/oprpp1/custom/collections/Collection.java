package hr.fer.oprpp1.custom.collections;

/**
 * Interface which represents some general collection of type <code>T</code>
 */
public interface Collection<T> {
    /**
     * Checks whether the collection is empty
     *
     * @return true if collection contains no objects and false otherwise
     */
    default boolean isEmpty() {
        return this.size() == 0;
    }

    /**
     * Returns the size of the collection
     *
     * @return the number of currently stored objects in this collections
     */
    int size();

    /**
     * Adds the given <code>T</code> value into this collection
     *
     * @param value <code>T</code> which will be added in the collection
     */
    void add(T value);

    /**
     * Method which checks if given Object is contained in the collection
     *
     * @param value Object which will be checked if it is in the collection
     * @return Returns true only if the collection contains given value, as determined by equals method
     */
    boolean contains(Object value);

    /**
     * Method which removes an instance of the given Object from the collection
     *
     * @param value Object whose instance will be removed from the collection
     * @return Returns true only if the collection contains given value and removes one occurrence of it
     */
    boolean remove(Object value);

    /**
     * Allocates new array with size equals to the size of this collections, fills it with collection content and
     * returns the array
     *
     * @return New array filled with the collection
     */
    Object[] toArray();

    /**
     * Method calls processor.process(.) for each element of this collection
     *
     * @param processor Given processor
     */
    default void forEach(Processor<? super T> processor){
        ElementsGetter<T> getter = this.createElementsGetter();
        while(getter.hasNextElement()){
            processor.process(getter.getNextElement());
        }
    }

    /**
     * Method adds into the current collection all elements from the given collection.
     *
     * @param other Collection from which all elements will be added to this one. Remains unchanged
     */
    default void addAll(Collection<? extends T> other) {
        class LocalProcessor implements Processor<T> {
            @Override
            public void process(T value) {
                Collection.this.add(value);
            }
        }

        Processor<T> l = new LocalProcessor();
        other.forEach(l);
    }

    /**
     * Removes all elements from this collection
     */
    void clear();

    /**
     * Creates ElementsGetter for current instance of class
     *
     * @return new Elements Getter
     */
    ElementsGetter<T> createElementsGetter();

    /**
     * Adds all elements which satisfy the Tester to the Collection
     *
     * @param col    given collection
     * @param tester given tester
     */
    default void addAllSatisfying(Collection<? extends T> col, Tester<? super T> tester) {
        class LocalProcessor implements Processor<T>{
            @Override
            public void process(T value) {
                if(tester.test(value))
                    Collection.this.add(value);
            }
        }
        col.forEach(new LocalProcessor());
    }
}
