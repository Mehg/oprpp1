package hr.fer.oprpp1.custom.collections;

/**
 * Interface whose purpose is checking whether Objects satisfy some condition
 */
public interface Tester<T> {
    /**
     * Method which checks if object satisfies some condition
     * @param obj given object
     * @return true if given objsect satisfies some condition, otherwise false
     */
    boolean test(T obj);
}
