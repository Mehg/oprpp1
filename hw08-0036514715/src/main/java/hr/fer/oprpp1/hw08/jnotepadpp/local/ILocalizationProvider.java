package hr.fer.oprpp1.hw08.jnotepadpp.local;

/**
 * Class which is able to give translations for given keys
 */
public interface ILocalizationProvider {
    /**
     * Method for registering an {@link ILocalizationListener}
     *
     * @param listener given {@link ILocalizationListener}
     */
    void addLocalisationListener(ILocalizationListener listener);

    /**
     * Method for removing an {@link ILocalizationListener}
     *
     * @param listener given listener
     */
    void removeLocalisationListener(ILocalizationListener listener);

    /**
     * Method which returns the translation for given key
     *
     * @param key given key
     * @return translation
     */
    String getString(String key);

    /**
     * Returns current language
     * @return current language
     */
    String getCurrentLanguage();
}
