package hr.fer.oprpp1.hw08.jnotepadpp.model;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Default implementation of {@link MultipleDocumentModel} with one tab for each document
 */
public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {
    /**
     * Internal list of all {@link SingleDocumentModel}
     */
    private final ArrayList<SingleDocumentModel> documents;
    /**
     * {@link SingleDocumentModel} which represents the current document
     */
    private SingleDocumentModel currentDocument;
    /**
     * Internal list od listeners
     */
    private final ArrayList<MultipleDocumentListener> listeners;
    /**
     * ImageIcon for saved file
     */
    private final ImageIcon saved;
    /**
     * ImageIcon for modified file
     */
    private final ImageIcon modified;

    /**
     * Creates a new DefaultMultipleDocumentModel
     */
    public DefaultMultipleDocumentModel() {
        documents = new ArrayList<>();
        listeners = new ArrayList<>();
        saved = loadIcon("../icons/black.png");
        modified = loadIcon("../icons/orange.png");

        this.addChangeListener(e -> {
            SingleDocumentModel current = getCurrentDocument();
            int index = getSelectedIndex();

            if (index == -1)
                currentDocument = null;
            else
                currentDocument = documents.get(index);

            notifyDocumentChanged(current, currentDocument);
        });

    }

    /**
     * Helper method for loading an icon with given name
     *
     * @param name given name
     * @return ImageIcon of given name
     * @throws IllegalStateException if something went wrong when loading the icon
     */
    private ImageIcon loadIcon(String name) {
        InputStream is = this.getClass().getResourceAsStream(name);
        if (is == null) throw new IllegalStateException("Something went wrong when loading the saved icon");

        byte[] bytes;
        try {
            bytes = is.readAllBytes();
            is.close();

        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
        return new ImageIcon(bytes);
    }

    @Override
    public SingleDocumentModel createNewDocument() {
        SingleDocumentModel current = currentDocument;
        SingleDocumentModel newDocument = new DefaultSingleDocumentModel(null, "");

        notifyAdded(newDocument);

        notifyDocumentChanged(current, newDocument);

        return newDocument;
    }

    @Override
    public SingleDocumentModel getCurrentDocument() {
        return currentDocument;
    }

    @Override
    public SingleDocumentModel loadDocument(Path path) {
        if (path == null) throw new NullPointerException("Given path cannot be null");

        SingleDocumentModel current = currentDocument;


        boolean exists = false;
        for (SingleDocumentModel document : documents) {
            if (document.getFilePath()!=null && document.getFilePath().equals(path)) {
                currentDocument = document;
                exists = true;
            }
        }

        if (!exists) {
            try {
                String fileContent = Files.readString(path);
                SingleDocumentModel model = new DefaultSingleDocumentModel(path, fileContent);
                currentDocument = model;
                notifyAdded(model);
            } catch (IOException e) {
                throw new IllegalStateException(e.getMessage());
            }
        }

        notifyDocumentChanged(current, currentDocument);

        return currentDocument;
    }

    @Override
    public void saveDocument(SingleDocumentModel model, Path newPath) {
        if (newPath == null && model.getFilePath() == null)
            throw new NullPointerException("NewPath and model path are both null");
        else if (newPath == null)
            newPath = model.getFilePath();

        for (SingleDocumentModel document : documents) {
            if (!document.equals(model) && document.getFilePath() != null && document.getFilePath().equals(newPath)) {
                throw new IllegalArgumentException("There already exists a document with given path");
            }
        }

        try {
            Files.writeString(newPath, model.getTextComponent().getText());
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
        model.setFilePath(newPath);
        model.setModified(false);

    }

    @Override
    public void closeDocument(SingleDocumentModel model) {
        removeTabAt(documents.indexOf(model));
        notifyRemoved(model);

        if (documents.isEmpty()) {
            notifyDocumentChanged(model, null);
        } else if (model.equals(currentDocument)) {
            notifyDocumentChanged(model, documents.get(0));
        }

    }

    @Override
    public void addMultipleDocumentListener(MultipleDocumentListener l) {
        listeners.add(l);
    }

    @Override
    public void removeMultipleDocumentListener(MultipleDocumentListener l) {
        listeners.remove(l);
    }

    @Override
    public int getNumberOfDocuments() {
        return documents.size();
    }

    @Override
    public SingleDocumentModel getDocument(int index) {
        return documents.get(index);
    }

    @Override
    public Iterator<SingleDocumentModel> iterator() {
        return documents.iterator();
    }

    /**
     * Private method used for notifying listeners that the current document changed and selects its tab
     *
     * @param previousModel given previous model
     * @param currentModel  given current model
     */
    private void notifyDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
        currentDocument = currentModel;

        listeners.forEach(l -> l.currentDocumentChanged(previousModel, currentModel));

        if (currentModel != null)
            setSelectedIndex(documents.indexOf(currentModel));
    }

    /**
     * Private method used for notifying listeners that the given model has been added and selects its tab
     *
     * @param model given model
     */
    private void notifyAdded(SingleDocumentModel model) {
        documents.add(model);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JScrollPane(model.getTextComponent()), BorderLayout.CENTER);

        addTab("", panel);
        updateTitle(model);

        model.addSingleDocumentListener(new SingleDocumentListener() {
            @Override
            public void documentModifyStatusUpdated(SingleDocumentModel model) {
                updateTitle(model);

            }

            @Override
            public void documentFilePathUpdated(SingleDocumentModel model) {
                updateTitle(model);
            }
        });

    }

    private void updateTitle(SingleDocumentModel model) {
        Path path = model.getFilePath();

        String title = path == null ? "(unnamed)" : path.getFileName().toString();
        String toolTip = path == null ? "(unnamed)" : path.toAbsolutePath().toString();

        int index = documents.indexOf(model);

        setTitleAt(index, title);
        setToolTipTextAt(index, toolTip);
        setIconAt(index, model.isModified() ? modified : saved);
    }

    /**
     * Private helper method for notifying listeners that the given model has been removed
     *
     * @param model given model
     */
    private void notifyRemoved(SingleDocumentModel model) {
        documents.remove(model);
        listeners.forEach(l -> l.documentRemoved(model));
    }
}
