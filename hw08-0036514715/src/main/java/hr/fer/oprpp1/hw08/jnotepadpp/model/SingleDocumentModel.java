package hr.fer.oprpp1.hw08.jnotepadpp.model;

import javax.swing.*;
import java.nio.file.Path;

/**
 * Interface which represents a model of single document, having information about file path from which document was loaded,
 * document modification status and reference to Swing component which is used for editing
 * (each document has its own editor component).
 */
public interface SingleDocumentModel {
    /**
     * Returns the {@link JTextArea} component of the current SingleDocumentModel
     *
     * @return the {@link JTextArea} component of the current SingleDocumentModel
     */
    JTextArea getTextComponent();

    /**
     * Returns the file path associated with current SingleDocumentModel
     *
     * @return {@link Path} associated with this SingleDocumentModel - will be null for a new document
     */
    Path getFilePath();

    /**
     * Sets the file path of current SingleDocumentModel to given {@link Path}
     *
     * @param path given {@link Path}
     */
    void setFilePath(Path path);

    /**
     * Method which checks whether the current SingleDocumentModel has been modified
     *
     * @return true if current SingleDocumentModel was modified, otherwise false
     */
    boolean isModified();

    /**
     * Sets the modification of current SingleDocumentModel to given boolean
     *
     * @param modified given boolean
     */
    void setModified(boolean modified);

    /**
     * Adds a {@link SingleDocumentListener} as a listener for this SingleDocumentModel
     *
     * @param l given {@link SingleDocumentListener}
     */
    void addSingleDocumentListener(SingleDocumentListener l);

    /**
     * Removes given {@link SingleDocumentListener} as a listener
     *
     * @param l given {@link SingleDocumentListener}
     */
    void removeSingleDocumentListener(SingleDocumentListener l);
}
