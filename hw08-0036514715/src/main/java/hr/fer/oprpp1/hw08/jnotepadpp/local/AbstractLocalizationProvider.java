package hr.fer.oprpp1.hw08.jnotepadpp.local;

import java.util.ArrayList;

/**
 * Abstract class which is an implementation of {@link ILocalizationProvider}. Handles listeners
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {
    /**
     * Internal list of listeners
     */
    private final ArrayList<ILocalizationListener> listeners;

    /**
     * Creates a new AbstractLocalizationProvider
     */
    public AbstractLocalizationProvider() {
        listeners = new ArrayList<>();
    }

    @Override
    public void addLocalisationListener(ILocalizationListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeLocalisationListener(ILocalizationListener listener) {
        listeners.remove(listener);
    }

    /**
     * Method which notifies all registered listeners
     */
    public void fire() {
        for (ILocalizationListener listener : listeners) {
            listener.localizationChanged();
        }
    }

}
