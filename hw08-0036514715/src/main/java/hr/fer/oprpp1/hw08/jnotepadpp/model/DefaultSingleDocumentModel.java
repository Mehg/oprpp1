package hr.fer.oprpp1.hw08.jnotepadpp.model;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Class which represents a default implementation of a {@link SingleDocumentModel}
 */
public class DefaultSingleDocumentModel implements SingleDocumentModel {
    /**
     * Path of current document - can be null
     */
    private Path filePath;
    /**
     * JTextArea of current document
     */
    private final JTextArea jTextArea;
    /**
     * Whether the current document was modified
     */
    private boolean modified;
    /**
     * List of {@link SingleDocumentListener} listeners
     */
    private final ArrayList<SingleDocumentListener> listeners;

    /**
     * Creates a new DefaultSingleDocumentModel from given arguments
     *
     * @param filePath    given filePath - can be null
     * @param textContent given text content
     */
    public DefaultSingleDocumentModel(Path filePath, String textContent) {
        this.filePath = filePath;
        this.jTextArea = new JTextArea(textContent);
        this.modified = false;

        listeners = new ArrayList<>();

        jTextArea.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                setModified(true);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                setModified(true);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                setModified(true);
            }
        });
    }

    @Override
    public JTextArea getTextComponent() {
        return jTextArea;
    }

    @Override
    public Path getFilePath() {
        return filePath;
    }

    @Override
    public void setFilePath(Path path) {
        this.filePath = path;

        listeners.forEach(l -> l.documentFilePathUpdated(this));
    }

    @Override
    public boolean isModified() {
        return modified;
    }

    @Override
    public void setModified(boolean modified) {
        this.modified = modified;

        listeners.forEach(l -> l.documentModifyStatusUpdated(this));
    }

    @Override
    public void addSingleDocumentListener(SingleDocumentListener l) {
        listeners.add(l);
    }

    @Override
    public void removeSingleDocumentListener(SingleDocumentListener l) {
        listeners.remove(l);
    }
}
