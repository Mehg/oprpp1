package hr.fer.oprpp1.hw08.jnotepadpp.model;

/**
 * Interface which represents a {@link SingleDocumentModel} listener
 */
public interface SingleDocumentListener {
    /**
     * Method for notifying if the given {@link SingleDocumentModel} has been modified
     *
     * @param model given {@link SingleDocumentModel}
     */
    void documentModifyStatusUpdated(SingleDocumentModel model);

    /**
     * Method for notifying if the given {@link SingleDocumentModel} path has been updated
     *
     * @param model given {@link SingleDocumentModel}
     */
    void documentFilePathUpdated(SingleDocumentModel model);
}
