package hr.fer.oprpp1.hw08.jnotepadpp.local;

import javax.swing.*;


/**
 * Class which creates a JLabel with localizable name
 */
public class LocalizableJLabel extends JLabel {


    /**
     * Creates a new LocalizableJLabel from given arguments
     *
     * @param key      given key
     * @param provider given provider
     */
    public LocalizableJLabel(String key, ILocalizationProvider provider) {
        setText(provider.getString(key));
        provider.addLocalisationListener(() -> setText(provider.getString(key)));
    }
}
