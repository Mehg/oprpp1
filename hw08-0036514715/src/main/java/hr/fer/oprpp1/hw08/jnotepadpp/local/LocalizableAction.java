package hr.fer.oprpp1.hw08.jnotepadpp.local;

import javax.swing.*;

/**
 * Class which creates an action with localizable name and description
 */
public abstract class LocalizableAction extends AbstractAction {


    /**
     * Creates a new LocalizableAction from given arguments
     *
     * @param key      given key
     * @param provider given provider
     */
    public LocalizableAction(String key, ILocalizationProvider provider) {
        putValue(NAME, provider.getString(key));
        putValue(SHORT_DESCRIPTION, provider.getString(key + ".desc"));
        provider.addLocalisationListener(() -> {
            putValue(NAME, provider.getString(key));
            putValue(SHORT_DESCRIPTION, provider.getString(key + ".desc"));
        });
    }

}

