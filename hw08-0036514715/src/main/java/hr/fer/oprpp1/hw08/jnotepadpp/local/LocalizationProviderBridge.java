package hr.fer.oprpp1.hw08.jnotepadpp.local;

/**
 * Class which is a decorator for some other {@link ILocalizationProvider}
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {
    /**
     * whether it is connected
     */
    private boolean connected;
    /**
     * current provider
     */
    private final ILocalizationProvider parent;
    /**
     * listener for connecting
     */
    private ILocalizationListener listener = this::fire;
    /**
     * current language
     */
    private String language;

    /**
     * Creates a new LocalizationProviderBridge fro given parameters
     *
     * @param parent given provider
     */
    public LocalizationProviderBridge(ILocalizationProvider parent) {
        this.parent = parent;
    }

    /**
     * Method used to connect to given provider if already not connected
     */
    public void connect() {
        if (connected)
            return;

        if (language != null && !language.equals(getCurrentLanguage()))
            fire();

        connected = true;
        parent.addLocalisationListener(listener);
    }

    /**
     * Method used to disconnect from given provider if already connected
     */
    public void disconnect() {
        if (!connected)
            return;

        connected = false;
        parent.removeLocalisationListener(this::fire);
        language = parent.getCurrentLanguage();

    }

    @Override
    public String getString(String key) {
        return parent.getString(key);
    }

    @Override
    public String getCurrentLanguage() {
        return parent.getCurrentLanguage();
    }
}
