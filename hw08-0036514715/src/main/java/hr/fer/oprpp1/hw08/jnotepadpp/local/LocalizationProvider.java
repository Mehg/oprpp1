package hr.fer.oprpp1.hw08.jnotepadpp.local;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class that is singleton of {@link ILocalizationProvider}
 */
public class LocalizationProvider extends AbstractLocalizationProvider {
    /**
     * Instance of LocalizationProvider
     */
    private static final LocalizationProvider instance = new LocalizationProvider();

    /**
     * Current language of the LocalizationProvider
     */
    private String language;
    /**
     * ResourceBundle of the LocalizationProvider
     */
    private ResourceBundle bundle;

    /**
     * Private method which constructs a new LocalizationProvider with language set to english
     */
    private LocalizationProvider() {
        language = "en";
        Locale locale = Locale.forLanguageTag(language);
        bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.jnotepadpp.jnotepadpp", locale);
    }

    /**
     * Method which returns an instance of a LocalizationProvider with default language set to english
     *
     * @return instance of LocalizationProvider
     */
    public static LocalizationProvider getInstance() {
        return instance;
    }

    /**
     * Sets the language to the given language
     *
     * @param language given language
     */
    public void setLanguage(String language) {
        this.language = language;

        Locale locale = Locale.forLanguageTag(language);
        bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.jnotepadpp.jnotepadpp", locale);
        fire();
    }

    @Override
    public String getString(String key) {
        return bundle.getString(key);
    }

    @Override
    public String getCurrentLanguage() {
        return language;
    }
}
