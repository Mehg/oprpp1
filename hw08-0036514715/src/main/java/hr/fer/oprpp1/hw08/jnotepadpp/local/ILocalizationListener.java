package hr.fer.oprpp1.hw08.jnotepadpp.local;

/**
 * Interface for listeners of {@link ILocalizationProvider}
 */
public interface ILocalizationListener {
    /**
     * Method called when language changed
     */
    void localizationChanged();
}
