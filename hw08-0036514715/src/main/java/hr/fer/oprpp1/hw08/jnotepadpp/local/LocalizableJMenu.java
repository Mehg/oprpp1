package hr.fer.oprpp1.hw08.jnotepadpp.local;

import javax.swing.*;

/**
 * Class which creates a JMenu with localizable name
 */
public class LocalizableJMenu extends JMenu {


    /**
     * Creates a new LocalizableJMenu from given arguments
     *
     * @param key      given key
     * @param provider given provider
     */
    public LocalizableJMenu(String key, ILocalizationProvider provider) {
        setText(provider.getString(key));
        provider.addLocalisationListener(() -> setText(provider.getString(key)));
    }
}
