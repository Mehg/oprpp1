package hr.fer.oprpp1.hw08.jnotepadpp.model;

import java.nio.file.Path;


/**
 * Interface which represents a model capable of holding zero, one or more documents,
 * where each document and having a concept of current document
 * – the one which is shown to the user and on which user works
 */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel> {
    /**
     * Method for creating a new document
     *
     * @return new {@link SingleDocumentModel}
     */
    SingleDocumentModel createNewDocument();

    /**
     * Method for getting the current document
     *
     * @return the current {@link SingleDocumentModel}
     */
    SingleDocumentModel getCurrentDocument();

    /**
     * Method for loading a document from given path
     *
     * @param path given path
     * @return {@link SingleDocumentModel} for given path
     * @throws NullPointerException if given path is null
     */
    SingleDocumentModel loadDocument(Path path);

    /**
     * Method for saving given {@link SingleDocumentModel} to a specified path
     *
     * @param model   given {@link SingleDocumentModel}
     * @param newPath given path
     * @throws IllegalArgumentException if given path is already a path of some {@link SingleDocumentModel}
     */
    void saveDocument(SingleDocumentModel model, Path newPath);

    /**
     * Method which closes given {@link SingleDocumentModel}
     *
     * @param model given {@link SingleDocumentModel}
     */
    void closeDocument(SingleDocumentModel model);

    /**
     * Method for adding a {@link MultipleDocumentListener}
     *
     * @param l given {@link MultipleDocumentListener}
     */
    void addMultipleDocumentListener(MultipleDocumentListener l);

    /**
     * Method for removing a {@link MultipleDocumentListener}
     *
     * @param l given {@link MultipleDocumentListener}
     */
    void removeMultipleDocumentListener(MultipleDocumentListener l);

    /**
     * Returns the current number of documents
     *
     * @return the current number of documents
     */
    int getNumberOfDocuments();

    /**
     * Returns a {@link SingleDocumentModel} for given index
     *
     * @param index given index
     * @return {@link SingleDocumentModel} at given index
     */
    SingleDocumentModel getDocument(int index);
}