package hr.fer.oprpp1.hw08.jnotepadpp.model;

/**
 * Interface which represents a {@link MultipleDocumentModel} listener
 */
public interface MultipleDocumentListener {
    /**
     * Method for notifying if the current document has changed from previous to current {@link SingleDocumentModel}
     *
     * @param previousModel previous {@link SingleDocumentModel}
     * @param currentModel  current {@link SingleDocumentModel}
     * @throws NullPointerException if both the previousModel and the currentModel are null
     */
    void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel);

    /**
     * Method for notifying if there has been a {@link SingleDocumentModel} added
     *
     * @param model given {@link SingleDocumentModel}
     */
    void documentAdded(SingleDocumentModel model);

    /**
     * Method for notifying if there has been a {@link SingleDocumentModel} removed
     *
     * @param model given {@link SingleDocumentModel}
     */
    void documentRemoved(SingleDocumentModel model);
}
