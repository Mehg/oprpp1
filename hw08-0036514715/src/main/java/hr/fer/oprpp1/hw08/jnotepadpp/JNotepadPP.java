package hr.fer.oprpp1.hw08.jnotepadpp;

import hr.fer.oprpp1.hw08.jnotepadpp.model.*;
import hr.fer.oprpp1.hw08.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.oprpp1.hw08.jnotepadpp.local.LocalizableAction;
import hr.fer.oprpp1.hw08.jnotepadpp.local.LocalizableJMenu;
import hr.fer.oprpp1.hw08.jnotepadpp.local.LocalizationProvider;

import javax.swing.*;
import javax.swing.event.CaretListener;
import javax.swing.text.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class which is an implementation of a tabbed note editor
 */
public class JNotepadPP extends JFrame {
    /**
     * MultipleDocumentModel used in JNotepadPP
     */
    private MultipleDocumentModel model;
    /**
     * Localization provider
     */
    private final FormLocalizationProvider provider;
    /**
     * Action which defines creating a new document
     */
    private Action newDocument;
    /**
     * Action which defines opening a document
     */
    private Action openDocument;
    /**
     * Action which defines saving a document
     */
    private Action saveDocument;
    /**
     * Action which defines saving as a new document
     */
    private Action saveAs;
    /**
     * Action which defines closing a tab
     */
    private Action closeDocument;
    /**
     * Action which defines cutting text
     */
    private Action cut;
    /**
     * Action which defines copying text
     */
    private Action copy;
    /**
     * Action which defines pasting text
     */
    private Action paste;
    /**
     * Action which defines evaluating stats
     */
    private Action stats;
    /**
     * Action which defines exiting
     */
    private Action exit;
    /**
     * Action which defines making selected text uppercase
     */
    private Action toUpperCase;
    /**
     * Action which defines making selected text lowercase
     */
    private Action toLowerCase;
    /**
     * Action which defines inverting case in selected text
     */
    private Action invertCase;
    /**
     * Action which defines sorting selected text ascend
     */
    private Action sortAsc;
    /**
     * Action which defines sorting selected text descend
     */
    private Action sortDesc;
    /**
     * Action which defines distinguishing between lines
     */
    private Action unique;
    /**
     * Action which defines Croatian language
     */
    private Action hr;
    /**
     * Action which defines English language
     */
    private Action en;
    /**
     * Action which defines German language
     */
    private Action de;
    /**
     * Action which defines whether application is running
     */
    private final AtomicBoolean running = new AtomicBoolean(true);


    /**
     * Creates a new JNotepadPP
     */
    public JNotepadPP() {
        setLocation(0, 0);
        setSize(600, 600);

        setTitle("JNotepad++");
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        provider = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);

        WindowListener wl = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                exit();
            }
        };

        addWindowListener(wl);

        initGUI();

        model.addMultipleDocumentListener(new MultipleDocumentListener() {
            private final SingleDocumentListener listener = new SingleDocumentListener() {
                @Override
                public void documentModifyStatusUpdated(SingleDocumentModel model) {

                }

                @Override
                public void documentFilePathUpdated(SingleDocumentModel model) {
                    if (model != null)
                        setTitle(model.getFilePath() == null ? "(unnamed) - JNotepad++" : model.getFilePath().toString() + " - JNotepad++");
                }
            };

            @Override
            public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
                if (previousModel == null && currentModel == null)
                    throw new IllegalArgumentException("Both parameters are null");
                if (previousModel != null)
                    previousModel.removeSingleDocumentListener(listener);
                if (currentModel != null)
                    currentModel.addSingleDocumentListener(listener);

                if (currentModel != null)
                    setTitle(currentModel.getFilePath() == null ? "(unnamed) - JNotepad++" : currentModel.getFilePath().toString() + " - JNotepad++");

            }

            @Override
            public void documentAdded(SingleDocumentModel model) {

            }

            @Override
            public void documentRemoved(SingleDocumentModel model) {

            }
        });

    }

    /**
     * Initializes GUI
     */
    public void initGUI() {
        JPanel panel = new JPanel(new BorderLayout());

        LocalizationProvider.getInstance().setLanguage("en");
        model = new DefaultMultipleDocumentModel();

        panel.add((Component) model, BorderLayout.CENTER);

        add(panel, BorderLayout.CENTER);

        createActions();
        createMenus();
        createToolbars();
        createStatusBar(panel);
    }

    /**
     * Initializes actions
     */
    public void createActions() {
        openDocument = new LocalizableAction("open", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                openDocument();
            }
        };
        openDocument.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control O"));
        openDocument.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.VK_O);

        newDocument = new LocalizableAction("new", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.createNewDocument();
            }
        };
        newDocument.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control N"));
        newDocument.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.VK_N);


        saveDocument = new LocalizableAction("save", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveDocument();
            }
        };
        saveDocument.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control S"));
        saveDocument.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.VK_S);

        saveAs = new LocalizableAction("saveAs", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveAs();
            }
        };
        saveAs.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift S"));
        saveAs.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.SHIFT_DOWN_MASK + KeyEvent.VK_S);

        closeDocument = new LocalizableAction("close", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                closeDocument();
            }
        };
        closeDocument.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control W"));
        closeDocument.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.VK_W);

        copy = new LocalizableAction("copy", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                copyInDocument();
            }
        };
        copy.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control C"));
        copy.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.VK_C);

        paste = new LocalizableAction("paste", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                pasteInDocument();
            }
        };
        paste.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control V"));
        paste.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.VK_V);

        cut = new LocalizableAction("cut", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                cutInDocument();
            }
        };
        cut.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control X"));
        cut.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.VK_X);

        stats = new LocalizableAction("stats", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculateStats();
            }
        };
        stats.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift F1"));
        stats.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.SHIFT_DOWN_MASK + KeyEvent.VK_F1);

        exit = new LocalizableAction("exit", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                exit();
            }
        };
        exit.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift F2"));
        exit.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.SHIFT_DOWN_MASK + KeyEvent.VK_F2);

        toUpperCase = new LocalizableAction("toUpperCase", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeCase(true, false);
            }
        };
        toUpperCase.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift F3"));
        toUpperCase.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.SHIFT_DOWN_MASK + KeyEvent.VK_F3);
        toUpperCase.setEnabled(false);

        toLowerCase = new LocalizableAction("toLowerCase", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeCase(false, false);
            }
        };
        toLowerCase.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift F4"));
        toLowerCase.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.SHIFT_DOWN_MASK + KeyEvent.VK_F4);
        toLowerCase.setEnabled(false);

        invertCase = new LocalizableAction("invertCase", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeCase(false, true);
            }
        };
        invertCase.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift F5"));
        invertCase.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.SHIFT_DOWN_MASK + KeyEvent.VK_F5);
        invertCase.setEnabled(false);

        sortAsc = new LocalizableAction("sortAsc", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                sort(true);
            }
        };
        sortAsc.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift F6"));
        sortAsc.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.SHIFT_DOWN_MASK + KeyEvent.VK_F6);
        sortAsc.setEnabled(false);

        sortDesc = new LocalizableAction("sortDesc", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                sort(false);
            }
        };
        sortDesc.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift F7"));
        sortDesc.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.SHIFT_DOWN_MASK + KeyEvent.VK_F7);
        sortDesc.setEnabled(false);

        unique = new LocalizableAction("unique", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                unique();
            }
        };
        unique.putValue(
                Action.ACCELERATOR_KEY,
                KeyStroke.getKeyStroke("control shift F8"));
        unique.putValue(
                Action.MNEMONIC_KEY,
                KeyEvent.SHIFT_DOWN_MASK + KeyEvent.VK_F8);
        unique.setEnabled(false);

        hr = new LocalizableAction("hr", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                LocalizationProvider.getInstance().setLanguage("hr");
            }
        };

        en = new LocalizableAction("en", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                LocalizationProvider.getInstance().setLanguage("en");
            }
        };

        de = new LocalizableAction("de", provider) {
            @Override
            public void actionPerformed(ActionEvent e) {
                LocalizationProvider.getInstance().setLanguage("de");
            }
        };
    }

    /**
     * Initializes menus
     */
    private void createMenus() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new LocalizableJMenu("file", provider);
        menuBar.add(fileMenu);

        fileMenu.add(new JMenuItem(openDocument));
        fileMenu.add(new JMenuItem(newDocument));
        fileMenu.add(new JMenuItem(saveDocument));
        fileMenu.add(new JMenuItem(saveAs));
        fileMenu.add(new JMenuItem(closeDocument));
        fileMenu.add(new JMenuItem(stats));
        fileMenu.add(new JMenuItem(exit));


        JMenu editMenu = new LocalizableJMenu("edit", provider);
        menuBar.add(editMenu);

        editMenu.add(new JMenuItem(copy));
        editMenu.add(new JMenuItem(paste));
        editMenu.add(new JMenuItem(cut));

        JMenu languageMenu = new LocalizableJMenu("language", provider);
        menuBar.add(languageMenu);

        languageMenu.add(new JMenuItem(hr));
        languageMenu.add(new JMenuItem(en));
        languageMenu.add(new JMenuItem(de));

        JMenu toolsMenu = new LocalizableJMenu("tools", provider);
        JMenu changeCase = new LocalizableJMenu("changeCase", provider);
        changeCase.add(new JMenuItem(toUpperCase));
        changeCase.add(new JMenuItem(toLowerCase));
        changeCase.add(new JMenuItem(invertCase));

        enableIfSelected(toUpperCase);
        enableIfSelected(toLowerCase);
        enableIfSelected(invertCase);

        JMenu sortMenu = new LocalizableJMenu("sort", provider);
        sortMenu.add(new JMenuItem(sortAsc));
        sortMenu.add(new JMenuItem(sortDesc));

        enableIfSelected(sortAsc);
        enableIfSelected(sortDesc);

        toolsMenu.add(changeCase);
        toolsMenu.add(sortMenu);
        toolsMenu.add(new JMenuItem(unique));
        enableIfSelected(unique);
        menuBar.add(toolsMenu);

        this.setJMenuBar(menuBar);
    }

    /**
     * Initializes toolbars
     */
    private void createToolbars() {
        JToolBar toolBar = new JToolBar();
        toolBar.setRollover(true);
        toolBar.setFloatable(true);

        toolBar.add(new JButton(newDocument));
        toolBar.add(new JButton(openDocument));
        toolBar.add(new JButton(saveDocument));
        toolBar.add(new JButton(saveAs));
        toolBar.add(new JButton(closeDocument));
        toolBar.addSeparator();
        toolBar.add(new JButton(copy));
        toolBar.add(new JButton(paste));
        toolBar.add(new JButton(cut));
        toolBar.addSeparator();
        toolBar.add(new JButton(stats));
        toolBar.addSeparator();
        toolBar.add(new JButton(exit));
//        toolBar.add(new JButton(toUpperCase));
//        toolBar.add(new JButton(toLowerCase));
//        toolBar.add(new JButton(invertCase));
//        toolBar.addSeparator();
//
//        toolBar.add(new JButton(sortAsc));
//        toolBar.add(new JButton(sortDesc));
//        toolBar.add(new JButton(unique));
//        toolBar.addSeparator();

        this.getContentPane().add(toolBar, BorderLayout.PAGE_START);
    }

    /**
     * Initializes the status bar which displays the position and length of document as well as time
     */
    private void createStatusBar(JPanel outer) {
        JPanel panel = new JPanel(new GridLayout(1, 3));
        panel.setBorder(BorderFactory.createMatteBorder(5, 0, 0, 0, Color.DARK_GRAY));

        JLabel length = new JLabel("");
        length.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
        JLabel position = new JLabel("");
        position.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.LIGHT_GRAY));
        JLabel clock = new JLabel("");
        clock.setHorizontalAlignment(SwingConstants.RIGHT);

        panel.add(length);
        panel.add(position);
        panel.add(clock);

        addLengthAndPositionListeners(length, position);
        addClockListener(clock);

//        this.getContentPane().add(panel, BorderLayout.PAGE_END);
        outer.add(panel, BorderLayout.PAGE_END);

    }

    /**
     * private helper method used for assigning length and position listeners
     *
     * @param length   given JLabel for length
     * @param position given JLabel for position
     */
    private void addLengthAndPositionListeners(JLabel length, JLabel position) {
        CaretListener caretListener = e -> updateLabels(length, position);
        provider.addLocalisationListener(() -> updateLabels(length, position));

        model.addMultipleDocumentListener(new MultipleDocumentListener() {
            @Override
            public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
                if (previousModel != null)
                    previousModel.getTextComponent().removeCaretListener(caretListener);

                if (currentModel != null) {
                    currentModel.getTextComponent().addCaretListener(caretListener);
                }

                updateLabels(length, position);
            }

            @Override
            public void documentAdded(SingleDocumentModel modelNew) {
                currentDocumentChanged(model.getCurrentDocument(), modelNew);
            }

            @Override
            public void documentRemoved(SingleDocumentModel modelOld) {
                currentDocumentChanged(modelOld, model.getCurrentDocument());
            }
        });
    }

    /**
     * Private helper method used for updating labels
     *
     * @param length   given JLabel length
     * @param position given JLabel position
     */
    private void updateLabels(JLabel length, JLabel position) {
        if (model.getCurrentDocument() == null) {
            position.setText("");
            length.setText("");
            return;
        }

        JTextComponent textComponent = model.getCurrentDocument().getTextComponent();
        Caret caret = textComponent.getCaret();
        int pos = textComponent.getCaretPosition();

        Element root = textComponent.getDocument().getDefaultRootElement();
        int row = root.getElementIndex(pos);
        int col = pos - root.getElement(row).getStartOffset();

        position.setText(provider.getString("ln") + (row + 1) +
                "  " + provider.getString("col") + (col +1) +
                "  " + provider.getString("sel") + (caret.getMark() - caret.getDot()));
        length.setText(provider.getString("length") + " " + textComponent.getText().length());
    }

    /**
     * Private helper method for adding clock
     *
     * @param clock given JLabel for clock
     */
    private void addClockListener(JLabel clock) {
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        Thread thread = new Thread(() -> {
            while (running.get()) {
                Calendar currentCalendar = Calendar.getInstance();
                Date currentTime = currentCalendar.getTime();
                clock.setText(dateTimeFormat.format(currentTime));

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Method which defines opening a document
     */
    private void openDocument() {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle(provider.getString("open.file"));
        if (fc.showOpenDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
            return;
        }
        File fileName = fc.getSelectedFile();
        Path filePath = fileName.toPath();

        if (!Files.isReadable(filePath)) {
            JOptionPane.showMessageDialog(
                    JNotepadPP.this,
                    provider.getString("file") + " " + fileName.getAbsolutePath() + " " + provider.getString("noExist"),
                    provider.getString("error"),
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
        model.loadDocument(filePath);
    }

    /**
     * Method which defines closing a document
     */
    private void closeDocument() {
        SingleDocumentModel currentDocument = model.getCurrentDocument();
        if (currentDocument == null)
            return;
        if (currentDocument.isModified()) {
            if (currentDocument.getFilePath() != null)
                saveDocument();
            else
                saveAs();
        }
        model.closeDocument(model.getCurrentDocument());
    }

    /**
     * Method which defines saving a document
     */
    private void saveDocument() {
        if (model.getCurrentDocument() == null)
            return;
        model.saveDocument(model.getCurrentDocument(), null);
    }

    /**
     * Method which defines saving as a new document
     */
    private void saveAs() {
        if (model.getCurrentDocument() == null)
            return;
        Path newPath;
        JFileChooser jfc = new JFileChooser();
        jfc.setDialogTitle(provider.getString("saveAs.doc"));

        int result = jfc.showSaveDialog(JNotepadPP.this);

        if (jfc.getSelectedFile().exists() && jfc.getSelectedFile().isFile() && result == JFileChooser.APPROVE_OPTION) {
            String[] options = new String[]{provider.getString("yes"), provider.getString("no")};
            int result2 = JOptionPane.showOptionDialog(
                    JNotepadPP.this,
                    provider.getString("already.exists"),
                    provider.getString("warning"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    null,
                    options,
                    options[0]);

            if (result2 == 0) {
                model.saveDocument(model.getCurrentDocument(), jfc.getSelectedFile().toPath());
            }
        } else if (result == JFileChooser.APPROVE_OPTION) {
            if (result != JFileChooser.APPROVE_OPTION) {
                JOptionPane.showMessageDialog(
                        JNotepadPP.this,
                        provider.getString("nothing"),
                        provider.getString("warning"),
                        JOptionPane.WARNING_MESSAGE);
                return;
            }
            model.saveDocument(model.getCurrentDocument(), jfc.getSelectedFile().toPath());

        }
    }

    /**
     * Method which defines copying in a document
     */
    private void copyInDocument() {
        if (model.getCurrentDocument() == null)
            return;
        JTextArea currentDocument = model.getCurrentDocument().getTextComponent();
        String text = currentDocument.getSelectedText();
        StringSelection selection = new StringSelection(text);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
    }

    /**
     * Method which defines pasting in a document
     */
    private void pasteInDocument() {
        if (model.getCurrentDocument() == null)
            return;
        JTextArea currentDocument = model.getCurrentDocument().getTextComponent();
        Document document = currentDocument.getDocument();
        Caret caret = currentDocument.getCaret();
        Element root = document.getDefaultRootElement();

        int firstLine = root.getElementIndex(Math.min(caret.getMark(), caret.getDot()));
        int startOfFirstLine = root.getElement(firstLine).getStartOffset();

        try {
            Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
            Transferable t = c.getContents(this);
            if (t == null) {
                return;
            }
            String copied = (String) t.getTransferData(DataFlavor.stringFlavor);
            document.insertString(startOfFirstLine, copied, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method which defines cutting in a document
     */
    private void cutInDocument() {
        if (model.getCurrentDocument() == null)
            return;
        JTextArea currentDocument = model.getCurrentDocument().getTextComponent();
        copyInDocument();
        currentDocument.replaceSelection("");
    }

    /**
     * Method which calculates stats of a document
     */
    private void calculateStats() {
        if (model.getCurrentDocument() == null)
            return;
        String currentDocument = model.getCurrentDocument().getTextComponent().getText();
        int numberOfCharacters = currentDocument.length();
        int numberOfNonBlank = currentDocument.replaceAll("\\s*", "").length();
        int numberOfLines = (int) currentDocument.lines().count();

        String sb = String.format(provider.getString("stats.all"), numberOfCharacters, numberOfNonBlank, numberOfLines);

        JOptionPane.showMessageDialog(
                this,
                sb,
                provider.getString("stats.title"),
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Method which exits the program
     */
    private void exit() {
        boolean shouldSave = false;

        for (int i = 0; i < model.getNumberOfDocuments(); i++) {
            if (model.getDocument(i).isModified())
                shouldSave = true;
        }
        if (!shouldSave) {
            dispose();
            return;
        }
        String[] options = new String[]{provider.getString("yes"), provider.getString("no"), provider.getString("cancel")};
        int result = JOptionPane.showOptionDialog(JNotepadPP.this,
                provider.getString("unsaved"),
                provider.getString("warning"), JOptionPane.DEFAULT_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null, options, options[0]);

        switch (result) {
            case 2, JOptionPane.CLOSED_OPTION -> {
            }
            case 0 -> {
                int number = model.getNumberOfDocuments();
                for (int i = 0; i < number; i++)
                    closeDocument();

                dispose();
            }
            case 1 -> dispose();
        }
        running.set(false);
    }

    /**
     * Private helper method for changing case in selected text
     *
     * @param toUpper whether to make all text uppercase
     * @param invert  whether to invert all text cases
     */
    private void changeCase(boolean toUpper, boolean invert) {
        if (model.getCurrentDocument() == null)
            return;

        JTextArea currentDocument = model.getCurrentDocument().getTextComponent();

        String selected = currentDocument.getSelectedText();

        if (invert) {
            selected = selected.chars().map(c -> Character.isLetter(c) ? c ^ ' ' : c)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();
        } else {
            selected = toUpper ? selected.toUpperCase() : selected.toLowerCase();
        }

        currentDocument.replaceSelection(selected);
    }

    /**
     * Private helper method which makes action enabled if something is selected
     *
     * @param action given action
     */
    private void enableIfSelected(Action action) {
        CaretListener caretListener = e -> {
            if (model.getCurrentDocument() != null)
                action.setEnabled(model.getCurrentDocument().getTextComponent().getSelectedText() != null);
            else action.setEnabled(false);
        };
        model.addMultipleDocumentListener(new MultipleDocumentListener() {
            @Override
            public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
                if (previousModel != null)
                    previousModel.getTextComponent().removeCaretListener(caretListener);
                if (currentModel != null) {
                    currentModel.getTextComponent().addCaretListener(caretListener);
                    action.setEnabled(model.getCurrentDocument().getTextComponent().getSelectedText() != null);
                }
            }

            @Override
            public void documentAdded(SingleDocumentModel model) {

            }

            @Override
            public void documentRemoved(SingleDocumentModel model) {

            }
        });
    }

    /**
     * Method which sorts selected text
     *
     * @param asc whether to sort ascending or descending
     */
    private void sort(boolean asc) {
        if (model.getCurrentDocument() == null)
            return;
        selectedLines(asc, false);
    }

    /**
     * Method which removes all duplicate lines from selected lines
     */
    private void unique() {
        if (model.getCurrentDocument() == null)
            return;
        selectedLines(false, true);
    }

    /**
     * Private helper method which either sorts selected lines or removes duplicates
     *
     * @param asc    whether to sort ascending or descending
     * @param unique whether to remove duplicates
     */
    private void selectedLines(boolean asc, boolean unique) {
        JTextComponent textComponent = model.getCurrentDocument().getTextComponent();
        Caret caret = textComponent.getCaret();

        Document document = textComponent.getDocument();
        Element root = document.getDefaultRootElement();
        int firstLine = root.getElementIndex(Math.min(caret.getMark(), caret.getDot()));
        int lastLine = root.getElementIndex(Math.max(caret.getMark(), caret.getDot()));

        int startOfFirstLine = root.getElement(firstLine).getStartOffset();
        int endOfLastLine = Math.min(root.getElement(lastLine).getEndOffset(), document.getLength());

        try {
            String text = document.getText(startOfFirstLine, endOfLastLine - startOfFirstLine);
            String[] lines = text.split("\n");
            String transformed;

            if (unique) {
                transformed = String.join("\n",
                        Arrays.stream(lines).distinct()
                                .toArray(String[]::new));
            } else {
                Locale locale = new Locale(provider.getCurrentLanguage());
                Collator collator = Collator.getInstance(locale);
                transformed = String.join("\n",
                        Arrays.stream(lines).sorted(asc ? collator : collator.reversed())
                                .toArray(String[]::new));
            }

            document.remove(startOfFirstLine, endOfLastLine - startOfFirstLine);
            document.insertString(startOfFirstLine, transformed + "\n", null);
        } catch (BadLocationException ignored) {
        }
    }

    /**
     * Main method
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new JNotepadPP().setVisible(true));
    }


}