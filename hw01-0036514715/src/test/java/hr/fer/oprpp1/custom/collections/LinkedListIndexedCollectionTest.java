package hr.fer.oprpp1.custom.collections;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LinkedListIndexedCollectionTest {
    @Test
    public void testDefaultConstructor() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        assertEquals(0, list.size());
        list.add("one");
        list.add("two");
        list.add("three");
        Object[] other = {"one", "two", "three"};
        assertArrayEquals(other, list.toArray());
    }

    @Test
    public void testConstructorWithCollection() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        list.add("one");
        list.add("two");
        list.add("three");
        Object[] other = {"one", "two", "three"};
        LinkedListIndexedCollection test = new LinkedListIndexedCollection(list);
        assertArrayEquals(other, test.toArray());
    }

    @Test
    public void testConstructorWithNullCollection() {
        assertThrows(NullPointerException.class, () -> new LinkedListIndexedCollection(null));
    }

    @Test
    public void testSize() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        assertEquals(0, list.size());
        list.add("one");
        assertEquals(1, list.size());
        list.remove(0);
        assertEquals(0, list.size());
    }

    @Test
    public void testAddNull() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        assertThrows(NullPointerException.class, () -> list.add(null));
    }


    @Test
    public void testContains() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        list.add("one");
        list.add("two");
        list.add("three");
        assertTrue(list.contains("three"));
        assertFalse(list.contains("four"));
    }


    @Test
    public void testRemoveValue() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        list.add("one");
        list.add("two");
        list.add("three");
        assertTrue(list.remove("one"));
        assertFalse(list.remove("four"));
    }

    @Test
    public void testClear() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        list.add("one");
        list.add("two");
        list.add("three");
        list.clear();
        Object[] other = new Object[0];
        assertArrayEquals(other, list.toArray());
    }

    @Test
    public void testGetInvalid() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> list.get(50));
    }

    @Test
    public void testGet() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        list.add("one");
        list.add("two");
        list.add("three");
        assertEquals("two", list.get(1));
    }


    @Test
    public void testInsertInvalid() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        assertThrows(NullPointerException.class, ()->list.insert(null, 0));
        assertThrows(IndexOutOfBoundsException.class, ()->list.insert("value", -1));
        assertThrows(IndexOutOfBoundsException.class, ()->list.insert("value", 1));
    }

    @Test
    public void testInsert() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        list.insert("one", 0); //empty - first insert
        list.insert("two", 1); //insert at back
        list.insert("three", 0); //insert at front
        list.insert("four", 2); //insert at back
        list.insert("five", 2); //insert in the middle
        Object[] other = {"three", "one", "five","four", "two"};
        assertArrayEquals(other, list.toArray());
    }

    @Test
    public void testIndexOf() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        list.add("one");
        list.add("two");
        list.add("three");
        assertEquals(1, list.indexOf("two"));
        assertEquals(-1, list.indexOf("eight"));
    }


    @Test
    public void testRemove() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        list.add("one");
        list.add("two");
        list.add("three");
        list.remove(1);
        list.remove(1);
        Object[] other = {"one"};
        Object[] other2 = {};
        assertArrayEquals(other, list.toArray());
        list.remove(0);
        assertArrayEquals(other2,list.toArray());
    }


    @Test
    public void testRemoveInvalid() {
        LinkedListIndexedCollection list = new LinkedListIndexedCollection();
        assertThrows(IndexOutOfBoundsException.class, ()->list.remove(-1));
        assertThrows(IndexOutOfBoundsException.class, ()->list.remove(11));
    }


}
