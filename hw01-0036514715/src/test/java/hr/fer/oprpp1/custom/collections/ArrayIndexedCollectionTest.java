package hr.fer.oprpp1.custom.collections;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayIndexedCollectionTest {
    @Test
    public void testDefaultConstructor() {
        ArrayIndexedCollection array = new ArrayIndexedCollection();
        assertEquals(16, array.getCapacity());
    }

    @Test
    public void testConstructorWithInitialCapacity() {
        ArrayIndexedCollection array = new ArrayIndexedCollection(25);
        assertEquals(25, array.getCapacity());
    }

    @Test
    public void testConstructorWithOtherCollectionSmallerSize() {
        ArrayIndexedCollection other = new ArrayIndexedCollection(10);
        for (int i = 0; i < 10; i++)
            other.add(i);
        ArrayIndexedCollection test = new ArrayIndexedCollection(other);
        assertArrayEquals(other.toArray(), test.toArray());
    }

    @Test
    public void testConstructorWithOtherCollectionBiggerSize() {
        ArrayIndexedCollection other = new ArrayIndexedCollection(20);
        for (int i = 0; i < 20; i++)
            other.add(i);
        ArrayIndexedCollection test = new ArrayIndexedCollection(other);
        assertArrayEquals(other.toArray(), test.toArray());
    }

    @Test
    public void testConstructorWithOtherCollectionCorrectSetSize() {
        ArrayIndexedCollection other = new ArrayIndexedCollection(20);
        for (int i = 0; i < 20; i++)
            other.add(i);
        ArrayIndexedCollection test = new ArrayIndexedCollection(other, 21);
        assertArrayEquals(other.toArray(), test.toArray());
    }

    @Test
    public void testConstructorWithOtherCollectionWrongSetSize() {
        ArrayIndexedCollection other = new ArrayIndexedCollection(20);
        for (int i = 0; i < 20; i++)
            other.add(i);
        ArrayIndexedCollection test = new ArrayIndexedCollection(other, 11);
        assertArrayEquals(other.toArray(), test.toArray());
    }

    @Test
    public void testConstructorWithZeroAsInitialSize() {
        assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(0));
    }

    @Test
    public void testConstructorWithOtherCollectionIsNull() {
        assertThrows(NullPointerException.class, () -> new ArrayIndexedCollection(null));
    }

    @Test
    public void testAddingNull() {
        ArrayIndexedCollection aic = new ArrayIndexedCollection();
        assertThrows(NullPointerException.class, () -> aic.add(null));
    }

    @Test
    public void testAddingDuplicates() {
        ArrayIndexedCollection aic = new ArrayIndexedCollection();
        aic.add(2);
        aic.add(2);
        aic.add(2);
        Object[] other = {2,2,2};
        assertArrayEquals(other, aic.toArray());
    }

    @Test
    public void testAddingNotNull() {
        ArrayIndexedCollection test = new ArrayIndexedCollection();
        Object[] other = {1, 2, 3};
        test.add(1);
        test.add(2);
        test.add(3);
        assertArrayEquals(other, test.toArray());
    }

    @Test
    public void testAddingFullCapacity() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(2);
        Object[] other = {1, 2, 3};
        test.add(1);
        test.add(2);
        test.add(3);
        assertArrayEquals(other, test.toArray());
        assertEquals(4, test.getCapacity());
    }

    @Test
    public void testRemoveObject() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        Object[] other = {1, 3};
        test.add(1);
        test.add(2);
        test.add(3);
        Object two = 2;
        test.remove(two);
        assertArrayEquals(other, test.toArray());
        assertEquals(2, test.size());
    }

    @Test
    public void testRemoveNonexistingObject() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        Object two = 2;
        assertFalse(test.remove(two));
    }

    @Test
    public void testSize() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(2);
        test.add(3);
        Object two = 2;
        test.remove(two);
        assertEquals(2, test.size());
    }

    @Test
    public void testContains() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        assertFalse(test.contains(2));
        assertTrue(test.contains(4));
    }

    @Test
    public void testClear() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        Object[] other = new Object[0];
        test.clear();
        assertArrayEquals(other, test.toArray());
    }

    @Test
    public void testgetWithIndex() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        assertEquals(3, test.get(2));
    }

    @Test
    public void testGetWithWrongIndex() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        assertThrows(IndexOutOfBoundsException.class, () -> test.get(-1));
        assertThrows(IndexOutOfBoundsException.class, () -> test.get(7));
    }
    @Test
    public void testInsert() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        test.insert(2, 1);
        Object[] other = {1,2,4,3};
        assertArrayEquals(other, test.toArray());
    }
    @Test
    public void testInsertWrongIndex() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        assertThrows(IndexOutOfBoundsException.class, ()->test.insert(2,-1));
        assertThrows(IndexOutOfBoundsException.class, ()->test.insert(2,8));
    }
    @Test
    public void testInsertNull() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        assertThrows(NullPointerException.class, ()->test.insert(null,1));
    }

    @Test
    public void testIndexOf() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        assertEquals(1, test.indexOf(4));
        assertEquals(-1, test.indexOf(5));
    }

    @Test
    public void testRemoveIndex() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        Object[] other = {1,3};
        test.remove(1);
        assertArrayEquals(other, test.toArray());
        Object[] other2 = {3};
        test.remove(0);
        assertArrayEquals(other2, test.toArray());
    }
    @Test
    public void testRemoveInvalidIndex() {
        ArrayIndexedCollection test = new ArrayIndexedCollection(3);
        test.add(1);
        test.add(4);
        test.add(3);
        assertThrows(IndexOutOfBoundsException.class, ()->test.remove(3));
    }


}
