package hr.fer.oprpp1.hw01;

import org.junit.jupiter.api.Test;

import java.awt.event.MouseAdapter;

import static org.junit.jupiter.api.Assertions.*;

public class ComplexNumberTest {
    @Test
    public void testDefaultConstructor() {
        ComplexNumber c = new ComplexNumber(0, 1);
        assertEquals("i", c.toString());
    }

    @Test
    public void testFromReal() {
        ComplexNumber other = new ComplexNumber(10, 0);
        ComplexNumber test = ComplexNumber.fromReal(10);
        assertEquals(other.getReal(), test.getReal());
        assertEquals(other.getImaginary(), test.getImaginary());
    }

    @Test
    public void testFromImaginary() {
        ComplexNumber other = new ComplexNumber(0, 10);
        ComplexNumber test = ComplexNumber.fromImaginary(10);
        assertEquals(other.getReal(), test.getReal());
        assertEquals(other.getImaginary(), test.getImaginary());
    }

    @Test
    public void testFromMagnitudeAndAngle() {
        ComplexNumber other = new ComplexNumber(1, 1);
        ComplexNumber test = ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(2), Math.PI / 4);
        assertEquals(other.getReal(), test.getReal());
        assertEquals(other.getImaginary(), test.getImaginary());
    }

    @Test
    public void testFromMagnitudeAndAngle2() {
        ComplexNumber other = new ComplexNumber(0, 1);
        ComplexNumber test = ComplexNumber.fromMagnitudeAndAngle(1, Math.PI / 2);
        assertEquals(other.getReal(), test.getReal());
        assertEquals(other.getImaginary(), test.getImaginary());
    }

    @Test
    public void testParsePM() {
        String s = "2.5-3i";
        ComplexNumber test = ComplexNumber.parse(s);
        assertEquals(2.5, test.getReal());
        assertEquals(-3, test.getImaginary());
    }

    @Test
    public void testParsePP() {
        String s = "2.5+3i";
        ComplexNumber test = ComplexNumber.parse(s);
        assertEquals(2.5, test.getReal());
        assertEquals(3, test.getImaginary());
    }

    @Test
    public void testParseMP() {
        String s = "-2.5+3i";
        ComplexNumber test = ComplexNumber.parse(s);
        assertEquals(-2.5, test.getReal());
        assertEquals(3, test.getImaginary());
    }

    @Test
    public void testParseMM() {
        String s = "-2.5-3i";
        ComplexNumber test = ComplexNumber.parse(s);
        assertEquals(-2.5, test.getReal());
        assertEquals(-3, test.getImaginary());
    }

    @Test
    public void testParseLeadingPlus() {
        String s = "+2.5-3i";
        ComplexNumber test = ComplexNumber.parse(s);
        assertEquals(2.5, test.getReal());
        assertEquals(-3, test.getImaginary());
    }

    @Test
    public void testParseZero() {
        String s = "0";
        ComplexNumber test = ComplexNumber.parse(s);
        assertEquals(0, test.getReal());
        assertEquals(0, test.getImaginary());
    }

    @Test
    public void testParseI() {
        String s = "i";
        ComplexNumber test = ComplexNumber.parse(s);
        assertEquals(0, test.getReal());
        assertEquals(1, test.getImaginary());
    }

    @Test
    public void testParseMI() {
        String s = "-i";
        ComplexNumber test = ComplexNumber.parse(s);
        assertEquals(0, test.getReal());
        assertEquals(-1, test.getImaginary());
    }

    @Test
    public void testParseInvalid() {
        String s = "-+i";
        assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse(s));
        String s2 = "+-i";
        assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse(s2));
    }

    @Test
    public void testParseInvalid2() {
        String s = "idk";
        assertThrows(IllegalArgumentException.class, () -> ComplexNumber.parse(s));
    }

    @Test
    public void testGetReal() {
        ComplexNumber c = new ComplexNumber(1, 0);
        assertEquals(1, c.getReal());
    }

    @Test
    public void testGetImaginary() {
        ComplexNumber c = new ComplexNumber(0, 1);
        assertEquals(1, c.getImaginary());
    }

    @Test
    public void testGetMagnitude() {
        ComplexNumber c = new ComplexNumber(1, 1);
        assertEquals(Math.sqrt(2), c.getMagnitude());
    }

    @Test
    public void testGetAngle() {
        ComplexNumber c = new ComplexNumber(1, 1);
        assertEquals(Math.PI / 4, c.getAngle());
    }

    @Test
    public void testAdd() {
        ComplexNumber test = new ComplexNumber(1, 1);
        ComplexNumber other = new ComplexNumber(2, 0);
        ComplexNumber sum = test.add(other);
        assertEquals(3, sum.getReal());
        assertEquals(1, sum.getImaginary());
    }

    @Test
    public void testSub() {
        ComplexNumber test = new ComplexNumber(1, 1);
        ComplexNumber other = new ComplexNumber(2, 0);
        ComplexNumber sum = test.sub(other);
        assertEquals(-1, sum.getReal());
        assertEquals(1, sum.getImaginary());
    }

    @Test
    public void testMul() {
        ComplexNumber test = new ComplexNumber(1, 1);
        ComplexNumber other = new ComplexNumber(2, 1);
        ComplexNumber mul = test.mul(other);
        assertEquals(1, mul.getReal());
        assertEquals(3, mul.getImaginary());
    }


    @Test
    public void testDiv() {
        ComplexNumber test = new ComplexNumber(3, 2);
        ComplexNumber other = new ComplexNumber(4, -3);
        ComplexNumber div = test.div(other);
        assertEquals(6.0 / 25.0, div.getReal());
        assertEquals(17.0 / 25.0, div.getImaginary());
    }

    @Test
    public void testDivZero() {
        ComplexNumber test = new ComplexNumber(3, 2);
        ComplexNumber other = new ComplexNumber(0, 0);
        assertThrows(ArithmeticException.class, () -> test.div(other));
    }

    @Test
    public void testPower() {
        ComplexNumber test = new ComplexNumber(3, 2);
        assertEquals(5, test.power(2).getReal());
        assertEquals(12, test.power(2).getImaginary());
    }


    @Test
    public void testPowerInvalid() {
        ComplexNumber test = new ComplexNumber(3, 2);
        assertThrows(IllegalArgumentException.class, () -> test.power(-1));
    }


    @Test
    public void testRoot() {
        ComplexNumber test = new ComplexNumber(2, 2);
        ComplexNumber[] result = test.root(3);
        ComplexNumber first = ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(2), -7.0 * Math.PI / 12.0);
        ComplexNumber second = ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(2), Math.PI / 12.0);
        ComplexNumber third = ComplexNumber.fromMagnitudeAndAngle(Math.sqrt(2), 3 * Math.PI / 4.0);

        assertEquals(first.getReal(), result[2].getReal());
        assertEquals(first.getImaginary(), result[2].getImaginary());
        assertEquals(second.getReal(), result[0].getReal());
        assertEquals(second.getImaginary(), result[0].getImaginary());
        assertEquals(third.getReal(), result[1].getReal());
        assertEquals(third.getImaginary(), result[1].getImaginary());
    }

    @Test
    public void testRootInvalid() {
        ComplexNumber test = new ComplexNumber(2, 2);
        assertThrows(IllegalArgumentException.class, () -> test.root(-2));
    }

    @Test
    public void testToString() {
        ComplexNumber i = new ComplexNumber(0, 1);
        ComplexNumber minus_i = new ComplexNumber(0, -1);
        ComplexNumber one = new ComplexNumber(1, 0);
        ComplexNumber minus_one = new ComplexNumber(-1, 0);
        ComplexNumber zero = new ComplexNumber(0, 0);
        ComplexNumber mm = new ComplexNumber(-3, -3);
        ComplexNumber pp = new ComplexNumber(3, 3);
        ComplexNumber pm = new ComplexNumber(3, -3);
        ComplexNumber mp = new ComplexNumber(-3, 3);

        assertEquals("i", i.toString());
        assertEquals("-i", minus_i.toString());
        assertEquals("1.0", one.toString());
        assertEquals("-1.0", minus_one.toString());
        assertEquals("0", zero.toString());
        assertEquals("-3.0-3.0i", mm.toString());
        assertEquals("3.0+3.0i", pp.toString());
        assertEquals("3.0-3.0i", pm.toString());
        assertEquals("-3.0+3.0i", mp.toString());
    }

}
