package hr.fer.oprpp1.hw01;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Represents an unmodifiable complex number
 */
public class ComplexNumber {
    private final double real;
    private final double imaginary;

    /**
     * Initializes a new complex number
     *
     * @param real      real part
     * @param imaginary imaginary part
     */
    public ComplexNumber(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    /**
     * Initializes a new complex number from real number
     *
     * @param real real part of complex number
     * @return new ComplexNumber
     */
    public static ComplexNumber fromReal(double real) {
        return new ComplexNumber(real, 0);
    }

    /**
     * Initializes a new complex number from imaginary number
     *
     * @param imaginary imaginary part of complex number
     * @return new ComplexNumber
     */
    public static ComplexNumber fromImaginary(double imaginary) {
        return new ComplexNumber(0, imaginary);
    }

    /**
     * Method which returns new ComplexNumber from given magnitude and angle
     *
     * @param magnitude magnitude
     * @param angle     angle
     * @return Complex number which is represented from given magnitude and angle
     * @throws IllegalArgumentException if magnitude is negative
     */
    public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
        if (magnitude < 0) throw new IllegalArgumentException("Magnitude has to be at least 0");
        double scale = Math.pow(10, 3);
        return new ComplexNumber(Math.round(magnitude * Math.cos(angle) * scale) / scale, Math.round(magnitude * Math.sin(angle) * scale) / scale);
    }

    /**
     * Method which returns new ComplexNumber from given String
     *
     * @param s given String
     * @return new ComplexNumber
     * @throws IllegalArgumentException if string cannot be parsed
     * @throws NullPointerException     if string is null
     */
    public static ComplexNumber parse(String s) {
        if (s == null) throw new NullPointerException("String must not be null");
        if (s.isBlank()) throw new NullPointerException("String must not be empty");

        s=s.trim();

        if(!s.matches("([+-]?\\s*\\d+(\\.\\d+)?\\s*([+-]?\\s*(\\d+(\\.\\d+)?)?\\s*i)?)|([+-]?\\s*(\\d+(\\.\\d+)?)?\\s*i)"))
            throw new IllegalArgumentException("Invalid string - cannot convert");

        double pm = 1.0;

        if (s.startsWith("+")) s = s.substring(1);
        else if (s.startsWith("-")) {
            s = s.substring(1);
            pm = -1.0;
        }

        String[] positive = s.split("\\+");
        String[] negative = s.split("-");

        if (positive.length == 2 && negative.length == 2)
            throw new IllegalArgumentException("Invalid string.Cannot convert");
        try {
            if (positive.length == 2) {
                double real = pm * Double.parseDouble(positive[0]);
                String img = positive[1].substring(0, positive[1].length() - 1);
                double imaginary;
                if (img.equals("")) imaginary = 1.0;
                else imaginary = Double.parseDouble(img);
                return new ComplexNumber(real, imaginary);
            } else if (negative.length == 2) {
                double real = pm * Double.parseDouble(negative[0]);
                String img = negative[1].substring(0, negative[1].length() - 1);
                double imaginary;
                if (img.equals("")) imaginary = 1.0;
                else imaginary = Double.parseDouble(img);
                return new ComplexNumber(real, -imaginary);
            } else if (positive.length == 1 && negative.length == 1) {
                double real = 0, imaginary = 0;
                if (s.endsWith("i")) {
                    if (s.length() == 1)
                        imaginary = pm;
                    else
                        imaginary = pm * Double.parseDouble(s.substring(0, s.length() - 1));
                } else {
                    real = pm * Double.parseDouble(s);
                }
                return new ComplexNumber(real, imaginary);
            } else {
                throw new IllegalArgumentException("Invalid string.Cannot convert");
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid string.Cannot convert");
        }
    }

    /**
     * Returns the real part of the complex number
     *
     * @return real part
     */
    public double getReal() {
        return real;
    }

    /**
     * Returns the imaginary part of the complex number
     *
     * @return imaginary part
     */
    public double getImaginary() {
        return imaginary;
    }

    /**
     * Returns magnitude from complex number
     *
     * @return magnitude
     */
    public double getMagnitude() {
        return Math.sqrt(real * real + imaginary * imaginary);
    }

    /**
     * Returns angle from complex number
     *
     * @return angle
     */
    public double getAngle() {
        if (real == 0) {
            if (imaginary == 0) return 0;
            else if (imaginary > 0) return Math.PI / 2.0;
            else return 3.0*Math.PI / 2.0;
        } else if (real < 0) {
            return Math.atan(imaginary / real) + Math.PI;
        } else {
            if (imaginary < 0)
                return Math.atan(imaginary / real) + 2.0 * Math.PI;
            else
                return Math.atan(imaginary / real);
        }

    }

    /**
     * Adds this complex number to other ComplexNumber c
     *
     * @param c other complex number
     * @return sum
     * @throws NullPointerException if other complex number is null
     */
    public ComplexNumber add(ComplexNumber c) {
        if (c == null) throw new NullPointerException("other complex number must not be null");
        return new ComplexNumber(c.real + this.real, c.imaginary + this.imaginary);
    }

    /**
     * Subtracts other ComplexNumber c from this ComplexNumber and returns it as new value
     *
     * @param c other ComplexNumber
     * @return new ComplexNumber which represents the sub
     * @throws NullPointerException if other complex number is null
     */
    public ComplexNumber sub(ComplexNumber c) {
        if (c == null) throw new NullPointerException("other complex number must not be null");
        return new ComplexNumber(this.real - c.real, this.imaginary - c.imaginary);
    }

    /**
     * Multiplies this complex number to other ComplexNumber c
     *
     * @param c other complex number
     * @return product
     * @throws NullPointerException if other complex number is null
     */
    public ComplexNumber mul(ComplexNumber c) {
        if (c == null) throw new NullPointerException("other complex number must not be null");
        double real = c.real * this.real - c.imaginary * this.imaginary;
        double imaginary = c.real * this.imaginary + c.imaginary * this.real;
        return new ComplexNumber(real, imaginary);
    }


    /**
     * Divides this complex number to other ComplexNumber c
     *
     * @param c other complex number
     * @return ComplexNumber result
     * @throws ArithmeticException  if dividing by zero
     * @throws NullPointerException if other complex number is null
     */
    public ComplexNumber div(ComplexNumber c) {
        if (c == null) throw new NullPointerException("other complex number must not be null");
        if (c.real == 0 && c.imaginary == 0) throw new ArithmeticException("Dividing by zero");
        ComplexNumber conjugate = new ComplexNumber(c.real, -c.imaginary);
        ComplexNumber upper = this.mul(conjugate);
        ComplexNumber lower = c.mul(conjugate);
        return new ComplexNumber(upper.real / lower.real, upper.imaginary / lower.real);
    }

    /**
     * Computes ComplexNumber to the power of n
     *
     * @param n power
     * @return ComplexNumber to the power of n
     * @throws IllegalArgumentException if n is less than 0
     */
    public ComplexNumber power(int n) {
        if (n < 0) throw new IllegalArgumentException("n must be at least 0");
        double magnitude = this.getMagnitude();
        double angle = this.getAngle();
        magnitude = Math.pow(magnitude, n);
        angle = n * angle;
        return fromMagnitudeAndAngle(magnitude, angle);
    }

    /**
     * Computes all roots of ComplexNumber
     *
     * @param n nth root
     * @return Array of new ComplexNumbers that are nth roots of the original
     * @throws IllegalArgumentException if n is less than 1
     */
    public ComplexNumber[] root(int n) {
        if (n < 1) throw new IllegalArgumentException("n must be at least 1");
        double angle = this.getAngle();
        double magnitude = this.getMagnitude();
        magnitude = Math.pow(magnitude, 1.0 / n);
        double k_angle;
        ComplexNumber[] array = new ComplexNumber[n];
        for (int i = 0; i < n; i++) {
            k_angle = (angle + 2.0 * i * Math.PI) / n;
            ComplexNumber c = fromMagnitudeAndAngle(magnitude, k_angle);
            array[i] = c;
        }
        return array;
    }

    /**
     * Converts ComplexNumber to String
     *
     * @return String which represents the given complex number
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (real == 0 || imaginary == 0) {
            if (real == 0 && imaginary == 0)
                sb.append(0);
            else if (real == 0) {
                if (imaginary == 1)
                    sb.append("i");
                else if (imaginary == -1)
                    sb.append("-i");
                else
                    sb.append(imaginary).append("i");
            } else
                sb.append(real);

        } else if (imaginary > 0) {
            if (imaginary == 1)
                sb.append(real).append("+").append("i");
            else
                sb.append(real).append("+").append(imaginary).append("i");
        } else {
            if (imaginary == -1)
                sb.append(real).append("-").append("i");
            else
                sb.append(real).append(imaginary).append("i");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComplexNumber that = (ComplexNumber) o;
        return Double.compare(that.real, real) == 0 &&
                Double.compare(that.imaginary, imaginary) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(real, imaginary);
    }
}