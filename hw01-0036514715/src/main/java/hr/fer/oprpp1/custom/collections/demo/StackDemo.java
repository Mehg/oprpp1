package hr.fer.oprpp1.custom.collections.demo;

import hr.fer.oprpp1.custom.collections.ObjectStack;

public class StackDemo {
    public static void main(String[] args) {
        String[] argument = args[0].split(" ");
        ObjectStack stack = new ObjectStack();

        if(argument.length==0) throw new Error("Invalid expression");
        for (String s : argument) {
            try {
                int number = Integer.parseInt(s);
                stack.push(number);
            } catch (NumberFormatException e) {
                if(stack.size()<2) throw new Error("Invalid expression");
                int second = (int) stack.pop();
                int first = (int) stack.pop();
                int result;
                if(second==0 && (s.equals("/")||s.equals("%"))){
                    throw new ArithmeticException("Division by zero");
                }
                switch (s) {
                    case "+" -> result = first + second;
                    case "-" -> result = first - second;
                    case "/" -> result = first / second;
                    case "*" -> result = first * second;
                    case "%" -> result = first % second;
                    default -> throw new IllegalArgumentException("Invalid expression");
                }
                stack.push(result);
            }
        }
        if(stack.size()!=1) throw new Error("Invalid expression");
        else System.out.println(stack.pop());
    }
}