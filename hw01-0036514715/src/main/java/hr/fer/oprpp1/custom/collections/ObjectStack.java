package hr.fer.oprpp1.custom.collections;

/**
 * Implementation of stack-backed collection of objects
 */
public class ObjectStack {
    private ArrayIndexedCollection stack;

    /**
     * Creates a new stack
     */
    public ObjectStack() {
        stack = new ArrayIndexedCollection();
    }

    /**
     * Checks if stack is empty
     *
     * @return true if empty, otherwise false
     */
    public boolean isEmpty() {
        return stack.isEmpty();
    }

    /**
     * Returns the size of the stack
     *
     * @return size of the stack
     */
    public int size() {
        return stack.size();
    }

    /**
     * Pushes new Object value on stack
     *
     * @param value which will be pushed on stack
     * @throws NullPointerException if value is null
     */
    public void push(Object value) {
        stack.add(value);
    }

    /**
     * Removes last value pushed on stack from stack and returns it
     * @return value from top of stack
     * @throws EmptyStackException if the stack is empty
     */
    public Object pop() {
        if (this.isEmpty()) throw new EmptyStackException("You're trying to pop from an empty stack");
        int stackHeight = this.size() - 1;
        Object top = stack.get(stackHeight);
        stack.remove(stackHeight);
        return top;
    }

    /**
     * Returns last element placed on stack; does not delete it from stack
     * @return value from top of stack
     * @throws EmptyStackException if the stack is empty
     */
    public Object peek(){
        if (this.isEmpty()) throw new EmptyStackException("You're trying to pop from an empty stack");
        return stack.get(this.size()-1);
    }

    /**
     * Removes all elements from stack
     */
    public void clear(){
        stack.clear();
    }
}
