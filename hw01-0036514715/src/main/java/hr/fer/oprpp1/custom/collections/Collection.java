package hr.fer.oprpp1.custom.collections;

/**
 * class which represents some general collection of objects
 */
public class Collection {
    protected Collection() {
    }

    /**
     * Checks whether the collection is empty
     *
     * @return true if collection contains no objects and false otherwise
     */
    public boolean isEmpty() {
        return this.size() == 0;
    }

    /**
     * Returns the size of the collection
     *
     * @return the number of currently stored objects in this collections
     */
    public int size() {
        return 0;
    }

    /**
     * Adds the given object into this collection
     *
     * @param value Object which will be added in the collection
     */
    public void add(Object value) {

    }

    /**
     * Method which checks if given Object is contained in the collection
     *
     * @param value Object which will be checked if it is in the collection
     * @return Returns true only if the collection contains given value, as determined by equals method
     */
    public boolean contains(Object value) {
        return true;
    }

    /**
     * Method which removes an instance of the given Object from the collection
     *
     * @param value Object whose instance will be removed from the collection
     * @return Returns true only if the collection contains given value and removes one occurrence of it
     */
    public boolean remove(Object value) {
        return false;
    }

    /**
     * Allocates new array with size equals to the size of this collections, fills it with collection content and
     * returns the array
     *
     * @return New array filled with the collection
     */
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    /**
     * Method calls processor.process(.) for each element of this collection
     *
     * @param processor Given processor
     */
    public void forEach(Processor processor) {

    }

    /**
     * Method adds into the current collection all elements from the given collection.
     *
     * @param other Collection from which all elements will be added to this one. Remains unchanged
     */
    public void addAll(Collection other) {
        class LocalProcessor extends Processor {
            @Override
            public void process(Object value) {
                Collection.this.add(value);
            }
        }

        Processor l = new LocalProcessor();
        other.forEach(l);
    }

    /**
     * Removes all elements from this collection
     */
    public void clear() {

    }
}
