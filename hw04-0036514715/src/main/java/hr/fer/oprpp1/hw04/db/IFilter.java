package hr.fer.oprpp1.hw04.db;

/**
 * Interface which models a filter for StudentRecords
 */
public interface IFilter {
    /**
     * Method which checks whether it can accept the given StudentRecord
     * @param record given StudentRecord
     * @return true if it accepts, otherwise false
     */
    boolean accepts(StudentRecord record);
}
