package hr.fer.oprpp1.hw04.db.scripting;

/**
 * Enumeration of all TokenTypes
 */
public enum TokenType {
    COMPARISON_OPERATOR,
    VARIABLE,
    STRING_LITERAL,
    EOF
}
