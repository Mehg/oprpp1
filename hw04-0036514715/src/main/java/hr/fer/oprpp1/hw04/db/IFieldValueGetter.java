package hr.fer.oprpp1.hw04.db;

import hr.fer.oprpp1.hw04.db.StudentRecord;

/**
 * Interface which is responsible for getting requested filed values from StudentRecords
 */
public interface IFieldValueGetter {
    /**
     * Method which returns requested string values form given StudentRecord
     *
     * @param record given StudentRecord
     * @return requested String value
     */
    String get(StudentRecord record);
}
