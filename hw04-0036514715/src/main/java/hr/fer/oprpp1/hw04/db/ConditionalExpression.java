package hr.fer.oprpp1.hw04.db;

import java.util.Objects;

/**
 * Class which models a simple conditional expression
 */
public class ConditionalExpression {
    /**
     * Field getter which will get the field that needs to be compared to
     */
    private final IFieldValueGetter fieldGetter;
    /**
     * String which we use to compare gotten field with
     */
    private final String stringLiteral;
    /**
     * Comparison operator which we use
     */
    private final IComparisonOperator comparisonOperator;

    /**
     * Creates a new ConditionalExpression from given parameters
     * @param fieldGetter given field getter
     * @param stringLiteral given string literal
     * @param comparisonOperator given comparison operator
     */
    public ConditionalExpression(IFieldValueGetter fieldGetter, String stringLiteral, IComparisonOperator comparisonOperator) {
        this.fieldGetter = fieldGetter;
        this.stringLiteral = stringLiteral;
        this.comparisonOperator = comparisonOperator;
    }

    /**
     * Returns current field getter
     * @return current field getter
     */
    public IFieldValueGetter getFieldGetter() {
        return fieldGetter;
    }

    /**
     * Returns current string literal
     * @return current string literal
     */
    public String getStringLiteral() {
        return stringLiteral;
    }

    /**
     * Returns current comparison operator
     * @return current comparison operator
     */
    public IComparisonOperator getComparisonOperator() {
        return comparisonOperator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConditionalExpression that = (ConditionalExpression) o;
        return Objects.equals(fieldGetter, that.fieldGetter) &&
                Objects.equals(stringLiteral, that.stringLiteral) &&
                Objects.equals(comparisonOperator, that.comparisonOperator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldGetter, stringLiteral, comparisonOperator);
    }
}
