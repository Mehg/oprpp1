package hr.fer.oprpp1.hw04.db.scripting;

/**
 * Simple implementation of a lexer for QueryParser purposes
 */
public class QueryLexer {
    /**
     * data that needs to be parsed
     */
    private final char[] data;
    /**
     * Last parsed token
     */
    private Token token;
    /**
     * Current index of parsing
     */
    private int currentIndex;

    /**
     * Creates a new QueryLexer which will create tokens from given text
     * @param text given text
     * @throws NullPointerException if given text is null
     */
    public QueryLexer(String text) {
        if(text==null) throw new NullPointerException("String text must not be null");
        this.data = text.toCharArray();
        currentIndex=0;
    }

    /**
     * Returns last created Token
     * @return last created Token
     */
    public Token getToken() {
        return token;
    }

    /**
     * Create and returns the next Token in line
     * @return next Token
     * @throws IllegalStateException if the whole text has been processed
     */
    public Token nextToken(){
        Token next;
        next = calculateToken();
        if(currentIndex== data.length && next == null){
            if(token !=null && token.getType().equals(TokenType.EOF)){
                throw new IllegalStateException("Lexer processed the whole text");
            }
            else
                next = new Token(TokenType.EOF, null);
        }
        token = next;
        return next;
    }

    /**
     * Private helper function which is used to create a new Token
     * @return next Token
     * @throws IllegalStateException if it couldn't create a new Token
     */
    private Token calculateToken(){
        Token next = null;

        while(currentIndex!= data.length){
            if(!Character.isWhitespace(data[currentIndex])){
                if(data[currentIndex]=='\"'){
                    next = getStringLiteral();
                    break;
                }else if (isComparisonOperator()){
                    next = getOperator();
                    break;
                }else if(Character.isLetter(data[currentIndex])){
                    next = getVariable();
                    break;
                }else
                    throw new IllegalStateException("Can't parse");
            }
            currentIndex++;
        }
        return next;
    }

    /**
     * Private helper function which determines whether next character is a valid operator
     * @return true if next character is a valid operator
     */
    private boolean isComparisonOperator(){
        return data[currentIndex]=='=' ||
                data[currentIndex]=='>' ||
                data[currentIndex]=='<' ||
                data[currentIndex]=='!';
    }

    /**
     * Returns next Token with type TokenType.COMPARISON_OPERATOR
     * @return next ComparisonOperator Token
     */
    private Token getOperator(){
        StringBuilder sb = new StringBuilder();
        while(currentIndex<data.length && isComparisonOperator()){
            sb.append(data[currentIndex++]);
        }
        return new Token(TokenType.COMPARISON_OPERATOR, sb.toString());
    }

    /**
     * Returns next Token with TokenType set to TokenType.STRING_LITERAL
     * @return next StringLiteral Token
     */
    private Token getStringLiteral(){
        StringBuilder sb = new StringBuilder();
        currentIndex++;
        while(data[currentIndex]!='"'){
            sb.append(data[currentIndex++]);
        }
        currentIndex++;
        return new Token(TokenType.STRING_LITERAL, sb.toString());
    }

    /**
     * Returns next Token with TokenType set to TokenType.VARIABLE
     * @return next Variable Token
     */
    private Token getVariable(){
        StringBuilder sb = new StringBuilder();
        while(currentIndex<data.length && Character.isLetter(data[currentIndex]) ){
            sb.append(data[currentIndex++]);
        }
        return new Token(TokenType.VARIABLE, sb.toString());

    }
}
