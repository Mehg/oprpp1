package hr.fer.oprpp1.hw04.db.scripting;

/**
 * Class which represents a Token used in QueryLexer
 */
public class Token {
    /**
     * TokenType
     */
    private final TokenType type;
    /**
     * Value of token
     */
    private final String value;

    /**
     * Creates a new Token from given parameters
     * @param type given TokenType
     * @param value given value
     */
    public Token(TokenType type, String value) {
        this.type = type;
        this.value = value;
    }

    /**
     * Returns the TokenType of current token
     * @return the TokenType of current token
     */
    public TokenType getType() {
        return type;
    }

    /**
     * Returns the value of current Token
     * @return the value of current Token
     */
    public String getValue() {
        return value;
    }
}
