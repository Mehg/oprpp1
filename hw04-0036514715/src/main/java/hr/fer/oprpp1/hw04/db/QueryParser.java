package hr.fer.oprpp1.hw04.db;

import hr.fer.oprpp1.hw04.db.scripting.QueryLexer;
import hr.fer.oprpp1.hw04.db.scripting.Token;
import hr.fer.oprpp1.hw04.db.scripting.TokenType;

import java.util.ArrayList;
import java.util.List;


/**
 * Simple parser used for parsing Query commands
 */
public class QueryParser {
    /**
     * Checks if query is direct query
     */
    private boolean isDirectQuery;
    /**
     * QueryLexer which the parser uses
     */
    private final QueryLexer lexer;
    /**
     * All conditional expression it could read
     */
    List<ConditionalExpression> conditionalExpressions;

    /**
     * Creates a new QueryParser from given queryString and parses the given string.
     * Result is stored in conditionalExpressions
     * @param queryString given String
     */
    public QueryParser(String queryString) {
        if (queryString.contains("query"))
            queryString = queryString.replace("query", "").trim();
        this.lexer = new QueryLexer(queryString);
        this.conditionalExpressions = new ArrayList<>();
        isDirectQuery = false;
        parse();
    }

    /**
     * Method used for parsing given queryString
     * @throws IllegalStateException if query is invalid
     */
    public void parse() {
        lexer.nextToken();
        while (isNotEnd()) {
            ConditionalExpression c = getOneConditionalExpression();
            conditionalExpressions.add(c);

            Token token = lexer.nextToken();

            if (!(token.getType().equals(TokenType.VARIABLE) && token.getValue().equalsIgnoreCase("AND")) && isNotEnd()) {
                throw new IllegalStateException("Invalid Query");
            }
            if(token.getType().equals(TokenType.VARIABLE)) lexer.nextToken();

        }
        if (conditionalExpressions.size() != 1 && isDirectQuery) {
            isDirectQuery = false;
        }

    }

    /**
     * Returns if the Query is a direct query
     * @return if the query is a direct query
     */
    boolean isDirectQuery() {
        return isDirectQuery;
    }

    /**
     * Returns value of string literal if query was direct query
     * @return value of string literal
     */
    String getQueriedJMBAG() {
        if(isDirectQuery)
            return conditionalExpressions.get(0).getStringLiteral();
        else throw new IllegalStateException("Can't request queried JMBAG  if not a direct Query");
    }

    /**
     * Returns all ConditionalExpressions that QueryParser has parsed
     * @return list of ConditionalExpressions
     */
    List<ConditionalExpression> getQuery() {
        return conditionalExpressions;
    }

    /**
     * Private helper method which checks if next token is EOF
     * @return true if EOF
     */
    private boolean isNotEnd() {
        return !lexer.getToken().getType().equals(TokenType.EOF);
    }

    /**
     * Method used for parsing one ConditionalExpression at a time
     * @return parsed ConditionalExpression
     * @throws IllegalStateException if something went wrong while parsing
     */
    private ConditionalExpression getOneConditionalExpression() {
        boolean jmbag = false, equals = false;
        Token token = lexer.getToken();

        if (!token.getType().equals(TokenType.VARIABLE)) {
            throw new IllegalStateException("query should start with variable");
        }

        IFieldValueGetter variable = switch (token.getValue()) {
            case "firstName" -> FieldValueGetters.FIRST_NAME;
            case "lastName" -> FieldValueGetters.LAST_NAME;
            case "jmbag" -> FieldValueGetters.JMBAG;
            default -> throw new IllegalStateException("Wrong field value");
        };
        if (token.getValue().equals("jmbag")) jmbag = true;

        token = lexer.nextToken();
        if (token.getType().equals(TokenType.EOF) || token.getType().equals(TokenType.STRING_LITERAL))
            throw new IllegalStateException("Not an operator");

        IComparisonOperator operator;
        if (token.getType().equals(TokenType.COMPARISON_OPERATOR)) {
            operator = switch (token.getValue()) {
                case ">" -> ComparisonOperators.GREATER;
                case ">=" -> ComparisonOperators.GREATER_OR_EQUALS;
                case "<=" -> ComparisonOperators.LESS_OR_EQUALS;
                case "<" -> ComparisonOperators.LESS;
                case "=" -> ComparisonOperators.EQUALS;
                case "!=" -> ComparisonOperators.NOT_EQUALS;
                default -> throw new IllegalStateException("Wrong operator");
            };
        } else if (token.getValue().equals("LIKE")) {
            operator = ComparisonOperators.LIKE;
        } else
            throw new IllegalStateException("Wrong operator");

        if (token.getValue().equals("=")) equals = true;

        token = lexer.nextToken();
        if (!token.getType().equals(TokenType.STRING_LITERAL))
            throw new IllegalStateException("Not a string literal");
        String stringLiteral = token.getValue();

        if (jmbag && equals) isDirectQuery = true;

        return new ConditionalExpression(variable, stringLiteral, operator);
    }

}

