package hr.fer.oprpp1.hw04.db;

import hr.fer.oprpp1.hw04.db.IFieldValueGetter;
import hr.fer.oprpp1.hw04.db.StudentRecord;

public class FieldValueGetters {
    public static final IFieldValueGetter FIRST_NAME = StudentRecord::getName;
    public static final IFieldValueGetter LAST_NAME = StudentRecord::getSurname;
    public static final IFieldValueGetter JMBAG = StudentRecord::getJmbag;
}
