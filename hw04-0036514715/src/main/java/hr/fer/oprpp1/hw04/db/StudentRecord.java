package hr.fer.oprpp1.hw04.db;

import java.util.Objects;

/**
 * Class which represents a StudentRecord
 */
public class StudentRecord {
    /**
     * student's jmbag - unique
     */
    private final String jmbag;
    /**
     * students surname
     */
    private final String surname;
    /**
     * students name
     */
    private final String name;
    /**
     * students grade
     */
    private final int grade;

    /**
     * Creates a new Student Record from given parameters
     * @param jmbag given jmbag - unique
     * @param surname given surname
     * @param name given name
     * @param grade given grade
     * @throws NullPointerException if jmbag, surname or name is null
     * @throws IllegalArgumentException if grade is less than one or greater than 5 and if length of jmbag isn't 10
     */
    public StudentRecord(String jmbag, String surname, String name, int grade) {
        if(jmbag==null || surname==null || name==null)
            throw new NullPointerException("jmbag, surname and name must not be null");
        this.jmbag = jmbag;
        if(jmbag.length()!=10)
            throw new IllegalArgumentException("jmbag must be exactly 10 characters long");
        this.surname = surname;
        this.name = name;
        if(grade<1 || grade>5)
            throw new IllegalArgumentException("Grade must be between 1 and 5");
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentRecord that = (StudentRecord) o;
        return Objects.equals(jmbag, that.jmbag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jmbag);
    }

    /**
     * Returns current jmbag
     * @return current jmbag
     */
    public String getJmbag() {
        return jmbag;
    }

    /**
     * returns current surname
     * @return current surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Returns current name
     * @return current name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns current grade
     * @return current grade
     */
    public int getGrade() {
        return grade;
    }
}
