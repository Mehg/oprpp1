package hr.fer.oprpp1.hw04.db;

/**
 * Class which defines some comparison operators
 * Defined operators are: <, <=, >, >=, =, like
 */
public class ComparisonOperators {
    /**
     * Implementation of comparison operator less - <
     */
    public static final IComparisonOperator LESS = (value1, value2) -> value1.compareTo(value2) < 0;
    /**
     * Implementation of comparison operator less or equal - <=
     */
    public static final IComparisonOperator LESS_OR_EQUALS = ((value1, value2) -> value1.compareTo(value2) <= 0);
    /**
     * Implementation of comparison operator greater - >
     */
    public static final IComparisonOperator GREATER = ((value1, value2) -> value1.compareTo(value2) > 0);
    /**
     * Implementation of comparison operator greater or equal - >=
     */
    public static final IComparisonOperator GREATER_OR_EQUALS = ((value1, value2) -> value1.compareTo(value2) >= 0);
    /**
     * Implementation of comparison operator equals - =
     */
    public static final IComparisonOperator EQUALS = (String::equals);
    public static final IComparisonOperator NOT_EQUALS = (value1, value2) -> !value1.equals(value2);
    /**
     * Implementation of comparison operator like - can contain wildcard *
     */
    public static final IComparisonOperator LIKE = (value1, value2) -> {
        if (value2.chars().filter(ch->ch=='*').count() > 1) throw new IllegalArgumentException("More than one * used");

        String[] parts = value2.split("\\*");
        if (value2.endsWith("*")) {
            return value1.startsWith(value2.substring(0, value2.length() - 1));
        } else if (value2.startsWith("*")) {
            return value1.endsWith(value2.substring(1));
        } else {
            if (parts[0].length() + parts[1].length() <= value1.length()) {
                return value1.startsWith(parts[0]) && value1.endsWith(parts[1]);
            }
            return false;
        }
    };

}
