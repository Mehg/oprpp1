package hr.fer.oprpp1.hw04.db;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class which represents a database of StudentRecords
 */
public class StudentDatabase {
    /**
     * Map of student JMBAG-s and StudentRecords for fast indexing
     */
    private final Map<String, StudentRecord> index;
    /**
     * List of StudentRecords
     */
    private final List<StudentRecord> listOfStudents;

    /**
     * Creates a new StudentDatabase and adds all students from given list of strings
     * @param students given list of strings
     * @throws IllegalArgumentException if trying to add student with duplicate jmbag
     */
    public StudentDatabase(List<String> students) {
        this.index = new HashMap<>();
        this.listOfStudents = new ArrayList<>();

        for (String student : students) {
            student = student.strip();
            String[] studentParameters = student.split("\t");

            String jmbag = studentParameters[0];
            if (index.containsKey(jmbag))
                throw new IllegalArgumentException("Student with given jmbag already exists");

            String surname = studentParameters[1];
            String name = studentParameters[2];
            int grade = Integer.parseInt(studentParameters[3]);

            StudentRecord sr = new StudentRecord(jmbag, surname, name, grade);
            index.put(jmbag, sr);
            listOfStudents.add(sr);

        }
    }

    /**
     * Returns StudentRecord with given jmbag with fast indexing
     * @param jmbag given jmbag
     * @return StudentRecord with given jmbag
     */
    public StudentRecord forJMBAG(String jmbag) {
        return index.get(jmbag);
    }

    /**
     * Returns a list of StudentRecords that satisfy the method accepts of given IFilter
     * @param filter given IFilter
     * @return list of StudentRecords that satisfy the method accepts of given IFilter
     */
    public List<StudentRecord> filter(IFilter filter) {
        return listOfStudents.stream().filter(filter::accepts).collect(Collectors.toList());
    }

}
