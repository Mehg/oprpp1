package hr.fer.oprpp1.hw04.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Class which loads database from database.txt and handles user input
 */
public class StudentDB {

    public static void main(String[] args) {
        StudentDatabase sd = readFromDatabase();


        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("> ");
            String line = sc.nextLine().trim();
            if (line.startsWith("query")) {
                try {
                    List<StudentRecord> records = selectFromDatabase(line, sd);
                    List<String> output = format(records);
                    output.forEach(System.out::println);
                    System.out.println();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            } else if (line.equalsIgnoreCase("exit")) {
                System.out.println("Goodbye!");
                System.exit(0);
            } else {
                System.out.println("Query command missing");
            }
        }


    }

    /**
     * Method for reading from database
     * @return new StudentDatabase
     */
    private static StudentDatabase readFromDatabase() {
        try {
            List<String> lines = Files.readAllLines(
                    Paths.get("./src/resources/database.txt"),
                    StandardCharsets.UTF_8
            );
            return new StudentDatabase(lines);

        } catch (IOException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
        return null;
    }

    /**
     * Method which selects from StudentDatabase the user input
     * @param line user input
     * @param sd StudentDatabase
     * @return ArrayList of StudentRecords that were selected from the database
     */
    private static List<StudentRecord> selectFromDatabase(String line, StudentDatabase sd) {
        if (line.contains("query"))
            line = line.replace("query", "").trim();
        QueryParser parser = new QueryParser(line);
        ArrayList<StudentRecord> list = new ArrayList<>();

        if (parser.isDirectQuery()) {
            System.out.println("Using index for record retrieval.");
            StudentRecord sr = sd.forJMBAG(parser.getQueriedJMBAG());
            if (sr != null)
                list.add(sd.forJMBAG(parser.getQueriedJMBAG()));
            return list;
        }
        list.addAll(sd.filter(new QueryFilter(parser.getQuery())));
        return list;
    }

    /**
     * Method which formats a table from given list of StudentRecords
     * @param records given StudentRecords
     * @return List of Strings which represent a table
     */
    private static List<String> format(List<StudentRecord> records) {
        ArrayList<String> text = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        if (records.isEmpty()) {
            text.add("Records selected: 0");
            return text;
        }

        int longestName = records.stream().mapToInt(r -> r.getName().length()).max().getAsInt();
        @SuppressWarnings("OptionalGetWithoutIsPresent")
        int longestSurname = records.stream().mapToInt(r -> r.getSurname().length()).max().getAsInt();

        char[] chars = new char[12];

        Arrays.fill(chars, '-');
        sb.append("+").append(String.valueOf(chars));

        chars = new char[longestSurname + 2];
        Arrays.fill(chars, '-');
        sb.append("+").append(String.valueOf(chars));

        chars = new char[longestName + 2];
        Arrays.fill(chars, '-');
        sb.append("+").append(String.valueOf(chars));

        chars = new char[3];
        Arrays.fill(chars, '-');
        sb.append("+").append(String.valueOf(chars)).append("+");

        text.add(sb.toString());
        sb = new StringBuilder();


        for (StudentRecord s : records) {
            sb.append("| ").append(s.getJmbag())
                    .append(" | ").append(s.getSurname())
                    .append(" ".repeat(Math.max(0, longestSurname - s.getSurname().length())))
                    .append(" | ").append(s.getName())
                    .append(" ".repeat(Math.max(0, longestName - s.getName().length())))
                    .append(" | ").append(s.getGrade())
                    .append(" |");
            text.add(sb.toString());
            sb = new StringBuilder();
        }

        text.add(text.get(0));
        text.add("Records selected: " + records.size());
        return text;

    }

}
