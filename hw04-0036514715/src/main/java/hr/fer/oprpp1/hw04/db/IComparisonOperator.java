package hr.fer.oprpp1.hw04.db;

/**
 * Class which represents an comparison operator between two strings
 */
public interface IComparisonOperator {
    /**
     * Checks if comparison between first and second string is satisfied
     * @param value1 first string
     * @param value2 second string
     * @return true if comparison is satisfied, otherwise false
     */
    boolean satisfied(String value1, String value2);

}
