package hr.fer.oprpp1.hw04.db;


import java.util.List;

/**
 * Class used for filtering StudentRecords
 */
public class QueryFilter implements IFilter{
    /**
     * List of conditional Expression the record must satisfy
     */
    private final List<ConditionalExpression> conditionalExpressionList;

    /**
     * Creates a new Query Filter from given parameters
     * @param conditionalExpressionList given list of ConditionalExpressions
     */
    public QueryFilter(List<ConditionalExpression> conditionalExpressionList) {
        this.conditionalExpressionList = conditionalExpressionList;
    }

    @Override
    public boolean accepts(StudentRecord record) {
        for(ConditionalExpression c : conditionalExpressionList){
            boolean accepts = c.getComparisonOperator().satisfied(c.getFieldGetter().get(record), c.getStringLiteral());
            if(!accepts){
                return false;
            }
        }
        return true;
    }
}
