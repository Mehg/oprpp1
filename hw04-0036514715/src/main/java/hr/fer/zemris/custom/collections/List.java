package hr.fer.zemris.custom.collections;

/**
 * Interface which represents a simple list
 * @param <T> given elements in the list
 */
public interface List<T> extends Collection<T> {
    /**
     * Method used for getting value at given index
     * @param index given index
     * @return returns value at given index
     */
    T get(int index);

    /**
     * Inserts given value at given position, while shifting other values for one place
     * @param value given value
     * @param position given position
     */
    void insert(T value, int position);

    /**
     * Returns the index of given value in current list
     * @param value give value
     * @return index of given value in current list
     */
    int indexOf(Object value);

    /**
     * Removes value at given index
     * @param index given index
     */
    void remove(int index);
}
