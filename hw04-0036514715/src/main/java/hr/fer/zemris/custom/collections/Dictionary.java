package hr.fer.zemris.custom.collections;

import java.util.ArrayList;
import java.util.Objects;

/**
 * An implementation of a dictionary - it maps keys to values
 *
 * @param <K> class of keys
 * @param <V> class of values
 */
public class Dictionary<K, V> {
    /**
     * Private class which represents an entry in the dictionary
     *
     * @param <K> class of keys
     * @param <V> class of values
     */
    private static class Entry<K, V> {
        private final K key;
        private final V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Entry<?, ?> entry = (Entry<?, ?>) o;
            return Objects.equals(key, entry.key);
        }
    }

    private final ArrayList<Entry<K, V>> dictionary;

    /**
     * Creates a new instance of a dictionary
     */
    public Dictionary() {
        this.dictionary = new ArrayList<>();
    }

    /**
     * Checks whether the dictionary is empty
     *
     * @return
     */
    public boolean isEmpty() {
        return dictionary.isEmpty();
    }

    /**
     * Returns the number of entries in the dictionary
     *
     * @return number of entries
     */
    public int size() {
        return dictionary.size();
    }

    /**
     * Removes all entries from the dictionary
     */
    public void clear() {
        dictionary.clear();
    }

    /**
     * Puts given key and value in the dictionary.
     * If an entry with given key already existed, replaces the old value with the new one and returns the old value.
     * If the given key isn't already in the dictionary returns null
     *
     * @param key   given <code>K</code> key
     * @param value given <code>V</code> value
     * @return old value if key already exists, otherwise null
     * @throws NullPointerException if key is null
     */
    public V put(K key, V value) {
        if (key == null) throw new NullPointerException("Key cannot be null");
        Entry<K, V> entry = new Entry<>(key, value);
        V oldval = null;
        if (dictionary.contains(entry)) {
            int i = dictionary.indexOf(entry);
            oldval = dictionary.get(i).value;
            dictionary.remove(entry);
        }
        dictionary.add(entry);
        return oldval;
    }

    /**
     * Returns the value for given key. If key does not exist in the dictionary returns null
     * @param key given key
     * @return value if entry with given key exists, otherwise null
     */
    public V get(Object key) {
        if(key==null) return null;
        Entry<K, V> entry = new Entry<>((K) key, null);
        if (dictionary.contains(entry)) {
            int i = dictionary.indexOf(entry);
            return dictionary.get(i).value;
        } else return null;
    }

    /**
     * Removes an entry from dictionary with given key and returns old value.
     * If dictionary doesn't contain an entry with given key returns null
     * @param key given key
     * @return old value if entry with given key as key exists, otherwise null
     */
    public V remove(K key) {
        Entry<K, V> entry = new Entry<>(key, null);
        if (dictionary.contains(entry)) {
            int i = dictionary.indexOf(entry);
            V val = dictionary.get(i).value;
            dictionary.remove(entry);
            return val;
        }
        return null;
    }
}