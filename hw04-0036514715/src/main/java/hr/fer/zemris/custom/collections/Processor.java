    package hr.fer.zemris.custom.collections;
    
    /**
     * The Processor is a model of an object capable of performing some operation on the passed object
     */
    public interface Processor<T> {
        void process(T value);
    }
