package hr.fer.zemris.custom.collections;

/**
 * Interface which allows getting elements from a Collection one by one
 */
public interface ElementsGetter<T> {
    /**
     * Method which returns true if Collection has next element
     *
     * @return true if Collection has next element, otherwise false
     */
    boolean hasNextElement();

    /**
     * Method which returns next element from a Collection
     *
     * @return next element from a Collection
     */
    T getNextElement();

    /**
     * Method processes each (remaining) element
     * @param p given processor
     */
    default void processRemaining(Processor<? super T> p){
        while(hasNextElement()){
            p.process(getNextElement());
        }
    }
}
