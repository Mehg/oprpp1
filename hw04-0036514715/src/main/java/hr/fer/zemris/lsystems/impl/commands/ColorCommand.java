package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

import java.awt.*;

/**
 * Class which represents a ColorCommand - it changes the turtles color to given color in the constructor
 */
public class ColorCommand implements Command {
    /**
     * color to which the command will change the turtles color
     */
    private final Color color;

    /**
     * Creates a new ColorCommand with color set to given color
     * @param color given color
     * @throws NullPointerException if given color is null
     */
    public ColorCommand(Color color) {
        if(color==null) throw new NullPointerException("Color must not be null");
        this.color = color;
    }

    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.getCurrentState().setColor(color);
    }
}
