package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.custom.collections.ObjectStack;

/**
 * Class which represents a context - stores TurtleStates
 */
public class Context {
    /**
     * Stack of TurtleStates used for storing current context
     */
    private final ObjectStack<TurtleState> stack;

    /**
     * Creates a new Context
     */
    public Context() {
        this.stack = new ObjectStack<>();
    }

    /**
     * Returns the value at the top of the stack without removing it
     * @return current state
     */
    public TurtleState getCurrentState() {
        return stack.peek();
    }

    /**
     * Pushes given TurtleState on current Context which makes it the current state
     * @param state given TurtleState state
     */
    public void pushState(TurtleState state) {
        if (state == null) throw new NullPointerException("State must not be null");
        stack.push(state);
    }

    /**
     * Removes current state - pops it from stack
     */
    public void popState() {
        stack.pop();
    }

}
