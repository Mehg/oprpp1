package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Class which represents a ScaleCommand - it changes the scale of TurtleState at the top of the context
 */
public class ScaleCommand implements Command {
    /**
     * Scales the turtleState for given factor
     */
    private final double factor;

    /**
     * Creates a new ScaleCommand with given factor
     * @param factor given factor
     */
    public ScaleCommand(double factor) {
        this.factor = factor;
    }

    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.getCurrentState().setUnitLength(factor);
    }
}
