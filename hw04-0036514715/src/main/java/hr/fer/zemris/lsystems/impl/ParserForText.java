package hr.fer.zemris.lsystems.impl;


import java.util.Scanner;

/**
 * Parser used for parsing LSystemBuilderImpl configurations
 */
public class ParserForText {
    /**
     * Main method which parses LSystemBuilderImpl configurations
     * and sets the parsed configurations for the given LSystemBuilderImpl
     *
     * @param text           given text that needs to be parsed
     * @param lSystemBuilder given LSystemBuilderImpl for which we set the configuration
     */
    public static void parse(String text, LSystemBuilderImpl lSystemBuilder) {
        text = text.strip();
        Scanner sc;

        try {
            if (text.contains("origin")) {
                text = text.split("origin")[1];
                text = text.strip();
                sc = new Scanner(text);
                lSystemBuilder.setOrigin(sc.nextDouble(), sc.nextDouble());
                sc.close();

            } else if (text.contains("angle")) {
                text = text.split("angle")[1];
                text = text.strip();
                sc = new Scanner(text);
                lSystemBuilder.setAngle(sc.nextDouble());
                sc.close();

            } else if (text.contains("unitLengthDegreeScaler")) {
                text = text.split("unitLengthDegreeScaler")[1];
                text = text.strip();
                sc = new Scanner(text);
                if (text.contains("/")) {
                    String first = text.split("/")[0].strip();
                    String second = text.split("/")[1].strip();
                    lSystemBuilder.setUnitLengthDegreeScaler(Double.parseDouble(first) / Double.parseDouble(second));
                } else {
                    lSystemBuilder.setUnitLengthDegreeScaler(sc.nextDouble());
                }
                sc.close();

            } else if (text.contains("unitLength")) {
                text = text.split("unitLength")[1];
                text = text.strip();
                sc = new Scanner(text);

                lSystemBuilder.setUnitLength(sc.nextDouble());
                sc.close();

            } else if (text.contains("command")) {
                text = text.split("command")[1];
                text = text.strip();
                char first = text.charAt(0);
                text = text.substring(1).strip();
                lSystemBuilder.registerCommand(first, text);

            } else if (text.contains("axiom")) {
                text = text.split("axiom", 2)[1];
                text = text.strip();
                lSystemBuilder.setAxiom(text);

            } else if (text.contains("production")) {
                text = text.split("production", 2)[1];
                text = text.strip();
                sc = new Scanner(text);
                char first = text.charAt(0);
                String second = text.substring(1).strip();
                lSystemBuilder.registerProduction(first, second);
                sc.close();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Given text cannot be parsed");
        }

    }
}