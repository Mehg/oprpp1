package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.impl.commands.*;

import java.awt.*;

/**
 * Parser used for parsing commands in String form
 * Commands which can be parsed are ColorCommand, DrawCommand,
 * PopCommand, PushCommand, RotateCommand, ScaleCommand and SkipCommand
 */
public class ParserForCommands {
    /**
     * Method which parses from given text a Command
     * @param text given text
     * @return new Command
     * @throws IllegalArgumentException if text cannot be parsed
     */
    public static Command parse(String text) {
        text = text.strip();

        try {
            if (text.contains("draw")) {
                text = text.split("draw")[1];
                text = text.strip();
                return new DrawCommand(Double.parseDouble(text));

            } else if (text.contains("skip")) {
                text = text.split("skip")[1];
                text = text.strip();
                return new SkipCommand(Double.parseDouble(text));

            } else if (text.contains("scale")) {
                text = text.split("scale")[1];
                text = text.strip();
                return new ScaleCommand(Double.parseDouble(text));

            } else if (text.contains("rotate")) {
                text = text.split("rotate")[1];
                text = text.strip();
                return new RotateCommand(Double.parseDouble(text));

            } else if (text.contains("push")) {
                return new PushCommand();

            } else if (text.contains("pop")) {
                return new PopCommand();

            } else if (text.contains("color")) {
                text = text.split("color")[1];
                text = text.strip();
                return new ColorCommand(Color.decode("#"+text));

            } else throw new IllegalArgumentException("Not a valid command");
        } catch (Exception e) {
            throw new IllegalArgumentException("Not a valid command");
        }
    }

}