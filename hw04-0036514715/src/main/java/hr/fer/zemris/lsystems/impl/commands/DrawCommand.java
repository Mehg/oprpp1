package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.math.Vector2D;

/**
 * Class which represents a DrawCommand - it draws a line with the turtle for steps length
 */
public class DrawCommand implements Command {
    /**
     * number of steps the turtle will take
     */
    private final double step;

    /**
     * Creates a new DrawCommand with given number of steps
     * @param step given number of steps
     */
    public DrawCommand(double step) {
        this.step = step;
    }

    @Override
    public void execute(Context ctx, Painter painter) {
        TurtleState turtleState = ctx.getCurrentState();

        Vector2D startPosition = turtleState.getPosition();
        double scalar = turtleState.getUnitLength()*step;
        Vector2D directionScaled = turtleState.getDirection().scaled(scalar);
        Vector2D endPosition = startPosition.added(directionScaled);

        painter.drawLine(startPosition.getX(),
                startPosition.getY(),
                endPosition.getX(),
                endPosition.getY(),
                turtleState.getColor(),
                1f);

        turtleState.setPosition(endPosition);
    }
}
