package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.Painter;

/**
 * Interface which models a simple command
 */
public interface Command {
    /**
     * Method which executes the current command
     * @param ctx context
     * @param painter painter
     */
    void execute(Context ctx, Painter painter);
}
