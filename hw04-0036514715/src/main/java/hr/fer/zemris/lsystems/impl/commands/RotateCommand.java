package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Class which represents a RotateCommand - it rotates the TurtleState at the top of the Context for given angle
 */
public class RotateCommand implements Command {
    /**
     * Given angle in degrees
     */
    private final double angle;

    /**
     * Creates a new RotateCommand from given angle
     * @param angle given angle
     */
    public RotateCommand(double angle) {
        this.angle = angle;
    }

    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.getCurrentState().getDirection().rotate(Math.toRadians(angle));
    }
}
