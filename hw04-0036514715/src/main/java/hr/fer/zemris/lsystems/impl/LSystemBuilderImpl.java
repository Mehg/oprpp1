package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.custom.collections.Dictionary;

import hr.fer.zemris.lsystems.LSystem;
import hr.fer.zemris.lsystems.LSystemBuilder;
import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.math.Vector2D;

import java.awt.*;


/**
 * Class which builds a LSystem
 */
public class LSystemBuilderImpl implements LSystemBuilder {
    /**
     * Registered production which can be applied to the axiom
     */
    private final Dictionary<Character, String> registeredProductions;
    /**
     * Registered action which can be taken for each character
     */
    private final Dictionary<Character, Command> registeredActions;
    /**
     * Unit of length
     */
    private double unitLength = 0.1;
    /**
     * Unit of length degree scaler
     */
    private double unitLengthDegreeScaler = 1;
    /**
     * Origin from which the LSystem starts
     */
    private Vector2D origin = new Vector2D(0, 0);
    /**
     * Angle at which the LSystem starts
     */
    private double angle = 0;
    /**
     * Starting axiom
     */
    private String axiom = "";

    /**
     * Creates a new LSystemBuilderImpl
     */
    public LSystemBuilderImpl() {
        registeredProductions = new Dictionary<>();
        registeredActions = new Dictionary<>();
    }

    @Override
    public LSystemBuilder setUnitLength(double v) {
        unitLength = v;
        return this;
    }

    @Override
    public LSystemBuilder setOrigin(double v, double v1) {
        origin = new Vector2D(v, v1);
        return this;
    }

    @Override
    public LSystemBuilder setAngle(double v) {
        angle = v;
        return this;
    }

    @Override
    public LSystemBuilder setAxiom(String s) {
        if (s == null) throw new NullPointerException("Axiom must not be null");
        axiom = s;
        return this;
    }

    @Override
    public LSystemBuilder setUnitLengthDegreeScaler(double v) {
        unitLengthDegreeScaler = v;
        return this;
    }

    @Override
    public LSystemBuilder registerProduction(char c, String s) {
        if (s == null) throw new NullPointerException("Replacement string must not be null");
        registeredProductions.put(c, s);
        return this;
    }

    @Override
    public LSystemBuilder registerCommand(char c, String s) {
        if (s == null) throw new NullPointerException("Replacement string must not be null");
        registeredActions.put(c, ParserForCommands.parse(s));
        return this;
    }

    @Override
    public LSystemBuilder configureFromText(String[] strings) {
        for(String s : strings){
            ParserForText.parse(s, this);
        }
        return this;
    }

    @Override
    public LSystem build() {
        return new LindenmayerSystem();
    }

    /**
     * Class which represents the LSystem the current LSystemBuilderImpl built
     */
    private class LindenmayerSystem implements LSystem {
        @Override
        public String generate(int i) {
            String current = axiom;

            for (int j = 0; j < i; j++) {
                char[] currentArray = current.toCharArray();
                StringBuilder sb = new StringBuilder();

                for (char c : currentArray) {
                    String production = registeredProductions.get(c);

                    if (production == null) {
                        sb.append(c);
                    } else {
                        sb.append(production);
                    }
                }
                current = sb.toString();
            }
            return current;
        }

        @Override
        public void draw(int i, Painter painter) {
            Context context = new Context();

            Vector2D newOrigin = origin.copy();
            Vector2D newDirection = new Vector2D(1, 0);
            newDirection.rotate(Math.toRadians(angle));
            double newScalar = unitLength * Math.pow(unitLengthDegreeScaler, i);

            TurtleState turtleState = new TurtleState(newOrigin, newDirection, Color.black, newScalar);
            context.pushState(turtleState);

            for (char c : generate(i).toCharArray()) {
                Command action = registeredActions.get(c);
                if (action != null) {
                    action.execute(context, painter);
                }
            }
        }
    }
}
