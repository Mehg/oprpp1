package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;

/**
 * Class which represents a PopCommand - it will pop the last state from given context
 */
public class PopCommand implements Command {
    /**
     * Pops the last TurtleState from given context
     * @param ctx context given context
     * @param painter painter given painter
     */
    @Override
    public void execute(Context ctx, Painter painter) {
        ctx.popState();
    }
}
