package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.math.Vector2D;

import java.awt.*;

/**
 * Class which represents a Turtle like the one in Logo
 */
public class TurtleState {
    /**
     * Current position of the turtle - the radius vector of its current position
     */
    private Vector2D position;
    /**
     * Current direction the turtle faces
     */
    private Vector2D direction;
    /**
     * Color which the turtle paints with
     */
    private Color color;
    /**
     * The size of one unit length with which the turtle paints
     */
    private double unitLength;

    /**
     * Creates a new TurtleState from given parameters
     * @param position current position of the turtle
     * @param direction the direction the turtle faces
     * @param color the color with which the turtle paints with
     * @param unitLength size of one unit length
     * @throws NullPointerException if position, direction or color are null
     */
    public TurtleState(Vector2D position, Vector2D direction, Color color, double unitLength) {
        if (position == null || direction == null || color == null )
            throw new NullPointerException("Parameters must not be null");
        this.position = position;
        this.direction = direction;
        this.color = color;
        this.unitLength = unitLength;
    }

    /**
     * Returns a new TurtleState which is a copy of this TurtleState
     * @return copy of this TurtleState
     */
    public TurtleState copy() {
        return new TurtleState(getPosition().copy(), getDirection().copy(), getColor(), getUnitLength());
    }

    /**
     * Returns current position at which the turtle is at
     * @return current position of the turtle
     */
    public Vector2D getPosition() {
        return position;
    }

    /**
     * Sets current position of this TurtleState to given Vector2D position
     * @param position given Vector2D position
     * @throws NullPointerException if given position is null
     */
    public void setPosition(Vector2D position) {
        if(position==null) throw new NullPointerException("Position must not be null");
        this.position = position;
    }

    /**
     * Returns the current direction the TurtleState turtle faces
     * @return current direction the turtle faces
     */
    public Vector2D getDirection() {
        return direction;
    }

    /**
     * Sets the current direction the turtle faces to given Vector2D direction
     * @param direction given Vector2D direction
     * @throws NullPointerException if given direction is null
     */
    public void setDirection(Vector2D direction) {
        if(direction==null)throw new NullPointerException("Direction must not be null");
        this.direction = direction;
    }

    /**
     * Return the current color with which the TurtleState is painting with
     * @return current color
     */
    public Color getColor() {
        return color;
    }

    /**
     * Sets the color with which the turtle paints to given color
     * @param color given color
     * @throws NullPointerException if given color is null
     */
    public void setColor(Color color) {
        if(color==null) throw new NullPointerException("Color must not be null");
        this.color = color;
    }

    /**
     * Returns current unit of length
     * @return current unit of length
     */
    public double getUnitLength() {
        return unitLength;
    }

    /**
     * Sets unitLength to given unitLength
     * @param unitLength given unitLength
     */
    public void setUnitLength(double unitLength) {
        this.unitLength = unitLength;
    }
}
