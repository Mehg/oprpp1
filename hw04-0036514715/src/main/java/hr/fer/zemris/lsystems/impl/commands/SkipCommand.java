package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.Command;
import hr.fer.zemris.lsystems.impl.Context;
import hr.fer.zemris.lsystems.impl.TurtleState;
import hr.fer.zemris.math.Vector2D;

/**
 * Class which represents a SkipCommand
 * - it moves the TurtleState at the top of the context for given steps and doesn't paint a line
 */
public class SkipCommand implements Command {
    /**
     * Number of steps the turtle will take
     */
    private final double step;

    /**
     * Creates a new SkipCommand with step set to given step
     * @param step given step
     */
    public SkipCommand(double step) {
        this.step = step;
    }

    @Override
    public void execute(Context ctx, Painter painter) {
        TurtleState turtleState = ctx.getCurrentState();

        Vector2D startPosition = turtleState.getPosition();
        double scalar = turtleState.getUnitLength()*step;
        Vector2D directionScaled = turtleState.getDirection().scaled(scalar);
        Vector2D endPosition = startPosition.added(directionScaled);

        turtleState.setPosition(endPosition);

    }
}
