package hr.fer.oprpp1.hw04.db;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FieldValueGettersTest {
    @Test
    public void testFIRSTNAME() {
        StudentRecord record = new StudentRecord("0000000005", "Brezović", "Jusufadis", 5);
        assertEquals(record.getName(), FieldValueGetters.FIRST_NAME.get(record));
    }
    @Test
    public void testLASTNAME() {
        StudentRecord record = new StudentRecord("0000000005", "Brezović", "Jusufadis", 5);
        assertEquals(record.getSurname(), FieldValueGetters.LAST_NAME.get(record));
    }

    @Test
    public void testJMBAG() {
        StudentRecord record = new StudentRecord("0000000005", "Brezović", "Jusufadis", 5);
        assertEquals(record.getJmbag(), FieldValueGetters.JMBAG.get(record));
    }


}
