package hr.fer.oprpp1.hw04.db;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class QueryFilterTest {
    @Test
    public void testGeneral() {
        StudentRecord sr = new StudentRecord("0000000002", "B", "Ana", 5);
        QueryFilter qf = new QueryFilter(new ArrayList<>() {{
            add(new ConditionalExpression(FieldValueGetters.JMBAG, "0000000002", ComparisonOperators.EQUALS));
            add(new ConditionalExpression(FieldValueGetters.FIRST_NAME, "A", ComparisonOperators.GREATER));
        }});

        assertTrue(qf.accepts(sr));
    }
}
