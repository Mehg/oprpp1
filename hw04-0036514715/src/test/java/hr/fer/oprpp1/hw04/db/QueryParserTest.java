package hr.fer.oprpp1.hw04.db;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class QueryParserTest {
    @Test
    public void testParserDirect() {
        QueryParser parser = new QueryParser("jmbag=\"0000000003\"\n");
        assertTrue(parser.isDirectQuery());
        assertEquals("0000000003", parser.getQueriedJMBAG());
    }

    @Test
    public void testParserInvalidDirect() {
        assertThrows(IllegalStateException.class, () -> new QueryParser("jmbag=firstName\n"));
    }

    @Test
    public void testSpaces() {
        QueryParser parser = new QueryParser("query lastName = \"Blažić\"");
        assertFalse(parser.isDirectQuery());
        assertEquals(new ArrayList<ConditionalExpression>() {{
            add(new ConditionalExpression(FieldValueGetters.LAST_NAME, "Blažić", ComparisonOperators.EQUALS));
        }}, parser.getQuery());
    }

    @Test
    public void testComposite() {
        QueryParser parser = new QueryParser("query firstName>\"A\" and lastName LIKE \"B*ć\"");
        assertFalse(parser.isDirectQuery());
        assertEquals(new ArrayList<ConditionalExpression>() {{
            add(new ConditionalExpression(FieldValueGetters.FIRST_NAME, "A", ComparisonOperators.GREATER));
            add(new ConditionalExpression(FieldValueGetters.LAST_NAME, "B*ć", ComparisonOperators.LIKE));
        }}, parser.getQuery());
    }

    @Test
    public void testBigComposite() {
        QueryParser parser = new QueryParser("query firstName>\"A\" and firstName<\"C\" and lastName LIKE \"B*ć\" and jmbag>\"0000000002\"");
        assertFalse(parser.isDirectQuery());
        assertEquals(new ArrayList<ConditionalExpression>() {{
            add(new ConditionalExpression(FieldValueGetters.FIRST_NAME, "A", ComparisonOperators.GREATER));
            add(new ConditionalExpression(FieldValueGetters.FIRST_NAME, "C", ComparisonOperators.LESS));
            add(new ConditionalExpression(FieldValueGetters.LAST_NAME, "B*ć", ComparisonOperators.LIKE));
            add(new ConditionalExpression(FieldValueGetters.JMBAG, "0000000002", ComparisonOperators.GREATER));
        }}, parser.getQuery());
    }

    @Test
    public void testAlmostDirect() {
        QueryParser parser = new QueryParser("query jmbag=\"0000000002\" and firstName>\"A\"");
        assertFalse(parser.isDirectQuery());
        assertEquals(new ArrayList<ConditionalExpression>() {{
            add(new ConditionalExpression(FieldValueGetters.JMBAG, "0000000002", ComparisonOperators.EQUALS));
            add(new ConditionalExpression(FieldValueGetters.FIRST_NAME, "A", ComparisonOperators.GREATER));
        }}, parser.getQuery());
    }
}
