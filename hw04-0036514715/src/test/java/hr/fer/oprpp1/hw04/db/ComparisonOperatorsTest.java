package hr.fer.oprpp1.hw04.db;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ComparisonOperatorsTest {
    @Test
    public void testLESS() {
        IComparisonOperator oper = ComparisonOperators.LESS;
        assertTrue(oper.satisfied("Ana", "Jasna"));
        assertFalse(oper.satisfied("Ana", "An"));
        assertFalse(oper.satisfied("Ana", "Ana"));
    }

    @Test
    public void testLESSOREQUALS() {
        IComparisonOperator oper = ComparisonOperators.LESS_OR_EQUALS;
        assertTrue(oper.satisfied("Ana", "Jasna"));
        assertTrue(oper.satisfied("Ana", "Ana"));
        assertFalse(oper.satisfied("Ana", "An"));
    }

    @Test
    public void testGREATER() {
        IComparisonOperator oper = ComparisonOperators.GREATER;
        assertFalse(oper.satisfied("Ana", "Jasna"));
        assertFalse(oper.satisfied("Ana", "Ana"));
        assertTrue(oper.satisfied("Ana", "An"));
    }

    @Test
    public void testGREATEROREQUALS() {
        IComparisonOperator oper = ComparisonOperators.GREATER_OR_EQUALS;
        assertFalse(oper.satisfied("Ana", "Jasna"));
        assertTrue(oper.satisfied("Ana", "Ana"));
        assertTrue(oper.satisfied("Ana", "An"));
    }

    @Test
    public void testEQUALS() {
        IComparisonOperator oper = ComparisonOperators.EQUALS;
        assertFalse(oper.satisfied("Ana", "Jasna"));
        assertTrue(oper.satisfied("Ana", "Ana"));
        assertFalse(oper.satisfied("Ana", "An"));
    }

    @Test
    public void testNOTEQUALS() {
        IComparisonOperator oper = ComparisonOperators.NOT_EQUALS;
        assertTrue(oper.satisfied("Ana", "Jasna"));
        assertFalse(oper.satisfied("Ana", "Ana"));
        assertTrue(oper.satisfied("Ana", "An"));
    }

    @Test
    public void testLIKE() {
        IComparisonOperator oper = ComparisonOperators.LIKE;
        assertFalse(oper.satisfied("Zagreb", "Aba*"));
        assertFalse(oper.satisfied("AAA", "AA*AA"));
        assertTrue(oper.satisfied("AAAA", "AA*AA"));
    }
}
