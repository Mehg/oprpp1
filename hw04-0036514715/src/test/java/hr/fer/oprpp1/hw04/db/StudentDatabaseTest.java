package hr.fer.oprpp1.hw04.db;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class StudentDatabaseTest {

    @Test
    public void testConstructor() {
        String text = "0000000005\tBrezović\tJusufadis\t2";
        StudentDatabase sd = new StudentDatabase(new ArrayList<>() {{
            add(text);
        }});
        StudentRecord sr = new StudentRecord("0000000005", "Brezović", "Jusufadis", 2);
        assertEquals(sr, sd.forJMBAG("0000000005"));
    }

    @Test
    public void testConstructorJmbagTooLong() {
        String text = "00000000005\tBrezović\tJusufadis\t2";
        assertThrows(IllegalArgumentException.class, () -> new StudentDatabase(new ArrayList<>() {{
            add(text);
        }}));

    }

    @Test
    public void testConstructorJmbagAlreadyAdded() {
        String text = "00000000005\tBrezović\tJusufadis\t2";
        assertThrows(IllegalArgumentException.class, () -> new StudentDatabase(new ArrayList<>() {{
            add(text);
            add(text);
        }}));
    }

    @Test
    public void testConstructorTwoSurnames() {
        String text = "0000000015\tGlavinić Pecotić\tKristijan\t4";
        StudentDatabase sd = new StudentDatabase(new ArrayList<>() {{
            add(text);
        }});
        StudentRecord sr = new StudentRecord("0000000015", "Glavinić Pecotić", "Kristijan", 4);
        assertEquals(sr, sd.forJMBAG("0000000015"));
    }

    @Test
    public void testConstructorGradeWrong() {
        String text = "0000000005\tBrezović\tJusufadis\t6";
        assertThrows(IllegalArgumentException.class, () -> new StudentDatabase(new ArrayList<>() {{
            add(text);
        }}));
    }

    @Test
    public void testGettingNonJMBAG() {
        String text = "0000000005\tBrezović\tJusufadis\t5";
        StudentDatabase sd = new StudentDatabase(new ArrayList<>() {{
            add(text);
        }});
        assertNull(sd.forJMBAG("0000003"));
    }

    @Test
    public void testGettingJMBAG() {
        String text = "0000000005\tBrezović\tJusufadis\t5";
        StudentDatabase sd = new StudentDatabase(new ArrayList<>() {{
            add(text);
        }});
        StudentRecord sr = new StudentRecord("0000000005", "Brezović", "Jusufadis", 5);
        assertEquals(sr, sd.forJMBAG("0000000005"));
    }

    @Test
    public void testfilterAllTrue() {
        String text = "0000000005\tBrezović\tJusufadis\t5";
        String text1 = "0000000006\tBrezović\tJusufadis\t5";
        String text2 = "0000000007\tBrezović\tJusufadis\t5";
        List<StudentRecord> all = new ArrayList<>() {{
            add(new StudentRecord("0000000005", "Brezović", "Jusufadis", 5));
            add(new StudentRecord("0000000006", "Brezović", "Jusufadis", 5));
            add(new StudentRecord("0000000007", "Brezović", "Jusufadis", 5));
        }};

        StudentDatabase sd = new StudentDatabase(new ArrayList<>() {{
            add(text);
            add(text1);
            add(text2);
        }});

        assertEquals(all, sd.filter(record -> true));
    }

    @Test
    public void testfilterAllFalse() {
        String text = "0000000005\tBrezović\tJusufadis\t5";
        String text1 = "0000000006\tBrezović\tJusufadis\t5";
        String text2 = "0000000007\tBrezović\tJusufadis\t5";
        List<StudentRecord> all = new ArrayList<>();

        StudentDatabase sd = new StudentDatabase(new ArrayList<>() {{
            add(text);
            add(text1);
            add(text2);
        }});

        assertEquals(all, sd.filter(record -> false));
    }
}
