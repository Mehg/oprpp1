package hr.fer.zemris.lsystems.impl;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LSystemBuilderImplTest {
    @Test
    public void testFromPDFForGenerating() {
        LSystemBuilderImpl test = new LSystemBuilderImpl();
        test.setAxiom("F");
        test.registerProduction('F', "F+F--F+F");

        assertEquals("F", test.build().generate(0));
        assertEquals("F+F--F+F", test.build().generate(1));
        assertEquals("F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F", test.build().generate(2));
    }
}
