package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * Models a simple BarChart
 */
public class BarChart {
    /**
     * List of XYValues for graphing the bars
     */
    private final List<XYValue> values;
    /**
     * x axis label
     */
    private final String xAxis;
    /**
     * y axis label
     */
    private final String yAxis;
    /**
     * the maximum value of y axis
     */
    private final int yMax;
    /**
     * the minimum value of y axis
     */
    private final int yMin;
    /**
     * the step of y axis
     */
    private int yStep;

    /**
     * Creates a new BarChart from given parameters
     *
     * @param values List of XYValues for graphing the bars
     * @param xAxis  x axis label
     * @param yAxis  y axis label
     * @param yMax   the maximum value of y axis
     * @param yMin   the minimum value of y axis
     * @param yStep  the step of y axis
     * @throws NullPointerException     if any of the arguments are null
     * @throws IllegalArgumentException if yMin is less than 0, if yMax is less than yMin
     *                                  or if some values were not defined properly
     */
    public BarChart(List<XYValue> values, String xAxis, String yAxis, int yMax, int yMin, int yStep) {
        if (values == null || xAxis == null || yAxis == null)
            throw new NullPointerException("Arguments must not be null");
        if (yMin < 0)
            throw new IllegalArgumentException("Minimum y must be greater or equal to 0");
        if (yMax < yMin)
            throw new IllegalArgumentException("Maximum y must be greater or equal to minimum y");
        for (XYValue value : values)
            if (value.getY() < yMin)
                throw new IllegalArgumentException("Y value of some value in given list is less than minimum y");
        this.values = values;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.yMax = yMax;
        this.yMin = yMin;
        this.yStep = yStep;

        while ((yMax - yMin) % this.yStep != 0) {
            this.yStep ++;
        }
    }

    /**
     * Returns the list of XYValues
     *
     * @return the list of XYValues
     */
    public List<XYValue> getValues() {
        return values;
    }

    /**
     * Returns the label of the x axis
     *
     * @return the label of the x axis
     */
    public String getXAxis() {
        return xAxis;
    }

    /**
     * Returns the label of the y axis
     *
     * @return the label of the y axis
     */
    public String getYAxis() {
        return yAxis;
    }

    /**
     * Returns the maximum value of the y axis
     *
     * @return the maximum value of the y axis
     */
    public int getYMax() {
        return yMax;
    }

    /**
     * Returns the minimum value of the y axis
     *
     * @return the minimum value of the y axis
     */
    public int getYMin() {
        return yMin;
    }

    /**
     * Returns the step of the y axis
     *
     * @return the step of the y axis
     */
    public int getYStep() {
        return yStep;
    }
}
