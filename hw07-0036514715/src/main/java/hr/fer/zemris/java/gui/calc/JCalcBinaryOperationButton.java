package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;

import java.util.function.DoubleBinaryOperator;

/**
 * Models all binary operation buttons of Calculator
 */
public class JCalcBinaryOperationButton extends JCalcButton {
    /**
     * Creates a new JCalcBinaryOperationButton with given name and default ActionListener from the given operator
     * The default ActionListener applies the operation if there is any pending and queues the next operation
     *
     * @param calcModel given CalcModel needed for default Action Listener
     * @param name      given name
     * @param operator  given operator
     */
    public JCalcBinaryOperationButton(CalcModel calcModel, String name, DoubleBinaryOperator operator) {
        super(name);
        this.addActionListener(c -> {
            if (calcModel.hasFrozenValue())
                throw new CalculatorInputException("Already has frozen value");
            if (calcModel.isActiveOperandSet())
                calcModel.setValue(
                        calcModel.getPendingBinaryOperation().applyAsDouble(
                                calcModel.getActiveOperand(),
                                calcModel.getValue())
                );
            calcModel.freezeValue(calcModel.toString());
            calcModel.setActiveOperand(calcModel.getValue());
            calcModel.setPendingBinaryOperation(operator);
            calcModel.clear();
        });
    }
}
