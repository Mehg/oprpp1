package hr.fer.zemris.java.gui.charts;

/**
 * Class which models a point with an x and y value
 */
public class XYValue {
    /**
     * Internal x value
     */
    private final int x;
    /**
     * Internal y value
     */
    private final int y;

    /**
     * Creates a new XYValue from given parameters
     *
     * @param x given x value
     * @param y given y value
     */
    public XYValue(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns the x component of current XYValue
     *
     * @return the x component of current XYValue
     */
    public int getX() {
        return x;
    }

    /**
     * Returns the y component of current XYValue
     *
     * @return the y component of current XYValue
     */
    public int getY() {
        return y;
    }
}
