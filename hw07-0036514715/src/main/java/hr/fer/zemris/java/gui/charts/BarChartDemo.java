package hr.fer.zemris.java.gui.charts;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used for constructing a BarChartComponent from given file
 */
public class BarChartDemo extends JFrame {
    /**
     * Internal BarChart
     */
    private final BarChart barChart;
    /**
     * Path provided by the user
     */
    private final String path;

    /**
     * Method which creates a new BarChartDemo from given parameters
     *
     * @param barChart given BarChart
     * @param path     given Path
     */
    public BarChartDemo(BarChart barChart, String path) {
        super("BarChart");
        this.barChart = barChart;
        this.path = path;

        setPreferredSize(new Dimension(600, 600));
        setLocation(20, 50);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        initGUI();
    }

    /**
     * Method used for initializing the GUI
     */
    public void initGUI() {
        Container cp = getContentPane();

        cp.setLayout(new BorderLayout(5, 5));
        JLabel path = new JLabel(this.path);
        path.setHorizontalAlignment(SwingConstants.CENTER);
        cp.add(path, BorderLayout.NORTH);

        BarChartComponent barChart = new BarChartComponent(this.barChart);
        cp.add(barChart, BorderLayout.CENTER);

    }

    /**
     * Main method used for extracting information from user input and creating a new BarChartDemo
     *
     * @param args user-provided path
     * @throws IllegalArgumentException if path is missing
     */
    public static void main(String[] args) {
        String xAxis = "";
        String yAxis = "";
        List<XYValue> values = new ArrayList<>();
        int yMin = 0;
        int yMax = 0;
        int yStep = 0;

        if (args.length == 0) {
            throw new IllegalArgumentException("Path is missing");
        }
        String path = args[0];

        try {
            List<String> lines = Files.readAllLines(Path.of(path));
            xAxis = lines.get(0);
            yAxis = lines.get(1);
            String[] points = lines.get(2).split(" ");

            for (String s : points)
                values.add(new XYValue(Integer.parseInt(s.split(",")[0]),
                        Integer.parseInt(s.split(",")[1])));

            yMin = Integer.parseInt(lines.get(3));
            yMax = Integer.parseInt(lines.get(4));
            yStep = Integer.parseInt(lines.get(5));

        } catch (IOException e) {
            System.err.println("Something went wrong while reading the file");
            System.exit(1);
        }

        BarChart barChart = new BarChart(values, xAxis, yAxis, yMax, yMin, yStep);
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new BarChartDemo(barChart, path);
            frame.pack();
            frame.setVisible(true);
        });

    }
}
