package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

/**
 * Simple implementation of a Calculator
 */
public class Calculator extends JFrame {
    /**
     * CalcModel used
     */
    private final CalcModel calcModel;
    /**
     * Stack of the calculator
     */
    private final Stack<Double> stack;

    /**
     * Creates a new Calculator with given CalcModel set as its model
     *
     * @param calcModel given CalcModel
     */
    public Calculator(CalcModel calcModel) {
        this.calcModel = calcModel;
        this.stack = new Stack<>();

        setPreferredSize(new Dimension(500, 400));
        setLocation(20, 50);
        setTitle("Java Calculator v1.0");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        initGUI();
    }

    /**
     * Initializes the GUI
     */
    public void initGUI() {
        Container cp = getContentPane();
        cp.setLayout(new CalcLayout(5));

        addDisplay(cp);
        addEquals(cp);
        addGenericButtons(cp);
        addInvOperations(cp);
        addNumbers(cp);
        addBinaryOperations(cp);
        addStackButtons(cp);
    }

    /**
     * Adds the display to the given container
     *
     * @param cp given container
     */
    private void addDisplay(Container cp) {
        JCalcDisplay display = new JCalcDisplay(calcModel);
        cp.add(display, "1,1");
    }


    /**
     * Adds the equals button to the given container
     *
     * @param cp given container
     */
    private void addEquals(Container cp) {
        JCalcEqualsButton equals = new JCalcEqualsButton(calcModel);
        cp.add(equals, "1,6");
    }


    /**
     * Adds the clear, reset, sign and dot button to the given container
     *
     * @param cp given container
     */
    private void addGenericButtons(Container cp) {
        JCalcButton clear = new JCalcButton("clr", c -> calcModel.clear());
        cp.add(clear, "1,7");

        JCalcButton reset = new JCalcButton("res", c -> {
            calcModel.clearAll();
            calcModel.freezeValue(null);
        });
        cp.add(reset, "2,7");

        JCalcButton sign = new JCalcButton("+/-", c -> {
            if (!calcModel.isEditable())
                calcModel.clear();
            calcModel.swapSign();
        });
        cp.add(sign, " 5,4");

        JCalcButton dot = new JCalcButton(".", c -> {
            if (!calcModel.isEditable())
                calcModel.clear();
            calcModel.insertDecimalPoint();
        }
        );
        cp.add(dot, "5,5");
    }


    /**
     * Adds the number buttons to the given container
     *
     * @param cp given container
     */
    private void addNumbers(Container cp) {
        for (int i = 0; i < 10; i++) {
            JCalcNumberButton number = new JCalcNumberButton(calcModel, i);
            cp.add(number, number.getRCPosition());
        }
    }

    /**
     * Adds the stack buttons to the given container
     *
     * @param cp given container
     */
    private void addStackButtons(Container cp) {
        JCalcButton push = new JCalcButton("push", c -> stack.push(Double.parseDouble(calcModel.toString())));
        JCalcButton pop = new JCalcButton("pop", c -> {
            if (!stack.isEmpty()) {
                Double value = stack.pop();
                calcModel.setValue(value);
                calcModel.freezeValue(null);
            }
        });

        cp.add(push, "3,7");
        cp.add(pop, "4,7");
    }


    /**
     * Adds the binary operations buttons to the given container
     *
     * @param cp given container
     */
    private void addBinaryOperations(Container cp) {
        Map<String, DoubleBinaryOperator> binaryOperatorMap = new LinkedHashMap<>();
        binaryOperatorMap.put("/", (f, s) -> f / s);
        binaryOperatorMap.put("*", (f, s) -> f * s);
        binaryOperatorMap.put("-", (f, s) -> f - s);
        binaryOperatorMap.put("+", Double::sum);

        int i = 2;
        for (String key : binaryOperatorMap.keySet()) {
            JCalcBinaryOperationButton operation = new JCalcBinaryOperationButton(calcModel, key, binaryOperatorMap.get(key));
            cp.add(operation, new RCPosition(i++, 6));
        }

    }


    /**
     * Adds the inverted operations buttons and the inv button to the given container
     *
     * @param cp given container
     */
    private void addInvOperations(Container cp) {
        JCheckBox inv = new JCheckBox("Inv");
        inv.setOpaque(true);

        JCalcInvertibleButton[] invertibleButtons = loadOperations();
        for (int i = 0, j; i < invertibleButtons.length; i++) {
            if (i % 2 == 0) j = 0;
            else j = 1;
            cp.add(invertibleButtons[i], new RCPosition(i / 2 + 2, j + 1));
            int finalI = i;
            inv.addActionListener(l -> {
                invertibleButtons[finalI].setInverted(inv.isSelected());
                invertibleButtons[finalI].setText(invertibleButtons[finalI].getText());
            });
        }

        cp.add(inv, "5, 7");

    }


    /**
     * Private helper method used for loading invertible operations
     *
     * @return JCalcInvertibleButton array of invertible buttons of predefined operations
     */
    private JCalcInvertibleButton[] loadOperations() {
        Map<String, DoubleUnaryOperator[]> operations = new LinkedHashMap<>();
        operations.put("1/x : 1/x", new DoubleUnaryOperator[]{
                (f) -> 1 / f, (f) -> 1 / f
        });
        operations.put("sin : arcsin", new DoubleUnaryOperator[]{
                Math::sin, Math::asin
        });
        operations.put("log : 10^x", new DoubleUnaryOperator[]{
                Math::log10, f -> Math.pow(10, f),
        });
        operations.put("cos : arccos", new DoubleUnaryOperator[]{
                Math::cos, Math::acos
        });

        operations.put("ln : e^x", new DoubleUnaryOperator[]{
                Math::log, Math::exp
        });
        operations.put("tan : arctan", new DoubleUnaryOperator[]{
                Math::tan, Math::atan
        });
        operations.put("x^n : x^(1/n)", new DoubleUnaryOperator[]{});
        operations.put("ctg : arcctg", new DoubleUnaryOperator[]{
                f -> 1 / Math.tan(f), f -> 1 / Math.atan(f)
        });

        ArrayList<JCalcInvertibleButton> buttons = new ArrayList<>();

        for (Map.Entry<String, DoubleUnaryOperator[]> entry : operations.entrySet()) {
            String key = entry.getKey();
            DoubleUnaryOperator[] operators = entry.getValue();
            JCalcInvertibleButton button;
            if (key.equals("x^n : x^(1/n)")) {
                button = new JCalcInvertibleButton("x^n", c -> {
                    if (calcModel.hasFrozenValue())
                        throw new CalculatorInputException("Frozen value already set");
                    if (calcModel.isActiveOperandSet())
                        calcModel.setValue(
                                calcModel.getPendingBinaryOperation().applyAsDouble(
                                        calcModel.getActiveOperand(),
                                        calcModel.getValue())
                        );
                    calcModel.freezeValue(calcModel.toString());
                    calcModel.setActiveOperand(calcModel.getValue());
                    calcModel.setPendingBinaryOperation(Math::pow);
                    calcModel.clear();
                },
                        "x^(1/n)", c -> {
                    if (calcModel.hasFrozenValue())
                        throw new CalculatorInputException("Frozen value already set");
                    if (calcModel.isActiveOperandSet())
                        calcModel.setValue(
                                calcModel.getPendingBinaryOperation().applyAsDouble(
                                        calcModel.getActiveOperand(),
                                        calcModel.getValue())
                        );
                    calcModel.freezeValue(calcModel.toString());
                    calcModel.setActiveOperand(calcModel.getValue());
                    calcModel.setPendingBinaryOperation((f, s) -> Math.pow(f, 1.0 / s));
                    calcModel.clear();
                });
            } else {
                button = new JCalcInvertibleButton(key.split(" : ")[0], operators[0],
                        key.split(" : ")[1], operators[1], calcModel);
            }

            buttons.add(button);

        }

        return buttons.toArray(JCalcInvertibleButton[]::new);
    }

    /**
     * Main method used for initializing and running the Calculator
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new Calculator(new CalcModelImpl());
            frame.pack();
            frame.setVisible(true);
        });
    }

}
