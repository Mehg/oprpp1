package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * Models all number buttons of Calculator
 */
public class JCalcNumberButton extends JCalcButton {
    /**
     * Current number
     */
    private final int number;

    /**
     * Creates a new JCalcNumberButton from given number and adds default ActionListener
     * The default ActionListener adds the number to the current value of the Calculator
     *
     * @param calcModel given CalcModel needed for the default ActionListener
     * @param number    given number
     */
    public JCalcNumberButton(CalcModel calcModel, int number) {
        super(String.valueOf(number));
        this.number = number;
        this.addActionListener(c -> calcModel.insertDigit(number));

        this.setFont(this.getFont().deriveFont(30f));
    }

    /**
     * Method which returns the RCPosition of a JCalcNumberButton
     *
     * @return RCPosition for this JCalcNumberButton
     */
    public RCPosition getRCPosition() {
        if (number == 0) {
            return new RCPosition(5, 3);
        }
        int row = 4 - (number - 1) / 3;
        int col = (number - 1) % 3 + 3;

        return new RCPosition(row, col);
    }

}
