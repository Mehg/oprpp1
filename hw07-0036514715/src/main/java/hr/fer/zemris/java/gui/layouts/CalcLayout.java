package hr.fer.zemris.java.gui.layouts;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Implementation of LayoutManager2 for Calculator
 */
public class CalcLayout implements LayoutManager2 {
    /**
     * Internal map of elements
     */
    public final Map<RCPosition, Component> elements;
    /**
     * Padding
     */
    private final int padding;
    /**
     * Number of rows
     */
    private static final int NUMBER_OF_ROWS = 5;
    /**
     * Number of columns
     */
    private static final int NUMBER_OF_COLUMNS = 7;

    /**
     * Constructor which creates a new CalcLayout with given padding
     *
     * @param padding given padding
     */
    public CalcLayout(int padding) {
        if (padding < 0) throw new IllegalArgumentException("Padding must be greater or equal to 0");
        this.padding = padding;
        this.elements = new HashMap<>();

    }

    /**
     * Constructor which creates a new CalcLayout with padding set to 0
     */
    public CalcLayout() {
        this(0);
    }

    @Override
    public void addLayoutComponent(Component comp, Object constraints) {
        if (comp == null || constraints == null) throw new NullPointerException("Arguments must not be null");
        if (!(constraints instanceof String) && !(constraints instanceof RCPosition))
            throw new IllegalArgumentException("Constraint must either be String or RCPosition");

        RCPosition position;
        if (constraints instanceof String)
            position = RCPosition.parse((String) constraints);
        else
            position = (RCPosition) constraints;

        checkPosition(position);

        elements.put(position, comp);

    }

    /**
     * Private helper method used for checking if given position is valid
     *
     * @param position given position
     * @throws CalcLayoutException if given position is not valid
     */
    private void checkPosition(RCPosition position) {
        if (position.getRow() < 1 || position.getRow() > 5 || position.getColumn() < 1 || position.getColumn() > 7)
            throw new CalcLayoutException("Invalid position - row must be [1,5], column must be [1,7]; was:" + position);
        else if (position.getRow() == 1 && position.getColumn() > 1 && position.getColumn() < 6)
            throw new CalcLayoutException("Invalid position - position is reserved for element 1,1");
        else if (elements.containsKey(position))
            throw new CalcLayoutException("Invalid position - already defined");
    }

    @Override
    public float getLayoutAlignmentX(Container target) {
        return 0;
    }

    @Override
    public float getLayoutAlignmentY(Container target) {
        return 0;
    }

    @Override
    public void invalidateLayout(Container target) {
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
        throw new UnsupportedOperationException("Unsupported operation: addLayoutComponent");
    }

    @Override
    public void removeLayoutComponent(Component comp) {
        elements.values().remove(comp);
    }

    @Override
    public Dimension maximumLayoutSize(Container target) {
        return layoutSize(target, Component::getMaximumSize);
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        return layoutSize(parent, Component::getPreferredSize);
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        return layoutSize(parent, Component::getMinimumSize);
    }

    /**
     * Private helper method used for minimumLayoutSize, preferredLayoutSize and maximumLayoutSize
     * Returns the maximum of whichever method called it
     *
     * @param container given container
     * @param function  given function
     * @return Dimension
     */
    private Dimension layoutSize(Container container, Function<Component, Dimension> function) {
        int height = 0;
        int heightOne;
        int width = 0;
        int widthOne;

        for (RCPosition position : elements.keySet()) {
            Component component = elements.get(position);
            Dimension dimension = function.apply(component);
            if (dimension != null) {
                widthOne = dimension.width;
                heightOne = dimension.height;
                if (position.equals(new RCPosition(1, 1)))
                    widthOne = (widthOne - 4 * padding) / 5;
                if (widthOne > width) width = widthOne;
                if (heightOne > height) height = heightOne;
            }
        }

        height = height != 0 ? height : 10;
        width = width != 0 ? width : 10;

        height = height * NUMBER_OF_ROWS + (NUMBER_OF_ROWS - 1) * padding;
        width = width * NUMBER_OF_COLUMNS + (NUMBER_OF_COLUMNS - 1) * padding;

        Insets insets = container.getInsets();

        height = height + insets.bottom + insets.top;
        width = width + insets.left + insets.right;

        return new Dimension(width, height);
    }

    @Override
    public void layoutContainer(Container parent) {
        Insets insets = parent.getInsets();
        int height = parent.getHeight() - insets.top - insets.bottom - (NUMBER_OF_ROWS - 1) * padding;
        int width = parent.getWidth() - insets.right - insets.left - (NUMBER_OF_COLUMNS - 1) * padding;

        int elementHeight = height / NUMBER_OF_ROWS;
        int elementWidth = width / NUMBER_OF_COLUMNS;

        int surplusHeight = height % NUMBER_OF_ROWS;
        int surplusWidth = height % NUMBER_OF_COLUMNS;

        ArrayList<Integer> heights = calculateAllDimension(elementHeight, surplusHeight, NUMBER_OF_ROWS);
        ArrayList<Integer> widths = calculateAllDimension(elementWidth, surplusWidth, NUMBER_OF_COLUMNS);

        for (Map.Entry<RCPosition, Component> entry : elements.entrySet()) {
            RCPosition position = entry.getKey();
            int currentHeight = heights.get(position.getRow() - 1), currentWidth = widths.get(position.getColumn() - 1);

            int x = insets.left;
            int y = insets.top;

            for (int i = 0; i < position.getColumn() - 1; i++)
                x = x + widths.get(i) + padding;

            for (int i = 0; i < position.getRow() - 1; i++)
                y = y + heights.get(i) + padding;

            if (position.equals(new RCPosition(1, 1))) {
                int width11 = 0;
                for (int i = 0; i < 5; i++)
                    width11 = width11 + widths.get(i) + padding;
                entry.getValue().setBounds(x, y, width11 - padding, currentHeight);
            } else
                entry.getValue().setBounds(x, y, currentWidth, currentHeight);

        }
    }

    /**
     * Private helper method used for calculating all heights or widths of elements
     *
     * @param elementDimension given elementHeight without surplus
     * @param surplusDimension given surplusHeight
     * @param max              maximum of that dimension
     * @return ArrayList of heights
     */
    private ArrayList<Integer> calculateAllDimension(int elementDimension, int surplusDimension, int max) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < max; i++) {
            list.add(elementDimension + addSurplus(i, surplusDimension, max));
        }
        return list;
    }

    /**
     * Private helper method used for distributing the surpluses
     *
     * @param position given position
     * @param surplus  given surplus
     * @param size     given size
     * @return the surplus that should be applied (0 or 1)
     */
    private int addSurplus(int position, int surplus, int size) {
        switch (surplus) {
            case 1 -> {
                if (position == Math.ceil(size / 2.0))
                    return 1;
                return 0;
            }
            case 2 -> {
                if (position == size - 1 || position == 2)
                    return 1;
                return 0;
            }
            case 3 -> {
                int helper = size / 3;
                if (position == helper || position == helper + 2 || position == helper + 4)
                    return 1;
                return 0;
            }
            case 4 -> {
                if (position == 1 || position == size || position == size / 2 || position == (size / 2 + 2))
                    return 1;
                return 0;
            }
            case 5 -> {
                if (position == 1 || position == 3 || position == 4 || position == 5 || position == 7)
                    return 1;
                return 0;
            }
            case 6 -> {
                if (position == 1 || position == 2 || position == 3 || position == 5 || position == 6 || position == 7)
                    return 1;
                return 0;
            }
        }
        return 0;
    }

}