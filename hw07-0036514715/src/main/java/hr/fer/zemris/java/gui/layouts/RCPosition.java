package hr.fer.zemris.java.gui.layouts;

import java.util.Objects;

/**
 * Class used for implementing a row-column type of position
 */
public class RCPosition {
    /**
     * internal row
     */
    private final int row;
    /**
     * internal column
     */
    private final int column;

    /**
     * Creates a new RCPosition from given parameters
     *
     * @param row    given row
     * @param column given column
     */
    public RCPosition(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * Return the internal row
     *
     * @return the internal row
     */
    public int getRow() {
        return row;
    }

    /**
     * Returns the internal column
     *
     * @return the internal column
     */
    public int getColumn() {
        return column;
    }


    /**
     * Static method which return the RCPosition equivalent for given text
     *
     * @param text given text
     * @return the RCPosition equivalent of given text
     * @throws IllegalArgumentException if given string is not parsable
     */
    public static RCPosition parse(String text) {
        if (!text.matches("\\s*-?\\d+\\s*,\\s*-?\\d+\\s*"))
            throw new IllegalArgumentException("Invalid string");

        String[] parts = text.split(",");
        int row = Integer.parseInt(parts[0].trim());
        int column = Integer.parseInt(parts[1].trim());
        return new RCPosition(row, column);
    }


    @Override
    public String toString() {
        return "row=" + row +
                ", column=" + column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RCPosition that = (RCPosition) o;
        return row == that.row &&
                column == that.column;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, column);
    }


}
