package hr.fer.zemris.java.gui.prim;

import javax.swing.*;
import java.awt.*;


/**
 * JFrame with two lists which show the next prime button when clicked on button next
 */
public class PrimDemo extends JFrame {
    /**
     * PrimListModel used
     */
    private final PrimListModel model;

    /**
     * Creates a new PrimDemo
     */
    public PrimDemo() {
        super("PrimDemo");
        setPreferredSize(new Dimension(205, 215));
        setLocation(20, 50);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        model = new PrimListModel();

        initGUI();
    }

    /**
     * Initializes GUI
     */
    private void initGUI() {
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout(5, 5));

        JPanel panel = new JPanel(new GridLayout(0, 2));
        ScrollPane sp1 = new ScrollPane();
        JList<Integer> list1 = new JList<>();
        list1.setModel(model);
        sp1.add(list1);
        panel.add(sp1);

        ScrollPane sp2 = new ScrollPane();
        JList<Integer> list2 = new JList<>();
        list2.setModel(model);
        sp2.add(list2);
        panel.add(sp2);

        cp.add(panel, BorderLayout.CENTER);
        JButton next = new JButton("sljedeći");
        next.addActionListener(l -> model.next());
        cp.add(next, BorderLayout.PAGE_END);

    }

    /**
     * Main method
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new PrimDemo();
            frame.pack();
            frame.setVisible(true);
        });
    }
}
