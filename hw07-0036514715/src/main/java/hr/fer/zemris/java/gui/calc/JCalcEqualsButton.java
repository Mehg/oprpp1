package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

/**
 * Class which represents the equals button of Calculator
 */
public class JCalcEqualsButton extends JCalcButton {

    /**
     * Creates a new JCalcEqualsButton
     * By default on click it either evaluates the expression or shows the number entered
     *
     * @param calcModel given CalcModel
     */
    public JCalcEqualsButton(CalcModel calcModel) {
        super("=");
        this.addActionListener(c -> {
            double value = calcModel.getValue();
            if (calcModel.isActiveOperandSet()) {
                value = calcModel.getPendingBinaryOperation().applyAsDouble(
                        calcModel.getActiveOperand(),
                        calcModel.getValue());
            }

            calcModel.setValue(value);
            calcModel.freezeValue(String.valueOf(value));
            calcModel.clearAll();
        });
    }

}
