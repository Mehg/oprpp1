package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalcValueListener;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleBinaryOperator;

/**
 * Simple implementation of a CalcModel
 */
public class CalcModelImpl implements CalcModel {
    /**
     * flag for whether the model is editable
     */
    private boolean isEditable;
    /**
     * flag for whether the current value is positive
     */
    private boolean isPositive;
    /**
     * String value of entered numbers
     */
    private String enteredNumbers;
    /**
     * value of entered numbers
     */
    private double value;
    /**
     * frozen value
     */
    private String frozenValue;
    /**
     * active operand
     */
    private double activeOperand;
    /**
     * whether the active operand is set
     */
    private boolean isActiveOperandSet;
    /**
     * pending operation if such exists
     */
    private DoubleBinaryOperator pendingOperation;
    /**
     * internal list of listeners
     */
    private final List<CalcValueListener> listeners;

    /**
     * Creates a new CalcModelImpl
     */
    public CalcModelImpl() {
        isEditable = true;
        isPositive = true;
        enteredNumbers = "";
        value = 0;
        frozenValue = null;
        isActiveOperandSet = false;
        pendingOperation = null;
        listeners = new ArrayList<>();
    }

    @Override
    public void addCalcValueListener(CalcValueListener l) {
        listeners.add(l);
    }

    @Override
    public void removeCalcValueListener(CalcValueListener l) {
        listeners.remove(l);
    }

    @Override
    public double getValue() {
        return isPositive ? value : -value;
    }

    @Override
    public void setValue(double value) {
        this.value = value < 0 ? -value : value;
        this.isPositive = value >= 0;
        this.isEditable = false;
        this.enteredNumbers = String.valueOf(this.value);
        updateListeners();
    }

    @Override
    public boolean isEditable() {
        return isEditable;
    }

    @Override
    public void clear() {
        isEditable = true;
        isPositive = true;
        enteredNumbers = "";
        value = 0;
        updateListeners();
    }

    @Override
    public void clearAll() {
        clear();
        isActiveOperandSet = false;
        pendingOperation = null;
        updateListeners();
    }

    @Override
    public void swapSign() throws CalculatorInputException {
        if (!isEditable) throw new CalculatorInputException("Calculator is not editable");
        if (hasFrozenValue()) frozenValue = null;
        isPositive = !isPositive;
        updateListeners();

    }

    @Override
    public void insertDecimalPoint() throws CalculatorInputException {
        if (!isEditable) throw new CalculatorInputException("Calculator is not editable");
        if (hasFrozenValue()) frozenValue = null;

        if (enteredNumbers.contains("."))
            throw new CalculatorInputException("Already contains decimal point");
        if (enteredNumbers.equals(""))
            throw new CalculatorInputException("No number entered");

        enteredNumbers += ".";
        updateListeners();
    }

    @Override
    public void insertDigit(int digit) throws CalculatorInputException, IllegalArgumentException {
        if (!isEditable) throw new CalculatorInputException("Calculator is not editable");
        if (digit < 0 || digit > 9) throw new IllegalArgumentException("Digit must be between 0 and 9");
        if (hasFrozenValue()) frozenValue = null;

        String newEnteredNumbers = enteredNumbers + digit;

        newEnteredNumbers = newEnteredNumbers.replaceAll("^0+([0-9])", "$1");

        try {
            double newEnteredNumbersValue = Double.parseDouble(newEnteredNumbers);
            if (Double.isInfinite(newEnteredNumbersValue) || Double.isNaN(newEnteredNumbersValue))
                throw new CalculatorInputException("Number is too big");

            enteredNumbers = newEnteredNumbers;
            value = newEnteredNumbersValue;
        } catch (NumberFormatException e) {
            throw new CalculatorInputException("Number is not parsable double");
        }
        updateListeners();
    }

    @Override
    public boolean isActiveOperandSet() {
        return isActiveOperandSet;
    }

    @Override
    public double getActiveOperand() throws IllegalStateException {
        if (!isActiveOperandSet())
            throw new IllegalStateException();
        return activeOperand;
    }

    @Override
    public void setActiveOperand(double activeOperand) {
        isActiveOperandSet = true;
        this.activeOperand = activeOperand;
        updateListeners();
    }

    @Override
    public void clearActiveOperand() {
        isActiveOperandSet = false;
        updateListeners();
    }

    @Override
    public DoubleBinaryOperator getPendingBinaryOperation() {
        return pendingOperation;
    }

    @Override
    public void setPendingBinaryOperation(DoubleBinaryOperator op) {
        this.pendingOperation = op;
        updateListeners();
    }

    @Override
    public void freezeValue(String value) {
        frozenValue = value;
        updateListeners();
    }

    @Override
    public boolean hasFrozenValue() {
        return frozenValue != null;
    }

    private void updateListeners() {
        for (CalcValueListener listener : listeners)
            listener.valueChanged(this);
    }

    @Override
    public String toString() {
        if (hasFrozenValue())
            return frozenValue;
        if (enteredNumbers.equals(""))
            return isPositive ? "0" : "-0";
        if (Double.isInfinite(value))
            return isPositive ? "Infinity" : "-Infinity";
        if (Double.isNaN(value))
            return "NaN";
        return isPositive ? enteredNumbers : "-" + enteredNumbers;

    }
}
