package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.model.CalcModel;

import javax.swing.*;
import java.awt.*;

/**
 * Class which represents a display that displays values in the Calculator
 */
public class JCalcDisplay extends JLabel {

    /**
     * Creates a new JCalcDisplay and adds default ActionListener
     * The default ActionListener shows the String value of the given CalcModel
     *
     * @param calcModel given CalcModel
     */
    public JCalcDisplay(CalcModel calcModel) {
        this.setFont(this.getFont().deriveFont(30f));
        this.setBackground(Color.YELLOW);
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.setOpaque(true);
        this.setHorizontalAlignment(SwingConstants.RIGHT);
        calcModel.addCalcValueListener(c -> this.setText(c.toString()));
    }
}
