package hr.fer.zemris.java.gui.prim;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.ArrayList;

/**
 * Implementation of a ListModel which is used for generating prime numbers
 */
public class PrimListModel implements ListModel<Integer> {
    /**
     * Internal list of prime numbers
     */
    ArrayList<Integer> primes;
    /**
     * Internal list of listeners
     */
    ArrayList<ListDataListener> listeners;

    /**
     * Creates a new PrimListModel and adds the first prime - 1
     */
    public PrimListModel() {
        this.primes = new ArrayList<>();
        this.listeners = new ArrayList<>();
        primes.add(1);
    }

    /**
     * Finds next prime and adds it to the internal list of primes
     */
    public void next() {
        int next = primes.get(getSize() - 1) + 1;

        while (!checkIfPrime(next)) {
            next++;
        }

        primes.add(next);

        for (ListDataListener l : listeners)
            l.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED,
                    getSize() - 1, getSize() - 1));
    }

    /**
     * For given number checks if it is a prime
     *
     * @param number given number
     * @return true if number is prime, otherwise false
     */
    private boolean checkIfPrime(int number) {
        boolean isNotPrime = false;
        for (int i = 2; i <= number / 2; ++i) {

            if (number % i == 0) {
                isNotPrime = true;
                break;
            }
        }
        return !isNotPrime;

    }

    @Override
    public int getSize() {
        return primes.size();
    }

    @Override
    public Integer getElementAt(int index) {
        return primes.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        listeners.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        listeners.remove(l);
    }
}
