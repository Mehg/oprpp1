package hr.fer.zemris.java.gui.layouts;

/**
 * Exception used in CalcLayout
 */
public class CalcLayoutException extends RuntimeException {
    /**
     * Default constructor with given message
     *
     * @param message given message
     */
    public CalcLayoutException(String message) {
        super(message);
    }
}
