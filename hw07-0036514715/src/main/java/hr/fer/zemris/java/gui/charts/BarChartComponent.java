package hr.fer.zemris.java.gui.charts;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;

/**
 * Models a simple BarChart JComponent
 */
public class BarChartComponent extends JComponent {
    /**
     * The internal barchart
     */
    private final BarChart barChart;
    /**
     * Padding from all sides to actual content
     */
    private final int PADDING = 20;
    /**
     * Offset between the y label and the y axis values
     */
    private final int OFFSET_Y = 20;
    /**
     * Offset between the x label and the x axis values
     */
    private final int OFFSET_X = 20;
    /**
     * Color used for filling in the bars
     */
    private final Color FILL_COLOR = Color.decode("0xf47748");
    /**
     * Color used as border for the bars
     */
    private final Color BORDER_COLOR = Color.WHITE;

    /**
     * position of origin from x axis
     */
    private int originX;
    /**
     * position of origin from y axis
     */
    private int originY;

    /**
     * Height which takes up one y value
     */
    private int heightOfYValue;

    /**
     * Width which takes up one x value
     */
    private int widthOfXValue;

    /**
     * Width of the x axis
     */
    private int widthOfXAxis;




    /**
     * Creates a new BarChartComponent from given BarChart
     *
     * @param barChart given BarChart
     */
    public BarChartComponent(BarChart barChart) {
        this.barChart = barChart;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        calculateValues(g2d);
        drawAuxiliaryLines(g2d);
        drawBars(g2d);
        drawCoordinateSystem(g2d);
        drawYLabel(g2d);
        drawXLabel(g2d);
        drawYValues(g2d);
        drawXValues(g2d);

        super.paintComponent(g);
    }

    /**
     * Private helper method used for drawing auxiliary lines
     *
     * @param g2d given Graphics2D
     */
    private void drawAuxiliaryLines(Graphics2D g2d) {
        int currentY = originY;
        for (int i = barChart.getYMin(); i <= barChart.getYMax(); i += barChart.getYStep()) {
            g2d.setColor(Color.LIGHT_GRAY);
            g2d.drawLine(originX, currentY, originX + widthOfXAxis, currentY);
            currentY -= heightOfYValue;
        }


        int currentX = originX;
        for(XYValue ignored : barChart.getValues()){
            g2d.drawLine(currentX, originY, currentX,  PADDING);
            currentX += widthOfXValue;
        }
        g2d.setColor(Color.BLACK);

    }

    /**
     * Private helper method used for drawing the bars of the BarChart
     *
     * @param g2d given Graphics2D
     */
    private void drawBars(Graphics2D g2d) {
        int index = 0;
        for (XYValue value : barChart.getValues()) {
            int x = originX + index++ * widthOfXValue;
            int height = (value.getY() - barChart.getYMin()) / barChart.getYStep() * heightOfYValue +
                    (value.getY() - barChart.getYMin()) % barChart.getYStep() * heightOfYValue/ barChart.getYStep();
            int y = originY - height;

            g2d.setColor(FILL_COLOR);
            g2d.fillRect(x, y, widthOfXValue, height);

            g2d.setColor(BORDER_COLOR);
            g2d.drawRect(x, y, widthOfXValue, height);
        }
        g2d.setColor(Color.BLACK);
    }

    /**
     * Private helper method used for drawing the x axis values
     *
     * @param g2d given Graphics2D
     */
    private void drawXValues(Graphics2D g2d) {
        int currentX = originX;
        for (XYValue value : barChart.getValues()) {
            g2d.drawString(String.valueOf(value.getX()),
                    currentX + widthOfXValue / 2 - g2d.getFontMetrics().stringWidth(String.valueOf(value.getX())) / 2,
                    originY + g2d.getFontMetrics().getHeight() + 10);
            System.out.println(currentX);
            currentX += widthOfXValue;
        }
    }

    /**
     * Private helper method used for drawing the y axis values
     *
     * @param g2d given Graphics2D
     */
    private void drawYValues(Graphics2D g2d) {
        int currentY = originY;
        for (int i = barChart.getYMin(); i <= barChart.getYMax(); i += barChart.getYStep()) {
            g2d.drawString(String.valueOf(i), originX - g2d.getFontMetrics().stringWidth(String.valueOf(i)) - 10,
                    currentY + g2d.getFontMetrics().getHeight() / 2);
            currentY -= heightOfYValue;
        }
    }

    /**
     * Private helper method used for drawing the x axis label
     *
     * @param g2d given Graphics2D
     */
    private void drawXLabel(Graphics2D g2d) {
        g2d.drawString(barChart.getXAxis(),
                (getWidth() - g2d.getFontMetrics().stringWidth(barChart.getXAxis())) / 2,
                getHeight() - PADDING);
    }

    /**
     * Private helper method used for drawing thw y axis label
     *
     * @param g2d given Graphics2D
     */
    private void drawYLabel(Graphics2D g2d) {
        AffineTransform defaultAt = g2d.getTransform();
        AffineTransform at = new AffineTransform();
        at.rotate(-Math.PI / 2);
        g2d.setTransform(at);

        g2d.drawString(barChart.getYAxis(),
                -(getHeight() / 2 + g2d.getFontMetrics().stringWidth(barChart.getYAxis()) / 2 + 2 * PADDING),
                PADDING);

        g2d.setTransform(defaultAt);
    }

    /**
     * Private helper method which calculates all the necessary values for further calculations
     *
     * @param g2d given Graphics2D
     */
    private void calculateValues(Graphics2D g2d) {
        int widthOfYValue = g2d.getFontMetrics().stringWidth(String.valueOf(barChart.getYMax()));
        int heightOfXValue = g2d.getFontMetrics().getHeight();

        originX = PADDING + g2d.getFontMetrics().getHeight() + OFFSET_Y + widthOfYValue;
        originY = getHeight() - PADDING - heightOfXValue - OFFSET_X - g2d.getFontMetrics().getHeight();

        int heightOfYAxis = originY - PADDING;
        int numberOfYValues = (barChart.getYMax() - barChart.getYMin()) / barChart.getYStep();
        heightOfYValue = heightOfYAxis / numberOfYValues;

        widthOfXAxis = getWidth() - originX - PADDING;
        widthOfXValue = widthOfXAxis / barChart.getValues().size();
    }

    /**
     * Private helper method used for drawing the coordinate system which consists of the x and y axis and the arrows
     *
     * @param g2d given Graphics2D
     */
    private void drawCoordinateSystem(Graphics2D g2d) {
        g2d.drawLine(originX, originY, originX, PADDING);
        g2d.drawLine(originX, originY, originX + widthOfXAxis, originY);

        GeneralPath shape = new GeneralPath();
        shape.moveTo(originX - 5, PADDING);
        shape.lineTo(originX + 5, PADDING);
        shape.lineTo(originX, PADDING - 7);
        shape.closePath();
        g2d.fill(shape);

        shape = new GeneralPath();
        shape.moveTo(originX + widthOfXAxis, originY - 5);
        shape.lineTo(originX + widthOfXAxis, originY + 5);
        shape.lineTo(originX + widthOfXAxis + 7, originY);
        shape.closePath();
        g2d.fill(shape);
    }

}
