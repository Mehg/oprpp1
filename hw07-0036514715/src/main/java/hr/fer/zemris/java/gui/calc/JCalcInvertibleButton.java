package hr.fer.zemris.java.gui.calc;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.calc.model.CalculatorInputException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.function.DoubleUnaryOperator;

/**
 * Models all invertible function buttons of Calculator
 */
public class JCalcInvertibleButton extends JButton {
    /**
     * normal text
     */
    private final String text;
    /**
     * inverted text
     */
    private final String invText;
    /**
     * inverted ActionListener
     */
    private ActionListener invListener;
    /**
     * normal ActionListener
     */
    private ActionListener listener;
    /**
     * Whether the inverted button is pressed
     */
    private boolean inverted;

    /**
     * Creates a new JCalcInvertibleButton with text set as current text an inverted text set as invText
     * Styles it in Calculator style
     *
     * @param text    given text
     * @param invText given inverted text
     */
    public JCalcInvertibleButton(String text, String invText) {
        this.text = text;
        this.invText = invText;
        inverted = false;
        this.setBackground(Color.decode("0xddddff"));
        this.setBorder(BorderFactory.createLineBorder(Color.decode("0x96a1b6")));
        this.setOpaque(true);
    }

    /**
     * Creates a new JCalcInvertibleButton with given parameters
     *
     * @param text              given normal text
     * @param listener          given normal listener
     * @param invText           given inverted text
     * @param invActionListener given inverted listener
     */
    public JCalcInvertibleButton(String text, ActionListener listener, String invText, ActionListener invActionListener) {
        this(text, invText);
        this.invListener = invActionListener;
        this.listener = listener;
    }

    /**
     * Creates a new JCalcInvertibleButton with given parameters and adds default ActionListener and
     * inverted ActionListener - used for unary operations
     * The default ActionListener, as well as the inverted, apply the operation to the current value of the Calculator
     *
     * @param text        given normal text
     * @param operator    given operator used in normal listener
     * @param invText     given inverted text
     * @param invOperator given operator used in inverted listener
     * @param calcModel   given CalcModel
     */
    public JCalcInvertibleButton(String text, DoubleUnaryOperator operator, String invText, DoubleUnaryOperator invOperator, CalcModel calcModel) {
        this(text, c -> {
            if (calcModel.hasFrozenValue())
                throw new CalculatorInputException("Already has frozen value");
            calcModel.setValue(operator.applyAsDouble(calcModel.getValue()));

        }, invText, c -> {
            if (calcModel.hasFrozenValue())
                throw new CalculatorInputException("Already has frozen value");
            calcModel.setValue(invOperator.applyAsDouble(calcModel.getValue()));
        });
    }

    @Override
    public String getText() {
        return inverted ? invText : text;
    }

    @Override
    public ActionListener[] getActionListeners() {
        ActionListener[] listeners = new ActionListener[1];
        listeners[0] = inverted ? invListener : listener;
        return listeners;
    }

    @Override
    protected void fireActionPerformed(ActionEvent event) {
        ActionListener thisListener = inverted ? invListener : listener;

        ActionEvent e;

        String actionCommand = event.getActionCommand();
        if (actionCommand == null) {
            actionCommand = getActionCommand();
        }
        e = new ActionEvent(this,
                ActionEvent.ACTION_PERFORMED,
                actionCommand,
                event.getWhen(),
                event.getModifiers());

        thisListener.actionPerformed(e);

    }

    /**
     * Sets the inverted value to given value
     *
     * @param inverted give value
     */
    public void setInverted(boolean inverted) {
        this.inverted = inverted;
    }
}
