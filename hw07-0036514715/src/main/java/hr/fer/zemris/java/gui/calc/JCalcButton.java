package hr.fer.zemris.java.gui.calc;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Generic button used in Calculator
 */
public class JCalcButton extends JButton {

    /**
     * Creates a new JCalcButton from given text and applies the calculator style
     *
     * @param text given text
     */
    public JCalcButton(String text) {
        this.setText(text);
        this.setBackground(Color.decode("0xddddff"));
        this.setBorder(BorderFactory.createLineBorder(Color.decode("0x96a1b6")));
        this.setOpaque(true);
    }

    /**
     * Creates a new JCalcButton with given text and ActionListener
     *
     * @param text     given text
     * @param listener given ActionListener
     */
    public JCalcButton(String text, ActionListener listener) {
        this(text);
        this.addActionListener(listener);
    }
}
