package hr.fer.zemris.java.gui.layouts;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import javax.swing.*;
import java.awt.*;

public class CalcLayoutTest {
    @Test
    public void testInvalidRowAndCol() {
        CalcLayout calcLayout = new CalcLayout();
        JComponent c = new JComponent() {
        };
        assertThrows(CalcLayoutException.class, () -> calcLayout.addLayoutComponent(c, "0,3"));
        assertThrows(CalcLayoutException.class, () -> calcLayout.addLayoutComponent(c, "6,3"));
        assertThrows(CalcLayoutException.class, () -> calcLayout.addLayoutComponent(c, "3,0"));
        assertThrows(CalcLayoutException.class, () -> calcLayout.addLayoutComponent(c, "3,8"));
    }

    @Test
    public void testInvalidFirstElement() {
        CalcLayout calcLayout = new CalcLayout();
        JComponent c = new JComponent() {
        };
        assertThrows(CalcLayoutException.class, () -> calcLayout.addLayoutComponent(c, "1,3"));
        assertThrows(CalcLayoutException.class, () -> calcLayout.addLayoutComponent(c, "1,2"));
        assertThrows(CalcLayoutException.class, () -> calcLayout.addLayoutComponent(c, "1,5"));
    }

    @Test
    public void testInvalidComponentAlreadyExists() {
        CalcLayout calcLayout = new CalcLayout();
        JComponent c = new JComponent() {
        };
        calcLayout.addLayoutComponent(c, "1,1");
        assertThrows(CalcLayoutException.class, () -> calcLayout.addLayoutComponent(c, "1,1"));
    }

    @Test
    public void testRemove() {
        CalcLayout calcLayout = new CalcLayout();
        JComponent c = new JComponent() {
        };

        JComponent c2 = new JButton();
        calcLayout.addLayoutComponent(c, "1,1");
        calcLayout.addLayoutComponent(c, "2,1");

        System.out.println(calcLayout.elements);
        System.out.println(calcLayout.elements.keySet());
        calcLayout.removeLayoutComponent(c);

        System.out.println(calcLayout.elements);

        System.out.println(calcLayout.elements.keySet());
    }

    @Test
    public void testFromPDF1() {
        JPanel p = new JPanel(new CalcLayout(2));
        JLabel l1 = new JLabel("");
        l1.setPreferredSize(new Dimension(10, 30));
        JLabel l2 = new JLabel("");
        l2.setPreferredSize(new Dimension(20, 15));
        p.add(l1, new RCPosition(2, 2));
        p.add(l2, new RCPosition(3, 3));
        Dimension dim = p.getPreferredSize();

        assertEquals(152, dim.width);
        assertEquals(158, dim.height);
    }

    @Test
    public void testFromPDF2() {
        JPanel p = new JPanel(new CalcLayout(2));
        JLabel l1 = new JLabel("");
        l1.setPreferredSize(new Dimension(108, 15));
        JLabel l2 = new JLabel("");
        l2.setPreferredSize(new Dimension(16, 30));
        p.add(l1, new RCPosition(1, 1));
        p.add(l2, new RCPosition(3, 3));
        Dimension dim = p.getPreferredSize();

        assertEquals(152, dim.width);
        assertEquals(158, dim.height);
    }
}
