package hr.fer.zemris.java.gui.layouts;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RCPositionTest {

    @Test

    public void testFactory() {
        String[] array = new String[]{"3, 7", " 3 , 7 "};
        RCPosition first = RCPosition.parse(array[0]);
        RCPosition second = RCPosition.parse(array[1]);
        RCPosition expected = new RCPosition(3,7);
        assertEquals(expected, first);
        assertEquals(expected, second);
    }

    @Test
    public void testNegative() {
        RCPosition expected = new RCPosition(-4, 1);
        RCPosition test = RCPosition.parse("-4, 1");
        assertEquals(expected, test);
    }
}
